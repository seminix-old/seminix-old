/*
 * Based on arch/arm/include/asm/cacheflush.h
 *
 * Copyright (C) 1999-2002 Russell King.
 * Copyright (C) 2012 ARM Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ASM_CACHEFLUSH_H
#define ASM_CACHEFLUSH_H

#include <seminix/types.h>
#include <seminix/smp.h>
#include <asm/barrier.h>
#include <asm/pgtable-types.h>
#include <asm/cpufeature.h>

struct page;
struct vm_area_struct;

#define PG_dcache_clean PG_arch_1

extern void __flush_icache_range(unsigned long start, unsigned long end);
/*
 *	invalidate_icache_range(start, end)
 *
 *		Invalidate the I-cache in the region described by start, end.
 *		- start  - virtual start address
 *		- end    - virtual end address
 */
extern int invalidate_icache_range(unsigned long start, unsigned long end);
/*
 *	__flush_dcache_area(kaddr, size)
 *
 *		Ensure that the data held in page is written back.
 *		- kaddr  - page address
 *		- size   - region size
 */
extern void __flush_dcache_area(void *addr, usize len);
extern void __inval_dcache_area(void *addr, usize len);
extern void __clean_dcache_area_poc(void *addr, usize len);
extern void __clean_dcache_area_pop(void *addr, usize len);
extern void __clean_dcache_area_pou(void *addr, usize len);
/*
 *  __flush_cache_user_range(start, end)
 *		Ensure coherency between the I-cache and the D-cache in the
 *		region described by start, end.
 *		- start  - virtual start address
 *		- end    - virtual end address
 */
extern long __flush_cache_user_range(unsigned long start, unsigned long end);
extern void sync_icache_aliases(void *kaddr, unsigned long len);

static inline void flush_icache_range(unsigned long start, unsigned long end)
{
    __flush_icache_range(start, end);

    /*
     * IPI all online CPUs so that they undergo a context synchronization
     * event and are forced to refetch the new instructions.
     */
    kick_all_cpus_sync();
}

static inline void flush_cache_range(struct vm_area_struct *vma,
    unsigned long start, unsigned long end)
{
}

/*
 * Cache maintenance functions used by the DMA API. No to be used directly.
 */
extern void __dma_map_area(const void *, usize, int);
extern void __dma_unmap_area(const void *, usize, int);
extern void __dma_flush_area(const void *, usize);

/*
 * Copy user data from/to a page which is mapped into a different
 * processes address space.  Really, we want to allow our "user
 * space" model to handle this.
 */
extern void copy_to_user_page(struct vm_area_struct *, struct page *,
    unsigned long, void *, const void *, unsigned long);
#define copy_from_user_page(vma, page, vaddr, dst, src, len) \
    do {							\
        memcpy(dst, src, len);				\
    } while (0)

extern void flush_dcache_page(struct page *);

static inline void __flush_icache_all(void)
{
    if (cpus_have_const_cap(ARM64_HAS_CACHE_DIC))
        return;

    asm("ic	ialluis");
    dsb(ish);
}

extern void __sync_icache_dcache(pte_t pteval);

#include <asm-generic/cacheflush.h>

#endif /* !ASM_CACHEFLUSH_H */
