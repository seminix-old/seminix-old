#ifndef ASM_BITSPERLONG_H
#define ASM_BITSPERLONG_H

#define __BITS_PER_LONG 64
#define BITS_PER_LONG 64
#define BITS_PER_LONG_LONG 64

#endif /* !ASM_BITSPERLONG_H */
