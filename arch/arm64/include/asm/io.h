/*
 * Based on arch/arm/include/asm/io.h
 *
 * Copyright (C) 1996-2000 Russell King
 * Copyright (C) 2012 ARM Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ASM_IO_H
#define ASM_IO_H

#include <seminix/types.h>
#include <seminix/pgtable.h>
#include <asm/byteorder.h>
#include <asm/barrier.h>

/*
 * Generic IO read/write.  These perform native-endian accesses.
 */
#define __raw_writeb __raw_writeb
static inline void __raw_writeb(u8 val, volatile void __iomem *addr)
{
    asm volatile("strb %w0, [%1]" : : "rZ" (val), "r" (addr));
}

#define __raw_writew __raw_writew
static inline void __raw_writew(u16 val, volatile void __iomem *addr)
{
    asm volatile("strh %w0, [%1]" : : "rZ" (val), "r" (addr));
}

#define __raw_writel __raw_writel
static inline void __raw_writel(u32 val, volatile void __iomem *addr)
{
    asm volatile("str %w0, [%1]" : : "rZ" (val), "r" (addr));
}

#define __raw_writeq __raw_writeq
static inline void __raw_writeq(u64 val, volatile void __iomem *addr)
{
    asm volatile("str %x0, [%1]" : : "rZ" (val), "r" (addr));
}

#define __raw_readb __raw_readb
static inline u8 __raw_readb(const volatile void __iomem *addr)
{
    u8 val;
    asm volatile("ldrb %w0, [%1]"
             : "=r" (val) : "r" (addr));
    return val;
}

#define __raw_readw __raw_readw
static inline u16 __raw_readw(const volatile void __iomem *addr)
{
    u16 val;

    asm volatile("ldrh %w0, [%1]"
             : "=r" (val) : "r" (addr));
    return val;
}

#define __raw_readl __raw_readl
static inline u32 __raw_readl(const volatile void __iomem *addr)
{
    u32 val;
    asm volatile("ldr %w0, [%1]"
             : "=r" (val) : "r" (addr));
    return val;
}

#define __raw_readq __raw_readq
static inline u64 __raw_readq(const volatile void __iomem *addr)
{
    u64 val;
    asm volatile("ldr %0, [%1]"
             : "=r" (val) : "r" (addr));
    return val;
}

/* IO barriers */
#define __io_ar(v)							\
({									\
    unsigned long tmp;						\
                                    \
    dma_rmb();								\
                                    \
    /*								\
     * Create a dummy control dependency from the IO read to any	\
     * later instructions. This ensures that a subsequent call to	\
     * udelay() will be ordered due to the ISB in get_cycles().	\
     */								\
    asm volatile("eor	%0, %1, %1\n"				\
             "cbnz	%0, ."					\
             : "=r" (tmp) : "r" ((unsigned long)(v))		\
             : "memory");					\
})

#define __io_bw()		dma_wmb()
#define __io_br(v)
#define __io_aw(v)

/* arm64-specific, don't use in portable drivers */
#define __iormb(v)		__io_ar(v)
#define __iowmb()		__io_bw()
#define __iomb()		dma_mb()

/*
 * io{read,write}{16,32,64}be() macros
 */
#define ioread16be(p)		({ __u16 __v = be16_to_cpu((__be16)__raw_readw(p)); __iormb(__v); __v; })
#define ioread32be(p)		({ __u32 __v = be32_to_cpu((__be32)__raw_readl(p)); __iormb(__v); __v; })
#define ioread64be(p)		({ __u64 __v = be64_to_cpu((__be64)__raw_readq(p)); __iormb(__v); __v; })

#define iowrite16be(v,p)	({ __iowmb(); __raw_writew((__u16)cpu_to_be16(v), p); })
#define iowrite32be(v,p)	({ __iowmb(); __raw_writel((__u32)cpu_to_be32(v), p); })
#define iowrite64be(v,p)	({ __iowmb(); __raw_writeq((__u64)cpu_to_be64(v), p); })

#include <asm-generic/io.h>

/*
 * I/O memory mapping functions.
 */
extern void __iomem *__ioremap(phys_addr_t phys_addr, usize size, pgprot_t prot);
#define ioremap(addr, size) __ioremap((addr), (size), __pgprot(PROT_DEVICE_nGnRE))

#endif /* !ASM_IO_H */
