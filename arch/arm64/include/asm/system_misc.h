/*
 * Based on arch/arm/include/asm/system_misc.h
 *
 * Copyright (C) 2012 ARM Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ASM_SYSTEM_MISC_H
#define ASM_SYSTEM_MISC_H

struct pt_regs;
enum reboot_mode;

void die(const char *msg, struct pt_regs *regs, int err);

void arm64_notify_die(const char *str, struct pt_regs *regs,
              int signo, int sicode, void __user *addr,
              int err);

void hook_debug_fault_code(int nr, int (*fn)(unsigned long, unsigned int,
                         struct pt_regs *),
               int sig, int code, const char *name);

extern void __show_regs(struct pt_regs *);

extern void (*arm_pm_restart)(enum reboot_mode reboot_mode, const char *cmd);

/*
 * Callbacks for platform drivers to implement.
 */
extern void (*pm_power_off)(void);

#endif	/* ASM_SYSTEM_MISC_H */
