#ifndef ASM_PAGE_H
#define ASM_PAGE_H

#define __ARCH_HAVE_CLEAR_PAGE
extern void clear_page(void *to);

#define __ARCH_HAVE_COPY_PAGE
extern void copy_page(void *to, const void *from);

extern void __cpu_clear_user_page(void *p, unsigned long user);
extern void __cpu_copy_user_page(void *to, const void *from,
                 unsigned long user);

#define clear_user_page(addr,vaddr,pg)  __cpu_clear_user_page(addr, vaddr)
#define copy_user_page(to,from,vaddr,pg) __cpu_copy_user_page(to, from, vaddr)

#endif /* ASM_PAGE_H */
