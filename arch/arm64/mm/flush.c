// SPDX-License-Identifier: GPL-2.0-only
/*
 * Based on arch/arm/mm/flush.c
 *
 * Copyright (C) 1995-2002 Russell King
 * Copyright (C) 2012 ARM Ltd.
 */
#include <seminix/pgtable.h>
#include <seminix/mm.h>
#include <seminix/mmap.h>
#include <asm/cacheflush.h>
#include <asm/cache.h>
#include <asm/tlbflush.h>

/*
 * Whilst the D-side always behaves as PIPT on AArch64, aliasing is
 * permitted in the I-cache.
 */
static inline int icache_is_aliasing(void)
{
    return test_bit(ICACHEF_ALIASING, &__icache_flags);
}

void sync_icache_aliases(void *kaddr, unsigned long len)
{
    unsigned long addr = (unsigned long)kaddr;

    if (icache_is_aliasing()) {
        __clean_dcache_area_pou(kaddr, len);
        __flush_icache_all();
    } else {
        /*
         * Don't issue kick_all_cpus_sync() after I-cache invalidation
         * for user mappings.
         */
        __flush_icache_range(addr, addr + len);
    }
}

static void flush_ptrace_access(struct vm_area_struct *vma, struct page *page,
                unsigned long uaddr, void *kaddr,
                unsigned long len)
{
    if (vma->vm_flags & VM_EXEC)
        sync_icache_aliases(kaddr, len);
}

/*
 * Copy user data from/to a page which is mapped into a different processes
 * address space.  Really, we want to allow our "user space" model to handle
 * this.
 */
void copy_to_user_page(struct vm_area_struct *vma, struct page *page,
               unsigned long uaddr, void *dst, const void *src,
               unsigned long len)
{
    memcpy(dst, src, len);
    flush_ptrace_access(vma, page, uaddr, dst, len);
}

void __sync_icache_dcache(pte_t pte)
{
    struct page *page = pte_page(pte);

    if (!test_and_set_bit(PG_dcache_clean, &page->flags))
        sync_icache_aliases(page_address(page),
                    PAGE_SIZE << compound_order(page));
}

/*
 * This function is called when a page has been modified by the kernel. Mark
 * it as dirty for later flushing when mapped in user space (if executable,
 * see __sync_icache_dcache).
 */
void flush_dcache_page(struct page *page)
{
    if (test_bit(PG_dcache_clean, &page->flags))
        clear_bit(PG_dcache_clean, &page->flags);
}
