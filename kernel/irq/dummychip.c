// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 1992, 1998-2006 Linus Torvalds, Ingo Molnar
 * Copyright (C) 2005-2006, Thomas Gleixner, Russell King
 *
 * This file contains the dummy interrupt chip implementation
 */
#include <seminix/cpumask.h>
#include <seminix/irq.h>
#include <seminix/irqdesc.h>
#include <seminix/irq/dummychip.h>
#include "internal.h"

/*
 * What should we do if we get a hw irq event on an illegal vector?
 * Each architecture has to answer this themself.
 */
static void ack_bad(struct irq_data *data)
{
    struct irq_desc *desc = irq_data_to_desc(data);

    print_irq_desc(data->irq, desc);
    ack_bad_irq(data->irq);
}

/*
 * NOP functions
 */
static void noop(struct irq_data *data) { }

static int noop_ret(struct irq_data *data)
{
    return 0;
}

/*
 * Generic no controller implementation
 */
struct irq_chip no_irq_chip = {
    .name		= "none",
    .irq_startup	= noop_ret,
    .irq_shutdown	= noop,
    .irq_unmask	= noop,
    .irq_mask	= noop,
    .irq_ack	= ack_bad,
    .flags		= IRQCHIP_SKIP_SET_WAKE,
};
