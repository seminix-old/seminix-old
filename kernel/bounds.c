// SPDX-License-Identifier: GPL-2.0
/*
 * Generate definitions needed by the preprocessor.
 * This code generates raw asm output which is post-processed
 * to extract and format the required data.
 */

#define __GENERATING_BOUNDS_H
/* Include headers that define the enum constants of interest */
#include <seminix/page_types.h>
#include <seminix/mmzone.h>
#include <seminix/kbuild.h>

int main(void)
{
    DEFINE(MAX_NR_ZONES, __MAX_NR_ZONES);
    DEFINE(NR_PAGEFLAGS, __NR_PAGEFLAGS);
    BLANK();

	return 0;
}
