#include <seminix/rcutree.h>

void rcu_idle_enter(void)
{
}

void rcu_idle_exit(void)
{
}

void call_rcu(struct rcu_head *head, rcu_callback_t func)
{
}

/*
 * Note a PREEMPT=n context switch.  The caller must have disabled interrupts.
 */
void rcu_note_context_switch(bool preempt)
{

}

