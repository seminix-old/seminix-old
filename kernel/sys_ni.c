// SPDX-License-Identifier: GPL-2.0

#include <seminix/errno.h>
#include <seminix/kernel.h>
#include <seminix/linkage.h>
#include <seminix/syscall.h>
#include <seminix/dump_stack.h>
#include <seminix/sched.h>
#include <seminix/sched/task_stack.h>
#include <asm/ptrace.h>

#define SYS_NI_DEBUG 1

asmlinkage long sys_ni_syscall(void);

/*
 * Non-implemented system calls get redirected here.
 */
asmlinkage long sys_ni_syscall(void)
{
#if defined SYS_NI_DEBUG
    panic("%s sys: %d\n", __func__, syscall_get_nr(current, current_pt_regs()));
#else
    return -ENOSYS;
#endif
}
