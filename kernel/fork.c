#include <seminix/sched.h>
#include <seminix/sched/task_stack.h>

void __put_task_struct(struct task_struct *tsk)
{
}

void put_task_stack(struct task_struct *tsk)
{
}

void __mmdrop(struct mm_struct *mm)
{
}

pid_t kernel_thread(int (*fn)(void *), void *arg, unsigned long flags)
{
    return 0;
}

struct task_struct *fork_idle(int cpu)
{
    return NULL;
}
void set_task_stack_end_magic(struct task_struct *tsk)
{
	unsigned long *stackend;

	stackend = end_of_stack(tsk);
	*stackend = STACK_END_MAGIC;	/* for overflow detection */
}
