/*
 *  kernel/sched/core.c
 *
 *  Core kernel scheduler code and related syscalls
 *
 *  Copyright (C) 1991-2002  Linus Torvalds
 */
#include "sched.h"
#include "pelt.h"
#include <asm-generic/switch_to.h>
#include <asm/mmu_context.h>

DEFINE_PER_CPU_SHARED_ALIGNED(struct rq, runqueues);

/*
 * Number of tasks to iterate in a single balance run.
 * Limited because this is done with IRQs disabled.
 */
const_debug unsigned int sysctl_sched_nr_migrate = 32;

/*
 * period over which we measure -rt task CPU usage in us.
 * default: 1s
 */
unsigned int sysctl_sched_rt_period = 1000000;

/*
 * part of the period that we allow rt tasks to run in us.
 * default: 0.95s
 */
int sysctl_sched_rt_runtime = 950000;

__read_mostly int scheduler_running;

/*
 * __task_rq_lock - lock the rq @p resides on.
 */
struct rq *__task_rq_lock(struct task_struct *p, struct rq_flags *rf)
    __acquires(rq->lock)
{
    struct rq *rq;

    for (;;) {
        rq = task_rq(p);
        raw_spin_lock(&rq->lock);
        if (likely(rq == task_rq(p) && !task_on_rq_migrating(p))) {
            rq_pin_lock(rq, rf);
            return rq;
        }
        raw_spin_unlock(&rq->lock);

        while (unlikely(task_on_rq_migrating(p)))
            cpu_relax();
    }
}

/*
 * task_rq_lock - lock p->pi_lock and lock the rq @p resides on.
 */
struct rq *task_rq_lock(struct task_struct *p, struct rq_flags *rf)
    __acquires(p->pi_lock)
    __acquires(rq->lock)
{
    struct rq *rq;

    for (;;) {
        raw_spin_lock_irqsave(&p->pi_lock, rf->flags);
        rq = task_rq(p);
        raw_spin_lock(&rq->lock);
        /*
         *	move_queued_task()		task_rq_lock()
         *
         *	ACQUIRE (rq->lock)
         *	[S] ->on_rq = MIGRATING		[L] rq = task_rq()
         *	WMB (__set_task_cpu())		ACQUIRE (rq->lock);
         *	[S] ->cpu = new_cpu		[L] task_rq()
         *					[L] ->on_rq
         *	RELEASE (rq->lock)
         *
         * If we observe the old CPU in task_rq_lock, the acquire of
         * the old rq->lock will fully serialize against the stores.
         *
         * If we observe the new CPU in task_rq_lock, the acquire will
         * pair with the WMB to ensure we must then also see migrating.
         */
        if (likely(rq == task_rq(p) && !task_on_rq_migrating(p))) {
            rq_pin_lock(rq, rf);
            return rq;
        }
        raw_spin_unlock(&rq->lock);
        raw_spin_unlock_irqrestore(&p->pi_lock, rf->flags);

        while (unlikely(task_on_rq_migrating(p)))
            cpu_relax();
    }
}

/*
 * RQ-clock updating methods:
 */

void update_rq_clock(struct rq *rq)
{
    i64 delta;

    if (rq->clock_update_flags & RQCF_ACT_SKIP)
        return;

#ifdef SCHED_DEBUG
    if (sched_feat(WARN_DOUBLE_CLOCK))
        SCHED_WARN_ON(rq->clock_update_flags & RQCF_UPDATED);
    rq->clock_update_flags |= RQCF_UPDATED;
#endif

    delta = sched_clock_cpu(cpu_of(rq)) - rq->clock;
    if (delta < 0)
        return;
    rq->clock += delta;
    rq->clock_task += delta;
}

/*
 * Use HR-timers to deliver accurate preemption points.
 */

static void hrtick_clear(struct rq *rq)
{
    if (hrtimer_active(&rq->hrtick_timer))
        hrtimer_cancel(&rq->hrtick_timer);
}

/*
 * High-resolution timer tick.
 * Runs from hardirq context with interrupts disabled.
 */
static enum hrtimer_restart hrtick(struct hrtimer *timer)
{
    struct rq *rq = container_of(timer, struct rq, hrtick_timer);
    struct rq_flags rf;

    WARN_ON_ONCE(cpu_of(rq) != smp_processor_id());

    rq_lock(rq, &rf);
    update_rq_clock(rq);
    rq->curr->sched_class->task_tick(rq, rq->curr, 1);
    rq_unlock(rq, &rf);

    return HRTIMER_NORESTART;
}

static void __hrtick_restart(struct rq *rq)
{
    struct hrtimer *timer = &rq->hrtick_timer;

    hrtimer_start_expires(timer, HRTIMER_MODE_ABS);
}

/*
 * called from hardirq (IPI) context
 */
static void __hrtick_start(void *arg)
{
    struct rq *rq = arg;
    struct rq_flags rf;

    rq_lock(rq, &rf);
    __hrtick_restart(rq);
    rq->hrtick_csd_pending = 0;
    rq_unlock(rq, &rf);
}

/*
 * Called to set the hrtick timer state.
 *
 * called with rq->lock held and irqs disabled
 */
void hrtick_start(struct rq *rq, u64 delay)
{
    struct hrtimer *timer = &rq->hrtick_timer;
    ktime_t time;
    i64 delta;

    /*
     * Don't schedule slices shorter than 10000ns, that just
     * doesn't make sense and can cause timer DoS.
     */
    delta = max_t(i64, delay, 10000LL);
    time = ktime_add_ns(timer->base->get_time(), delta);

    hrtimer_set_expires(timer, time);

    if (rq == this_rq()) {
        __hrtick_restart(rq);
    } else if (!rq->hrtick_csd_pending) {
        smp_call_function_single_async(cpu_of(rq), &rq->hrtick_csd);
        rq->hrtick_csd_pending = 1;
    }
}

static void hrtick_rq_init(struct rq *rq)
{
    rq->hrtick_csd_pending = 0;
    rq->hrtick_csd.flags = 0;
    rq->hrtick_csd.func = __hrtick_start;
    rq->hrtick_csd.info = rq;

    hrtimer_init(&rq->hrtick_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
    rq->hrtick_timer.function = hrtick;
}

/*
 * cmpxchg based fetch_or, macro so it works for different integer types
 */
#define fetch_or(ptr, mask)						\
    ({								\
        typeof(ptr) _ptr = (ptr);				\
        typeof(mask) _mask = (mask);				\
        typeof(*_ptr) _old, _val = *_ptr;			\
                                    \
        for (;;) {						\
            _old = cmpxchg(_ptr, _val, _val | _mask);	\
            if (_old == _val)				\
                break;					\
            _val = _old;					\
        }							\
    _old;								\
})

/*
 * Atomically set TIF_NEED_RESCHED and test for TIF_POLLING_NRFLAG,
 * this avoids any races wrt polling state changes and thereby avoids
 * spurious IPIs.
 */
static bool set_nr_and_not_polling(struct task_struct *p)
{
    struct thread_info *ti = task_thread_info(p);
    return !(fetch_or(&ti->flags, _TIF_NEED_RESCHED) & _TIF_POLLING_NRFLAG);
}

/*
 * Atomically set TIF_NEED_RESCHED if TIF_POLLING_NRFLAG is set.
 *
 * If this returns true, then the idle task promises to call
 * sched_ttwu_pending() and reschedule soon.
 */
static bool set_nr_if_polling(struct task_struct *p)
{
    struct thread_info *ti = task_thread_info(p);
    typeof(ti->flags) old, val = READ_ONCE(ti->flags);

    for (;;) {
        if (!(val & _TIF_POLLING_NRFLAG))
            return false;
        if (val & _TIF_NEED_RESCHED)
            return true;
        old = cmpxchg(&ti->flags, val, val | _TIF_NEED_RESCHED);
        if (old == val)
            break;
        val = old;
    }
    return true;
}

/**
 * wake_q_add() - queue a wakeup for 'later' waking.
 * @head: the wake_q_head to add @task to
 * @task: the task to queue for 'later' wakeup
 *
 * Queue a task for later wakeup, most likely by the wake_up_q() call in the
 * same context, _HOWEVER_ this is not guaranteed, the wakeup can come
 * instantly.
 *
 * This function must be used as-if it were wake_up_process(); IOW the task
 * must be ready to be woken at this location.
 */
void wake_q_add(struct wake_q_head *head, struct task_struct *task)
{
    struct wake_q_node *node = &task->wake_q;

    /*
     * Atomically grab the task, if ->wake_q is !nil already it means
     * its already queued (either by us or someone else) and will get the
     * wakeup due to that.
     *
     * In order to ensure that a pending wakeup will observe our pending
     * state, even in the failed case, an explicit smp_mb() must be used.
     */
    smp_mb__before_atomic();
    if (cmpxchg_relaxed(&node->next, NULL, WAKE_Q_TAIL))
        return;

    get_task_struct(task);

    /*
     * The head is context local, there can be no concurrency.
     */
    *head->lastp = node;
    head->lastp = &node->next;
}

void wake_up_q(struct wake_q_head *head)
{
    struct wake_q_node *node = head->first;

    while (node != WAKE_Q_TAIL) {
        struct task_struct *task;

        task = container_of(node, struct task_struct, wake_q);
        BUG_ON(!task);
        /* Task can safely be re-inserted now: */
        node = node->next;
        task->wake_q.next = NULL;

        /*
         * wake_up_process() executes a full barrier, which pairs with
         * the queueing in wake_q_add() so as not to miss wakeups.
         */
        wake_up_process(task);
        put_task_struct(task);
    }
}

/*
 * resched_curr - mark rq's current task 'to be rescheduled now'.
 *
 * On UP this means the setting of the need_resched flag, on SMP it
 * might also involve a cross-CPU call to trigger the scheduler on
 * the target CPU.
 */
void resched_curr(struct rq *rq)
{
    struct task_struct *curr = rq->curr;
    int cpu;

    if (test_tsk_need_resched(curr))
        return;

    cpu = cpu_of(rq);

    if (cpu == smp_processor_id()) {
        set_tsk_need_resched(curr);
        set_preempt_need_resched();
        return;
    }

    if (set_nr_and_not_polling(curr))
        smp_send_reschedule(cpu);
}

void resched_cpu(int cpu)
{
    struct rq *rq = cpu_rq(cpu);
    unsigned long flags;

    raw_spin_lock_irqsave(&rq->lock, flags);
    if (cpu_online(cpu) || cpu == smp_processor_id())
        resched_curr(rq);
    raw_spin_unlock_irqrestore(&rq->lock, flags);
}

/*
 * When add_timer_on() enqueues a timer into the timer wheel of an
 * idle CPU then this timer might expire before the next timer event
 * which is scheduled to wake up that CPU. In case of a completely
 * idle system the next event might even be infinite time into the
 * future. wake_up_idle_cpu() ensures that the CPU is woken up and
 * leaves the inner idle loop so the newly added timer is taken into
 * account when the CPU goes back to idle and evaluates the timer
 * wheel for the next timer event.
 */
void wake_up_idle_cpu(int cpu)
{
    struct rq *rq = cpu_rq(cpu);

    if (cpu == smp_processor_id())
        return;

    if (set_nr_and_not_polling(rq->idle))
        smp_send_reschedule(cpu);
}

static void set_load_weight(struct task_struct *p, bool update_load)
{
    int prio = p->static_prio - MAX_RT_PRIO;
    struct load_weight *load = &p->se.load;

    /*
     * SCHED_IDLE tasks get minimal weight:
     */
    if (task_has_idle_policy(p)) {
        load->weight = scale_load(WEIGHT_IDLEPRIO);
        load->inv_weight = WMULT_IDLEPRIO;
        p->se.runnable_weight = load->weight;
        return;
    }

    /*
     * SCHED_OTHER tasks have to update their load when changing their
     * weight
     */
    if (update_load && p->sched_class == &fair_sched_class) {
        reweight_task(p, prio);
    } else {
        load->weight = scale_load(sched_prio_to_weight[prio]);
        load->inv_weight = sched_prio_to_wmult[prio];
        p->se.runnable_weight = load->weight;
    }
}

static inline void enqueue_task(struct rq *rq, struct task_struct *p, int flags)
{
    if (!(flags & ENQUEUE_NOCLOCK))
        update_rq_clock(rq);

    p->sched_class->enqueue_task(rq, p, flags);
}

static inline void dequeue_task(struct rq *rq, struct task_struct *p, int flags)
{
    if (!(flags & DEQUEUE_NOCLOCK))
        update_rq_clock(rq);

    p->sched_class->dequeue_task(rq, p, flags);
}

void activate_task(struct rq *rq, struct task_struct *p, int flags)
{
    if (task_contributes_to_load(p))
        rq->nr_uninterruptible--;

    enqueue_task(rq, p, flags);
}

void deactivate_task(struct rq *rq, struct task_struct *p, int flags)
{
    if (task_contributes_to_load(p))
        rq->nr_uninterruptible++;

    dequeue_task(rq, p, flags);
}

/*
 * __normal_prio - return the priority that is based on the static prio
 */
static inline int __normal_prio(struct task_struct *p)
{
    return p->static_prio;
}

/*
 * Calculate the expected normal priority: i.e. priority
 * without taking RT-inheritance into account. Might be
 * boosted by interactivity modifiers. Changes upon fork,
 * setprio syscalls, and whenever the interactivity
 * estimator recalculates.
 */
static inline int normal_prio(struct task_struct *p)
{
    int prio;

    if (task_has_dl_policy(p))
        prio = MAX_DL_PRIO-1;
    else if (task_has_rt_policy(p))
        prio = MAX_RT_PRIO-1 - p->rt_priority;
    else
        prio = __normal_prio(p);
    return prio;
}

/*
 * Calculate the current priority, i.e. the priority
 * taken into account by the scheduler. This value might
 * be boosted by RT tasks, or might be boosted by
 * interactivity modifiers. Will be RT if the task got
 * RT-boosted. If not then it returns p->normal_prio.
 */
static int effective_prio(struct task_struct *p)
{
    p->normal_prio = normal_prio(p);
    /*
     * If we are RT tasks or we were boosted to RT priority,
     * keep the priority unchanged. Otherwise, update priority
     * to the normal priority:
     */
    if (!rt_prio(p->prio))
        return p->normal_prio;
    return p->prio;
}

/**
 * task_curr - is this task currently executing on a CPU?
 * @p: the task in question.
 *
 * Return: 1 if the task is currently executing. 0 otherwise.
 */
inline int task_curr(const struct task_struct *p)
{
    return cpu_curr(task_cpu(p)) == p;
}

/*
 * switched_from, switched_to and prio_changed must _NOT_ drop rq->lock,
 * use the balance_callback list if you want balancing.
 *
 * this means any call to check_class_changed() must be followed by a call to
 * balance_callback().
 */
static inline void check_class_changed(struct rq *rq, struct task_struct *p,
                       const struct sched_class *prev_class,
                       int oldprio)
{
    if (prev_class != p->sched_class) {
        if (prev_class->switched_from)
            prev_class->switched_from(rq, p);

        p->sched_class->switched_to(rq, p);
    } else if (oldprio != p->prio || dl_task(p))
        p->sched_class->prio_changed(rq, p, oldprio);
}

void check_preempt_curr(struct rq *rq, struct task_struct *p, int flags)
{
    const struct sched_class *class;

    if (p->sched_class == rq->curr->sched_class) {
        rq->curr->sched_class->check_preempt_curr(rq, p, flags);
    } else {
        for_each_class(class) {
            if (class == rq->curr->sched_class)
                break;
            if (class == p->sched_class) {
                resched_curr(rq);
                break;
            }
        }
    }

    /*
     * A queue event has occurred, and we're going to schedule.  In
     * this case, we can save a useless back to back clock update.
     */
    if (task_on_rq_queued(rq->curr) && test_tsk_need_resched(rq->curr))
        rq_clock_skip_update(rq);
}

static inline bool is_per_cpu_kthread(struct task_struct *p)
{
    if (!(p->flags & PF_KTHREAD))
        return false;

    if (p->nr_cpus_allowed != 1)
        return false;

    return true;
}

/*
 * Per-CPU kthreads are allowed to run on !actie && online CPUs, see
 * __set_cpus_allowed_ptr() and select_fallback_rq().
 */
static inline bool is_cpu_allowed(struct task_struct *p, int cpu)
{
    if (!cpumask_test_cpu(cpu, &p->cpus_allowed))
        return false;

    if (is_per_cpu_kthread(p))
        return cpu_online(cpu);

    return cpu_online(cpu);
}

/*
 * This is how migration works:
 *
 * 1) we invoke migration_cpu_stop() on the target CPU using
 *    stop_one_cpu().
 * 2) stopper starts to run (implicitly forcing the migrated thread
 *    off the CPU)
 * 3) it checks whether the migrated task is still in the wrong runqueue.
 * 4) if it's in the wrong runqueue then the migration thread removes
 *    it and puts it into the right queue.
 * 5) stopper completes and stop_one_cpu() returns and the migration
 *    is done.
 */

/*
 * move_queued_task - move a queued task to new rq.
 *
 * Returns (locked) new rq. Old rq's lock is released.
 */
static struct rq *move_queued_task(struct rq *rq, struct rq_flags *rf,
                   struct task_struct *p, int new_cpu)
{
    p->on_rq = TASK_ON_RQ_MIGRATING;
    dequeue_task(rq, p, DEQUEUE_NOCLOCK);
    set_task_cpu(p, new_cpu);
    rq_unlock(rq, rf);

    rq = cpu_rq(new_cpu);

    rq_lock(rq, rf);
    BUG_ON(task_cpu(p) != new_cpu);
    enqueue_task(rq, p, 0);
    p->on_rq = TASK_ON_RQ_QUEUED;
    check_preempt_curr(rq, p, 0);

    return rq;
}

struct migration_arg {
    struct task_struct *task;
    int dest_cpu;
};

/*
 * Move (not current) task off this CPU, onto the destination CPU. We're doing
 * this because either it can't run here any more (set_cpus_allowed()
 * away from this CPU, or CPU going down), or because we're
 * attempting to rebalance this task on exec (sched_exec).
 *
 * So we race with normal scheduler movements, but that's OK, as long
 * as the task is no longer on this CPU.
 */
static struct rq *__migrate_task(struct rq *rq, struct rq_flags *rf,
                 struct task_struct *p, int dest_cpu)
{
    /* Affinity changed (again). */
    if (!is_cpu_allowed(p, dest_cpu))
        return rq;

    update_rq_clock(rq);
    return move_queued_task(rq, rf, p, dest_cpu);
}

/*
 * migration_cpu_stop - this will be executed by a highprio stopper thread
 * and performs thread migration by bumping thread off CPU then
 * 'pushing' onto another runqueue.
 */
static int migration_cpu_stop(void *data)
{
    struct migration_arg *arg = data;
    struct task_struct *p = arg->task;
    struct rq *rq = this_rq();
    struct rq_flags rf;

    /*
     * The original target CPU might have gone down and we might
     * be on another CPU but it doesn't matter.
     */
    local_irq_disable();
    /*
     * We need to explicitly wake pending tasks before running
     * __migrate_task() such that we will not miss enforcing cpus_allowed
     * during wakeups, see set_cpus_allowed_ptr()'s TASK_WAKING test.
     */
    sched_ttwu_pending();

    raw_spin_lock(&p->pi_lock);
    rq_lock(rq, &rf);
    /*
     * If task_rq(p) != rq, it cannot be migrated here, because we're
     * holding rq->lock, if p->on_rq == 0 it cannot get enqueued because
     * we're holding p->pi_lock.
     */
    if (task_rq(p) == rq) {
        if (task_on_rq_queued(p))
            rq = __migrate_task(rq, &rf, p, arg->dest_cpu);
        else
            p->wake_cpu = arg->dest_cpu;
    }
    rq_unlock(rq, &rf);
    raw_spin_unlock(&p->pi_lock);

    local_irq_enable();
    return 0;
}

/*
 * sched_class::set_cpus_allowed must do the below, but is not required to
 * actually call this function.
 */
void set_cpus_allowed_common(struct task_struct *p, const struct cpumask *new_mask)
{
    cpumask_copy(&p->cpus_allowed, new_mask);
    p->nr_cpus_allowed = cpumask_weight(new_mask);
}

void do_set_cpus_allowed(struct task_struct *p, const struct cpumask *new_mask)
{
    struct rq *rq = task_rq(p);
    bool queued, running;

    queued = task_on_rq_queued(p);
    running = task_current(rq, p);

    if (queued)
        dequeue_task(rq, p, DEQUEUE_SAVE | DEQUEUE_NOCLOCK);

    if (running)
        put_prev_task(rq, p);

    p->sched_class->set_cpus_allowed(p, new_mask);

    if (queued)
        enqueue_task(rq, p, ENQUEUE_RESTORE | ENQUEUE_NOCLOCK);
    if (running)
        set_curr_task(rq, p);
}

/*
 * Change a given task's CPU affinity. Migrate the thread to a
 * proper CPU and schedule it away if the CPU it's executing on
 * is removed from the allowed bitmask.
 *
 * NOTE: the caller must have a valid reference to the task, the
 * task must not exit() & deallocate itself prematurely. The
 * call is not atomic; no spinlocks may be held.
 */
static int __set_cpus_allowed_ptr(struct task_struct *p,
                  const struct cpumask *new_mask, bool check)
{
    const struct cpumask *cpu_valid_mask = cpu_online_mask;
    unsigned int dest_cpu;
    struct rq_flags rf;
    struct rq *rq;
    int ret = 0;

    rq = task_rq_lock(p, &rf);
    update_rq_clock(rq);

    /*
     * Must re-check here, to close a race against __kthread_bind(),
     * sched_setaffinity() is not guaranteed to observe the flag.
     */
    if (check && (p->flags & PF_NO_SETAFFINITY)) {
        ret = -EINVAL;
        goto out;
    }

    if (cpumask_equal(&p->cpus_allowed, new_mask))
        goto out;

    if (!cpumask_intersects(new_mask, cpu_valid_mask)) {
        ret = -EINVAL;
        goto out;
    }

    do_set_cpus_allowed(p, new_mask);

    if (p->flags & PF_KTHREAD) {
        /*
         * For kernel threads that do indeed end up on online &&
         * !active we want to ensure they are strict per-CPU threads.
         */
        WARN_ON(cpumask_intersects(new_mask, cpu_online_mask) && p->nr_cpus_allowed != 1);
    }

    /* Can the task run on the task's current CPU? If so, we're done */
    if (cpumask_test_cpu(task_cpu(p), new_mask))
        goto out;

    dest_cpu = cpumask_any_and(cpu_valid_mask, new_mask);
    if (task_running(rq, p) || p->state == TASK_WAKING) {
        struct migration_arg arg = { p, dest_cpu };
        /* Need help from migration thread: drop lock and wait. */
        task_rq_unlock(rq, p, &rf);
        stop_one_cpu(cpu_of(rq), migration_cpu_stop, &arg);
        return 0;
    } else if (task_on_rq_queued(p)) {
        /*
         * OK, since we're going to drop the lock immediately
         * afterwards anyway.
         */
        rq = move_queued_task(rq, &rf, p, dest_cpu);
    }
out:
    task_rq_unlock(rq, p, &rf);

    return ret;
}

int set_cpus_allowed_ptr(struct task_struct *p, const struct cpumask *new_mask)
{
    return __set_cpus_allowed_ptr(p, new_mask, false);
}

void set_task_cpu(struct task_struct *p, unsigned int new_cpu)
{
#ifdef SCHED_DEBUG
    /*
     * We should never call set_task_cpu() on a blocked task,
     * ttwu() will sort out the placement.
     */
    WARN_ON_ONCE(p->state != TASK_RUNNING && p->state != TASK_WAKING &&
            !p->on_rq);

    /*
     * Migrating fair class task must have p->on_rq = TASK_ON_RQ_MIGRATING,
     * because schedstat_wait_{start,end} rebase migrating task's wait_start
     * time relying on p->on_rq.
     */
    WARN_ON_ONCE(p->state == TASK_RUNNING &&
             p->sched_class == &fair_sched_class &&
             (p->on_rq && !task_on_rq_migrating(p)));
    /*
     * Clearly, migrating tasks to offline CPUs is a fairly daft thing.
     */
    WARN_ON_ONCE(!cpu_online(new_cpu));
#endif
    if (task_cpu(p) != new_cpu) {
        if (p->sched_class->migrate_task_rq)
            p->sched_class->migrate_task_rq(p, new_cpu);
        p->se.nr_migrations++;
    }

    __set_task_cpu(p, new_cpu);
}

/*
 * wait_task_inactive - wait for a thread to unschedule.
 *
 * If @match_state is nonzero, it's the @p->state value just checked and
 * not expected to change.  If it changes, i.e. @p might have woken up,
 * then return zero.  When we succeed in waiting for @p to be off its CPU,
 * we return a positive number (its total switch count).  If a second call
 * a short while later returns the same number, the caller can be sure that
 * @p has remained unscheduled the whole time.
 *
 * The caller must ensure that the task *will* unschedule sometime soon,
 * else this function might spin for a *long* time. This function can't
 * be called with interrupts off, or it may introduce deadlock with
 * smp_call_function() if an IPI is sent by the same process we are
 * waiting to become inactive.
 */
unsigned long wait_task_inactive(struct task_struct *p, long match_state)
{
    int running, queued;
    struct rq_flags rf;
    unsigned long ncsw;
    struct rq *rq;

    for (;;) {
        /*
         * We do the initial early heuristics without holding
         * any task-queue locks at all. We'll only try to get
         * the runqueue lock when things look like they will
         * work out!
         */
        rq = task_rq(p);

        /*
         * If the task is actively running on another CPU
         * still, just relax and busy-wait without holding
         * any locks.
         *
         * NOTE! Since we don't hold any locks, it's not
         * even sure that "rq" stays as the right runqueue!
         * But we don't care, since "task_running()" will
         * return false if the runqueue has changed and p
         * is actually now running somewhere else!
         */
        while (task_running(rq, p)) {
            if (match_state && unlikely(p->state != match_state))
                return 0;
            cpu_relax();
        }

        /*
         * Ok, time to look more closely! We need the rq
         * lock now, to be *sure*. If we're wrong, we'll
         * just go back and repeat.
         */
        rq = task_rq_lock(p, &rf);
        running = task_running(rq, p);
        queued = task_on_rq_queued(p);
        ncsw = 0;
        if (!match_state || p->state == match_state)
            ncsw = p->nvcsw | LONG_MIN; /* sets MSB */
        task_rq_unlock(rq, p, &rf);

        /*
         * If it changed from the expected state, bail out now.
         */
        if (unlikely(!ncsw))
            break;

        /*
         * Was it really running after all now that we
         * checked with the proper locks actually held?
         *
         * Oops. Go back and try again..
         */
        if (unlikely(running)) {
            cpu_relax();
            continue;
        }

        /*
         * It's not enough that it's not actively running,
         * it must be off the runqueue _entirely_, and not
         * preempted!
         *
         * So if it was still runnable (but just not actively
         * running right now), it's preempted, and we should
         * yield - it could be a while.
         */
        if (unlikely(queued)) {
            ktime_t to = NSEC_PER_SEC / HZ;

            set_current_state(TASK_UNINTERRUPTIBLE);
            schedule_hrtimeout(&to, HRTIMER_MODE_REL);
            continue;
        }

        /*
         * Ahh, all good. It wasn't running, and it wasn't
         * runnable, which means that it will never become
         * running in the future either. We're all done!
         */
        break;
    }

    return ncsw;
}

/***
 * kick_process - kick a running thread to enter/exit the kernel
 * @p: the to-be-kicked thread
 *
 * Cause a process which is running on another CPU to enter
 * kernel-mode, without any delay. (to get signals handled.)
 *
 * NOTE: this function doesn't have to take the runqueue lock,
 * because all it wants to ensure is that the remote task enters
 * the kernel. If the IPI races and the task has been migrated
 * to another CPU then no harm is done and the purpose has been
 * achieved as well.
 */
void kick_process(struct task_struct *p)
{
    int cpu;

    preempt_disable();
    cpu = task_cpu(p);
    if ((cpu != smp_processor_id()) && task_curr(p))
        smp_send_reschedule(cpu);
    preempt_enable();
}

/*
 * ->cpus_allowed is protected by both rq->lock and p->pi_lock
 *
 * A few notes on cpu_active vs cpu_online:
 *
 *  - cpu_active must be a subset of cpu_online
 *
 *  - on CPU-up we allow per-CPU kthreads on the online && !active CPU,
 *    see __set_cpus_allowed_ptr(). At this point the newly online
 *    CPU isn't yet part of the sched domains, and balancing will not
 *    see it.
 *
 *  - on CPU-down we clear cpu_active() to mask the sched domains and
 *    avoid the load balancer to place new tasks on the to be removed
 *    CPU. Existing tasks will remain running there and will be taken
 *    off.
 *
 * This means that fallback selection must not select !active CPUs.
 * And can assume that any active CPU must be online. Conversely
 * select_task_rq() below may allow selection of !active CPUs in order
 * to satisfy the above rules.
 */
static int select_fallback_rq(int cpu, struct task_struct *p)
{
    enum { possible, fail } state = possible;
    int dest_cpu;

    for (;;) {
        /* Any allowed, online CPU? */
        for_each_cpu(dest_cpu, &p->cpus_allowed) {
            if (!is_cpu_allowed(p, dest_cpu))
                continue;

            goto out;
        }

        /* No more Mr. Nice Guy. */
        switch (state) {
        case possible:
            do_set_cpus_allowed(p, cpu_possible_mask);
            state = fail;
            break;

        case fail:
            BUG();
            break;
        }
    }

out:
    /*
     * Don't tell them about moving exiting tasks or
     * kernel threads (both mm NULL), since they never
     * leave kernel.
     */
    if (p->mm && printk_ratelimit()) {
        printk("process %d (%s) no longer affine to cpu%d\n",
                task_pid_nr(p), p->comm, cpu);
    }

    return dest_cpu;
}

/*
 * The caller (fork, wakeup) owns p->pi_lock, ->cpus_allowed is stable.
 */
static inline
int select_task_rq(struct task_struct *p, int cpu, int sd_flags, int wake_flags)
{
    if (p->nr_cpus_allowed > 1)
        cpu = p->sched_class->select_task_rq(p, cpu, sd_flags, wake_flags);
    else
        cpu = cpumask_any(&p->cpus_allowed);

    /*
     * In order not to call set_task_cpu() on a blocking task we need
     * to rely on ttwu() to place the task on a valid ->cpus_allowed
     * CPU.
     *
     * Since this is common to all placement strategies, this lives here.
     *
     * [ this allows ->select_task() to simply return task_cpu(p) and
     *   not worry about this generic constraint ]
     */
    if (unlikely(!is_cpu_allowed(p, cpu)))
        cpu = select_fallback_rq(task_cpu(p), p);

    return cpu;
}

static void update_avg(u64 *avg, u64 sample)
{
    i64 diff = sample - *avg;
    *avg += diff >> 3;
}

void sched_set_stop_task(int cpu, struct task_struct *stop)
{
    struct sched_param param = { .sched_priority = MAX_RT_PRIO - 1 };
    struct task_struct *old_stop = cpu_rq(cpu)->stop;

    if (stop) {
        /*
         * Make it appear like a SCHED_FIFO task, its something
         * userspace knows about and won't get confused about.
         *
         * Also, it will make PI more or less work without too
         * much confusion -- but then, stop work should not
         * rely on PI working anyway.
         */
        sched_setscheduler_nocheck(stop, SCHED_FIFO, &param);

        stop->sched_class = &stop_sched_class;
    }

    cpu_rq(cpu)->stop = stop;

    if (old_stop) {
        /*
         * Reset it back to a normal scheduling class so that
         * it can die in pieces.
         */
        old_stop->sched_class = &rt_sched_class;
    }
}

static inline void ttwu_activate(struct rq *rq, struct task_struct *p, int en_flags)
{
    activate_task(rq, p, en_flags);
    p->on_rq = TASK_ON_RQ_QUEUED;
}

/*
 * Mark the task runnable and perform wakeup-preemption.
 */
static void ttwu_do_wakeup(struct rq *rq, struct task_struct *p, int wake_flags,
               struct rq_flags *rf)
{
    check_preempt_curr(rq, p, wake_flags);
    p->state = TASK_RUNNING;

    if (p->sched_class->task_woken) {
        /*
         * Our task @p is fully woken up and running; so its safe to
         * drop the rq->lock, hereafter rq is only used for statistics.
         */
        rq_unpin_lock(rq, rf);
        p->sched_class->task_woken(rq, p);
        rq_repin_lock(rq, rf);
    }

    if (rq->idle_stamp) {
        u64 delta = rq_clock(rq) - rq->idle_stamp;
        u64 max = 2*rq->max_idle_balance_cost;

        update_avg(&rq->avg_idle, delta);

        if (rq->avg_idle > max)
            rq->avg_idle = max;

        rq->idle_stamp = 0;
    }
}

static void
ttwu_do_activate(struct rq *rq, struct task_struct *p, int wake_flags,
         struct rq_flags *rf)
{
    int en_flags = ENQUEUE_WAKEUP | ENQUEUE_NOCLOCK;

    if (p->sched_contributes_to_load)
        rq->nr_uninterruptible--;

    if (wake_flags & WF_MIGRATED)
        en_flags |= ENQUEUE_MIGRATED;

    ttwu_activate(rq, p, en_flags);
    ttwu_do_wakeup(rq, p, wake_flags, rf);
}

/*
 * Called in case the task @p isn't fully descheduled from its runqueue,
 * in this case we must do a remote wakeup. Its a 'light' wakeup though,
 * since all we need to do is flip p->state to TASK_RUNNING, since
 * the task is still ->on_rq.
 */
static int ttwu_remote(struct task_struct *p, int wake_flags)
{
    struct rq_flags rf;
    struct rq *rq;
    int ret = 0;

    rq = __task_rq_lock(p, &rf);
    if (task_on_rq_queued(p)) {
        /* check_preempt_curr() may use rq clock */
        update_rq_clock(rq);
        ttwu_do_wakeup(rq, p, wake_flags, &rf);
        ret = 1;
    }
    __task_rq_unlock(rq, &rf);

    return ret;
}

void sched_ttwu_pending(void)
{
    struct rq *rq = this_rq();
    struct llist_node *llist = llist_del_all(&rq->wake_list);
    struct task_struct *p, *t;
    struct rq_flags rf;

    if (!llist)
        return;

    rq_lock_irqsave(rq, &rf);
    update_rq_clock(rq);

    llist_for_each_entry_safe(p, t, llist, wake_entry)
        ttwu_do_activate(rq, p, p->sched_remote_wakeup ? WF_MIGRATED : 0, &rf);

    rq_unlock_irqrestore(rq, &rf);
}

void scheduler_ipi(void)
{
    /*
     * Fold TIF_NEED_RESCHED into the preempt_count; anybody setting
     * TIF_NEED_RESCHED remotely (for the first time) will also send
     * this IPI.
     */
    preempt_fold_need_resched();

    if (llist_empty(&this_rq()->wake_list))
        return;

    /*
     * Not all reschedule IPI handlers call irq_enter/irq_exit, since
     * traditionally all their work was done from the interrupt return
     * path. Now that we actually do some work, we need to make sure
     * we do call them.
     *
     * Some archs already do call them, luckily irq_enter/exit nest
     * properly.
     *
     * Arguably we should visit all archs and update all handlers,
     * however a fair share of IPIs are still resched only so this would
     * somewhat pessimize the simple resched case.
     */
    irq_enter();
    sched_ttwu_pending();
    irq_exit();
}

static void ttwu_queue_remote(struct task_struct *p, int cpu, int wake_flags)
{
    struct rq *rq = cpu_rq(cpu);

    p->sched_remote_wakeup = !!(wake_flags & WF_MIGRATED);

    if (llist_add(&p->wake_entry, &cpu_rq(cpu)->wake_list)) {
        if (!set_nr_if_polling(rq->idle))
            smp_send_reschedule(cpu);
    }
}

void wake_up_if_idle(int cpu)
{
    struct rq *rq = cpu_rq(cpu);
    struct rq_flags rf;

    rcu_read_lock();

    if (!is_idle_task(rcu_dereference(rq->curr)))
        goto out;

    if (!set_nr_if_polling(rq->idle)) {
        rq_lock_irqsave(rq, &rf);
        if (is_idle_task(rq->curr))
            smp_send_reschedule(cpu);
        /* Else CPU is not idle, do nothing here: */
        rq_unlock_irqrestore(rq, &rf);
    }
out:
    rcu_read_unlock();
}

static void ttwu_queue(struct task_struct *p, int cpu, int wake_flags)
{
    struct rq *rq = cpu_rq(cpu);
    struct rq_flags rf;

    if (sched_feat(TTWU_QUEUE)) {
        sched_clock_cpu(cpu); /* Sync clocks across CPUs */
        ttwu_queue_remote(p, cpu, wake_flags);
        return;
    }

    rq_lock(rq, &rf);
    update_rq_clock(rq);
    ttwu_do_activate(rq, p, wake_flags, &rf);
    rq_unlock(rq, &rf);
}

/*
 * Notes on Program-Order guarantees on SMP systems.
 *
 *  MIGRATION
 *
 * The basic program-order guarantee on SMP systems is that when a task [t]
 * migrates, all its activity on its old CPU [c0] happens-before any subsequent
 * execution on its new CPU [c1].
 *
 * For migration (of runnable tasks) this is provided by the following means:
 *
 *  A) UNLOCK of the rq(c0)->lock scheduling out task t
 *  B) migration for t is required to synchronize *both* rq(c0)->lock and
 *     rq(c1)->lock (if not at the same time, then in that order).
 *  C) LOCK of the rq(c1)->lock scheduling in task
 *
 * Release/acquire chaining guarantees that B happens after A and C after B.
 * Note: the CPU doing B need not be c0 or c1
 *
 * Example:
 *
 *   CPU0            CPU1            CPU2
 *
 *   LOCK rq(0)->lock
 *   sched-out X
 *   sched-in Y
 *   UNLOCK rq(0)->lock
 *
 *                                   LOCK rq(0)->lock // orders against CPU0
 *                                   dequeue X
 *                                   UNLOCK rq(0)->lock
 *
 *                                   LOCK rq(1)->lock
 *                                   enqueue X
 *                                   UNLOCK rq(1)->lock
 *
 *                   LOCK rq(1)->lock // orders against CPU2
 *                   sched-out Z
 *                   sched-in X
 *                   UNLOCK rq(1)->lock
 *
 *
 *  BLOCKING -- aka. SLEEP + WAKEUP
 *
 * For blocking we (obviously) need to provide the same guarantee as for
 * migration. However the means are completely different as there is no lock
 * chain to provide order. Instead we do:
 *
 *   1) smp_store_release(X->on_cpu, 0)
 *   2) smp_cond_load_acquire(!X->on_cpu)
 *
 * Example:
 *
 *   CPU0 (schedule)  CPU1 (try_to_wake_up) CPU2 (schedule)
 *
 *   LOCK rq(0)->lock LOCK X->pi_lock
 *   dequeue X
 *   sched-out X
 *   smp_store_release(X->on_cpu, 0);
 *
 *                    smp_cond_load_acquire(&X->on_cpu, !VAL);
 *                    X->state = WAKING
 *                    set_task_cpu(X,2)
 *
 *                    LOCK rq(2)->lock
 *                    enqueue X
 *                    X->state = RUNNING
 *                    UNLOCK rq(2)->lock
 *
 *                                          LOCK rq(2)->lock // orders against CPU1
 *                                          sched-out Z
 *                                          sched-in X
 *                                          UNLOCK rq(2)->lock
 *
 *                    UNLOCK X->pi_lock
 *   UNLOCK rq(0)->lock
 *
 *
 * However, for wakeups there is a second guarantee we must provide, namely we
 * must ensure that CONDITION=1 done by the caller can not be reordered with
 * accesses to the task state; see try_to_wake_up() and set_current_state().
 */

/**
 * try_to_wake_up - wake up a thread
 * @p: the thread to be awakened
 * @state: the mask of task states that can be woken
 * @wake_flags: wake modifier flags (WF_*)
 *
 * If (@state & @p->state) @p->state = TASK_RUNNING.
 *
 * If the task was not queued/runnable, also place it back on a runqueue.
 *
 * Atomic against schedule() which would dequeue a task, also see
 * set_current_state().
 *
 * This function executes a full memory barrier before accessing the task
 * state; see set_current_state().
 *
 * Return: %true if @p->state changes (an actual wakeup was done),
 *	   %false otherwise.
 */
static int
try_to_wake_up(struct task_struct *p, unsigned int state, int wake_flags)
{
    unsigned long flags;
    int cpu, success = 0;

    /*
     * If we are going to wake up a thread waiting for CONDITION we
     * need to ensure that CONDITION=1 done by the caller can not be
     * reordered with p->state check below. This pairs with mb() in
     * set_current_state() the waiting thread does.
     */
    raw_spin_lock_irqsave(&p->pi_lock, flags);
    smp_mb__after_spinlock();
    if (!(p->state & state))
        goto out;

    /* We're going to change ->state: */
    success = 1;
    cpu = task_cpu(p);

    /*
     * Ensure we load p->on_rq _after_ p->state, otherwise it would
     * be possible to, falsely, observe p->on_rq == 0 and get stuck
     * in smp_cond_load_acquire() below.
     *
     * sched_ttwu_pending()			try_to_wake_up()
     *   STORE p->on_rq = 1			  LOAD p->state
     *   UNLOCK rq->lock
     *
     * __schedule() (switch to task 'p')
     *   LOCK rq->lock			  smp_rmb();
     *   smp_mb__after_spinlock();
     *   UNLOCK rq->lock
     *
     * [task p]
     *   STORE p->state = UNINTERRUPTIBLE	  LOAD p->on_rq
     *
     * Pairs with the LOCK+smp_mb__after_spinlock() on rq->lock in
     * __schedule().  See the comment for smp_mb__after_spinlock().
     */
    smp_rmb();
    if (p->on_rq && ttwu_remote(p, wake_flags))
        goto out;

    /*
     * Ensure we load p->on_cpu _after_ p->on_rq, otherwise it would be
     * possible to, falsely, observe p->on_cpu == 0.
     *
     * One must be running (->on_cpu == 1) in order to remove oneself
     * from the runqueue.
     *
     * __schedule() (switch to task 'p')	try_to_wake_up()
     *   STORE p->on_cpu = 1		  LOAD p->on_rq
     *   UNLOCK rq->lock
     *
     * __schedule() (put 'p' to sleep)
     *   LOCK rq->lock			  smp_rmb();
     *   smp_mb__after_spinlock();
     *   STORE p->on_rq = 0			  LOAD p->on_cpu
     *
     * Pairs with the LOCK+smp_mb__after_spinlock() on rq->lock in
     * __schedule().  See the comment for smp_mb__after_spinlock().
     */
    smp_rmb();

    /*
     * If the owning (remote) CPU is still in the middle of schedule() with
     * this task as prev, wait until its done referencing the task.
     *
     * Pairs with the smp_store_release() in finish_task().
     *
     * This ensures that tasks getting woken will be fully ordered against
     * their previous state and preserve Program Order.
     */
    smp_cond_load_acquire(&p->on_cpu, !VAL);

    p->sched_contributes_to_load = !!task_contributes_to_load(p);
    p->state = TASK_WAKING;

    if (p->in_iowait)
        atomic_dec(&task_rq(p)->nr_iowait);

    cpu = select_task_rq(p, p->wake_cpu, SD_BALANCE_WAKE, wake_flags);
    if (task_cpu(p) != cpu) {
        wake_flags |= WF_MIGRATED;
        set_task_cpu(p, cpu);
    }

    ttwu_queue(p, cpu, wake_flags);
out:
    raw_spin_unlock_irqrestore(&p->pi_lock, flags);

    return success;
}

/**
 * wake_up_process - Wake up a specific process
 * @p: The process to be woken up.
 *
 * Attempt to wake up the nominated process and move it to the set of runnable
 * processes.
 *
 * Return: 1 if the process was woken up, 0 if it was already running.
 *
 * This function executes a full memory barrier before accessing the task state.
 */
int wake_up_process(struct task_struct *p)
{
    return try_to_wake_up(p, TASK_NORMAL, 0);
}

int wake_up_state(struct task_struct *p, unsigned int state)
{
    return try_to_wake_up(p, state, 0);
}

/*
 * Perform scheduler related setup for a newly forked process p.
 * p is forked by current.
 *
 * __sched_fork() is basic setup used by init_idle() too:
 */
static void __sched_fork(unsigned long clone_flags, struct task_struct *p)
{
    p->on_rq			= 0;

    p->se.on_rq			= 0;
    p->se.exec_start		= 0;
    p->se.sum_exec_runtime		= 0;
    p->se.prev_sum_exec_runtime	= 0;
    p->se.nr_migrations		= 0;
    p->se.vruntime			= 0;
    INIT_LIST_HEAD(&p->se.group_node);

    RB_CLEAR_NODE(&p->dl.rb_node);
    init_dl_task_timer(&p->dl);
    init_dl_inactive_task_timer(&p->dl);
    __dl_clear_params(p);

    INIT_LIST_HEAD(&p->rt.run_list);
    p->rt.time_slice	= sched_rr_timeslice;
    p->rt.on_rq		= 0;
    p->rt.on_list		= 0;
}

/*
 * fork()/clone()-time setup:
 */
int sched_fork(unsigned long clone_flags, struct task_struct *p)
{
    unsigned long flags;

    __sched_fork(clone_flags, p);
    /*
     * We mark the process as NEW here. This guarantees that
     * nobody will actually run it, and a signal or other external
     * event cannot wake it up and insert it on the runqueue either.
     */
    p->state = TASK_NEW;

    /*
     * Make sure we do not leak PI boosting priority to the child.
     */
    p->prio = current->normal_prio;

    /*
     * Revert to default priority/policy on fork if requested.
     */
    if (unlikely(p->sched_reset_on_fork)) {
        if (task_has_dl_policy(p) || task_has_rt_policy(p)) {
            p->policy = SCHED_NORMAL;
            p->static_prio = NICE_TO_PRIO(0);
            p->rt_priority = 0;
        } else if (PRIO_TO_NICE(p->static_prio) < 0)
            p->static_prio = NICE_TO_PRIO(0);

        p->prio = p->normal_prio = __normal_prio(p);
        set_load_weight(p, false);

        /*
         * We don't need the reset flag anymore after the fork. It has
         * fulfilled its duty:
         */
        p->sched_reset_on_fork = 0;
    }

    if (dl_prio(p->prio))
        return -EAGAIN;
    else if (rt_prio(p->prio))
        p->sched_class = &rt_sched_class;
    else
        p->sched_class = &fair_sched_class;

    init_entity_runnable_average(&p->se);

    /*
     * The child is not yet in the pid-hash so no cgroup attach races,
     * and the cgroup is pinned to this child due to cgroup_fork()
     * is ran before sched_fork().
     *
     * Silence PROVE_RCU.
     */
    raw_spin_lock_irqsave(&p->pi_lock, flags);
    /*
     * We're setting the CPU for the first time, we don't migrate,
     * so use __set_task_cpu().
     */
    __set_task_cpu(p, smp_processor_id());
    if (p->sched_class->task_fork)
        p->sched_class->task_fork(p);
    raw_spin_unlock_irqrestore(&p->pi_lock, flags);

    p->on_cpu = 0;
    init_task_preempt_count(p);
    plist_node_init(&p->pushable_tasks, MAX_PRIO);
    RB_CLEAR_NODE(&p->pushable_dl_tasks);
    return 0;
}

unsigned long to_ratio(u64 period, u64 runtime)
{
    if (runtime == RUNTIME_INF)
        return BW_UNIT;

    /*
     * Doing this here saves a lot of checks in all
     * the calling paths, and returning zero seems
     * safe for them anyway.
     */
    if (period == 0)
        return 0;

    return div64_u64(runtime << BW_SHIFT, period);
}

/*
 * wake_up_new_task - wake up a newly created task for the first time.
 *
 * This function will do some initial scheduler statistics housekeeping
 * that must be done for every newly created context, then puts the task
 * on the runqueue and wakes it.
 */
void wake_up_new_task(struct task_struct *p)
{
    struct rq_flags rf;
    struct rq *rq;

    raw_spin_lock_irqsave(&p->pi_lock, rf.flags);
    p->state = TASK_RUNNING;
    /*
     * Fork balancing, do it here and not earlier because:
     *  - cpus_allowed can change in the fork path
     *  - any previously selected CPU might disappear through hotplug
     *
     * Use __set_task_cpu() to avoid calling sched_class::migrate_task_rq,
     * as we're not fully set-up yet.
     */
    p->recent_used_cpu = task_cpu(p);
    __set_task_cpu(p, select_task_rq(p, task_cpu(p), SD_BALANCE_FORK, 0));
    rq = __task_rq_lock(p, &rf);
    update_rq_clock(rq);
    post_init_entity_util_avg(&p->se);

    activate_task(rq, p, ENQUEUE_NOCLOCK);
    p->on_rq = TASK_ON_RQ_QUEUED;
    check_preempt_curr(rq, p, WF_FORK);
    if (p->sched_class->task_woken) {
        /*
         * Nothing relies on rq->lock after this, so its fine to
         * drop it.
         */
        rq_unpin_lock(rq, &rf);
        p->sched_class->task_woken(rq, p);
        rq_repin_lock(rq, &rf);
    }
    task_rq_unlock(rq, p, &rf);
}

static inline void prepare_task(struct task_struct *next)
{
    /*
     * Claim the task as running, we do this before switching to it
     * such that any running task will have this set.
     */
    next->on_cpu = 1;
}

static inline void finish_task(struct task_struct *prev)
{
    /*
     * After ->on_cpu is cleared, the task can be moved to a different CPU.
     * We must ensure this doesn't happen until the switch is completely
     * finished.
     *
     * In particular, the load of prev->state in finish_task_switch() must
     * happen before this.
     *
     * Pairs with the smp_cond_load_acquire() in try_to_wake_up().
     */
    smp_store_release(&prev->on_cpu, 0);
}

static inline void
prepare_lock_switch(struct rq *rq, struct task_struct *next, struct rq_flags *rf)
{
    /*
     * Since the runqueue lock will be released by the next
     * task (which is an invalid locking op but in the case
     * of the scheduler it's an obvious special-case), so we
     * do an early lockdep release here:
     */
    rq_unpin_lock(rq, rf);
}

static inline void finish_lock_switch(struct rq *rq)
{
    /*
     * If we are tracking spinlock dependencies then we have to
     * fix up the runqueue lock - which gets 'carried over' from
     * prev into current:
     */
    raw_spin_unlock_irq(&rq->lock);
}

/*
 * NOP if the arch has not defined these:
 */

#ifndef prepare_arch_switch
# define prepare_arch_switch(next)	do { } while (0)
#endif

#ifndef finish_arch_post_lock_switch
# define finish_arch_post_lock_switch()	do { } while (0)
#endif

/**
 * prepare_task_switch - prepare to switch tasks
 * @rq: the runqueue preparing to switch
 * @prev: the current task that is being switched out
 * @next: the task we are going to switch to.
 *
 * This is called with the rq lock held and interrupts off. It must
 * be paired with a subsequent finish_task_switch after the context
 * switch.
 *
 * prepare_task_switch sets up locking and calls architecture specific
 * hooks.
 */
static inline void
prepare_task_switch(struct rq *rq, struct task_struct *prev,
            struct task_struct *next)
{
    prepare_task(next);
    prepare_arch_switch(next);
}

/**
 * finish_task_switch - clean up after a task-switch
 * @prev: the thread we just switched away from.
 *
 * finish_task_switch must be called after the context switch, paired
 * with a prepare_task_switch call before the context switch.
 * finish_task_switch will reconcile locking set up by prepare_task_switch,
 * and do any other architecture-specific cleanup actions.
 *
 * Note that we may have delayed dropping an mm in context_switch(). If
 * so, we finish that here outside of the runqueue lock. (Doing it
 * with the lock held can cause deadlocks; see schedule() for
 * details.)
 *
 * The context switch have flipped the stack from under us and restored the
 * local variables which were saved when this task called schedule() in the
 * past. prev == current is still correct but we need to recalculate this_rq
 * because prev may have moved to another CPU.
 */
static struct rq *finish_task_switch(struct task_struct *prev)
    __releases(rq->lock)
{
    struct rq *rq = this_rq();
    struct mm_struct *mm = rq->prev_mm;
    long prev_state;

    /*
     * The previous task will have left us with a preempt_count of 2
     * because it left us after:
     *
     *	schedule()
     *	  preempt_disable();			// 1
     *	  __schedule()
     *	    raw_spin_lock_irq(&rq->lock)	// 2
     *
     * Also, see FORK_PREEMPT_COUNT.
     */
    if (WARN_ONCE(preempt_count() != 2*PREEMPT_DISABLE_OFFSET,
              "corrupted preempt_count: %s/%d/0x%x\n",
              current->comm, current->pid, preempt_count()))
        preempt_count_set(FORK_PREEMPT_COUNT);

    rq->prev_mm = NULL;

    /*
     * A task struct has one reference for the use as "current".
     * If a task dies, then it sets TASK_DEAD in tsk->state and calls
     * schedule one last time. The schedule call will never return, and
     * the scheduled task must drop that reference.
     *
     * We must observe prev->state before clearing prev->on_cpu (in
     * finish_task), otherwise a concurrent wakeup can get prev
     * running on another CPU and we could rave with its RUNNING -> DEAD
     * transition, resulting in a double drop.
     */
    prev_state = prev->state;
    finish_task(prev);
    finish_lock_switch(rq);
    finish_arch_post_lock_switch();

    /*
     * When switching through a kernel thread, the loop in
     * membarrier_{private,global}_expedited() may have observed that
     * kernel thread and not issued an IPI. It is therefore possible to
     * schedule between user->kernel->user threads without passing though
     * switch_mm(). Membarrier requires a barrier after storing to
     * rq->curr, before returning to userspace, so provide them here:
     *
     * - a full memory barrier for {PRIVATE,GLOBAL}_EXPEDITED, implicitly
     *   provided by mmdrop(),
     * - a sync_core for SYNC_CORE.
     */
    if (mm)
        mmdrop(mm);

    if (unlikely(prev_state == TASK_DEAD)) {
        if (prev->sched_class->task_dead)
            prev->sched_class->task_dead(prev);

        /* Task is done with its stack. */
        put_task_stack(prev);
        put_task_struct(prev);
    }

    return rq;
}

/* rq->lock is NOT held, but preemption is disabled */
static void __balance_callback(struct rq *rq)
{
    struct callback_head *head, *next;
    void (*func)(struct rq *rq);
    unsigned long flags;

    raw_spin_lock_irqsave(&rq->lock, flags);
    head = rq->balance_callback;
    rq->balance_callback = NULL;
    while (head) {
        func = (void (*)(struct rq *))head->func;
        next = head->next;
        head->next = NULL;
        head = next;

        func(rq);
    }
    raw_spin_unlock_irqrestore(&rq->lock, flags);
}

static inline void balance_callback(struct rq *rq)
{
    if (unlikely(rq->balance_callback))
        __balance_callback(rq);
}

/**
 * schedule_tail - first thing a freshly forked thread must call.
 * @prev: the thread we just switched away from.
 */
asmlinkage __visible void schedule_tail(struct task_struct *prev)
    __releases(rq->lock)
{
    struct rq *rq;

    /*
     * New tasks start with FORK_PREEMPT_COUNT, see there and
     * finish_task_switch() for details.
     *
     * finish_task_switch() will drop rq->lock() and lower preempt_count
     * and the preempt_enable() will end up enabling preemption (on
     * PREEMPT_COUNT kernels).
     */

    rq = finish_task_switch(prev);
    balance_callback(rq);
    preempt_enable();

    if (current->set_child_tid)
        put_user(task_pid_nr(current), current->set_child_tid);

    calculate_sigpending();
}

/*
 * context_switch - switch to the new MM and the new thread's register state.
 */
static __always_inline struct rq *
context_switch(struct rq *rq, struct task_struct *prev,
           struct task_struct *next, struct rq_flags *rf)
{
    struct mm_struct *mm, *oldmm;

    prepare_task_switch(rq, prev, next);

    mm = next->mm;
    oldmm = prev->active_mm;

    /*
     * If mm is non-NULL, we pass through switch_mm(). If mm is
     * NULL, we will pass through mmdrop() in finish_task_switch().
     * Both of these contain the full memory barrier required by
     * membarrier after storing to rq->curr, before returning to
     * user-space.
     */
    if (!mm) {
        next->active_mm = oldmm;
        mmgrab(oldmm);
        enter_lazy_tlb(oldmm, next);
    } else
        switch_mm(oldmm, mm, next);

    if (!prev->mm) {
        prev->active_mm = NULL;
        rq->prev_mm = oldmm;
    }

    rq->clock_update_flags &= ~(RQCF_ACT_SKIP|RQCF_REQ_SKIP);

    prepare_lock_switch(rq, next, rf);

    /* Here we just switch the register state and the stack. */
    switch_to(prev, next, prev);
    barrier();

    return finish_task_switch(prev);
}

/*
 * nr_running and nr_context_switches:
 *
 * externally visible scheduler statistics: current number of runnable
 * threads, total number of context switches performed since bootup.
 */
unsigned long nr_running(void)
{
    unsigned long i, sum = 0;

    for_each_online_cpu(i)
        sum += cpu_rq(i)->nr_running;

    return sum;
}

/*
 * Check if only the current task is running on the CPU.
 *
 * Caution: this function does not check that the caller has disabled
 * preemption, thus the result might have a time-of-check-to-time-of-use
 * race.  The caller is responsible to use it correctly, for example:
 *
 * - from a non-preemptible section (of course)
 *
 * - from a thread that is bound to a single CPU
 *
 * - in a loop with very short iterations (e.g. a polling loop)
 */
bool single_task_running(void)
{
    return raw_rq()->nr_running == 1;
}

unsigned long long nr_context_switches(void)
{
    int i;
    unsigned long long sum = 0;

    for_each_possible_cpu(i)
        sum += cpu_rq(i)->nr_switches;

    return sum;
}

/*
 * Consumers of these two interfaces, like for example the cpuidle menu
 * governor, are using nonsensical data. Preferring shallow idle state selection
 * for a CPU that has IO-wait which might not even end up running the task when
 * it does become runnable.
 */

unsigned long nr_iowait_cpu(int cpu)
{
    return atomic_read(&cpu_rq(cpu)->nr_iowait);
}

/*
 * IO-wait accounting, and how its mostly bollocks (on SMP).
 *
 * The idea behind IO-wait account is to account the idle time that we could
 * have spend running if it were not for IO. That is, if we were to improve the
 * storage performance, we'd have a proportional reduction in IO-wait time.
 *
 * This all works nicely on UP, where, when a task blocks on IO, we account
 * idle time as IO-wait, because if the storage were faster, it could've been
 * running and we'd not be idle.
 *
 * This has been extended to SMP, by doing the same for each CPU. This however
 * is broken.
 *
 * Imagine for instance the case where two tasks block on one CPU, only the one
 * CPU will have IO-wait accounted, while the other has regular idle. Even
 * though, if the storage were faster, both could've ran at the same time,
 * utilising both CPUs.
 *
 * This means, that when looking globally, the current IO-wait accounting on
 * SMP is a lower bound, by reason of under accounting.
 *
 * Worse, since the numbers are provided per CPU, they are sometimes
 * interpreted per CPU, and that is nonsensical. A blocked task isn't strictly
 * associated with any one particular CPU, it can wake to another CPU than it
 * blocked on. This means the per CPU IO-wait number is meaningless.
 *
 * Task CPU affinities can make all that even more 'interesting'.
 */

unsigned long nr_iowait(void)
{
    unsigned long i, sum = 0;

    for_each_possible_cpu(i)
        sum += nr_iowait_cpu(i);

    return sum;
}

/*
 * sched_exec - execve() is a valuable balancing opportunity, because at
 * this point the task has the smallest effective memory and cache footprint.
 */
void sched_exec(void)
{
    struct task_struct *p = current;
    unsigned long flags;
    int dest_cpu;

    raw_spin_lock_irqsave(&p->pi_lock, flags);
    dest_cpu = p->sched_class->select_task_rq(p, task_cpu(p), SD_BALANCE_EXEC, 0);
    if (dest_cpu == smp_processor_id())
        goto unlock;

    if (likely(cpu_online(dest_cpu))) {
        struct migration_arg arg = { p, dest_cpu };

        raw_spin_unlock_irqrestore(&p->pi_lock, flags);
        stop_one_cpu(task_cpu(p), migration_cpu_stop, &arg);
        return;
    }
unlock:
    raw_spin_unlock_irqrestore(&p->pi_lock, flags);
}

/*
 * The function fair_sched_class.update_curr accesses the struct curr
 * and its field curr->exec_start; when called from task_sched_runtime(),
 * we observe a high rate of cache misses in practice.
 * Prefetching this data results in improved performance.
 */
static inline void prefetch_curr_exec_start(struct task_struct *p)
{
    struct sched_entity *curr = (&task_rq(p)->cfs)->curr;

    prefetch(curr);
    prefetch(&curr->exec_start);
}

/*
 * Return accounted runtime for the task.
 * In case the task is currently running, return the runtime plus current's
 * pending runtime that have not been accounted yet.
 */
unsigned long long task_sched_runtime(struct task_struct *p)
{
    struct rq_flags rf;
    struct rq *rq;
    u64 ns;

#if defined(CONFIG_64BIT)
    /*
     * 64-bit doesn't need locks to atomically read a 64-bit value.
     * So we have a optimization chance when the task's delta_exec is 0.
     * Reading ->on_cpu is racy, but this is ok.
     *
     * If we race with it leaving CPU, we'll take a lock. So we're correct.
     * If we race with it entering CPU, unaccounted time is 0. This is
     * indistinguishable from the read occurring a few cycles earlier.
     * If we see ->on_cpu without ->on_rq, the task is leaving, and has
     * been accounted, so we're correct here as well.
     */
    if (!p->on_cpu || !task_on_rq_queued(p))
        return p->se.sum_exec_runtime;
#endif

    rq = task_rq_lock(p, &rf);
    /*
     * Must be ->curr _and_ ->on_rq.  If dequeued, we would
     * project cycles that may never be accounted to this
     * thread, breaking clock_gettime().
     */
    if (task_current(rq, p) && task_on_rq_queued(p)) {
        prefetch_curr_exec_start(p);
        update_rq_clock(rq);
        p->sched_class->update_curr(rq);
    }
    ns = p->se.sum_exec_runtime;
    task_rq_unlock(rq, p, &rf);

    return ns;
}

/*
 * This function gets called by the timer code, with HZ frequency.
 * We call it with interrupts disabled.
 */
void scheduler_tick(void)
{
    int cpu = smp_processor_id();
    struct rq *rq = cpu_rq(cpu);
    struct task_struct *curr = rq->curr;
    struct rq_flags rf;

    rq_lock(rq, &rf);

    update_rq_clock(rq);
    curr->sched_class->task_tick(rq, curr, 0);
    cpu_load_update_active(rq);
    calc_global_load_tick(rq);

    rq_unlock(rq, &rf);

    trigger_load_balance(rq);
}

/*
 * Print scheduling while atomic bug:
 */
static noinline void __schedule_bug(struct task_struct *prev)
{
    printk(KERN_ERR "BUG: scheduling while atomic: %s/%d/0x%08x\n",
        prev->comm, prev->pid, preempt_count());

    dump_stack();
}

/*
 * Various schedule()-time debugging checks and statistics:
 */
static inline void schedule_debug(struct task_struct *prev)
{
#ifdef CONFIG_SCHED_STACK_END_CHECK
    if (task_stack_end_corrupted(prev))
        panic("corrupted stack end detected inside scheduler\n");
#endif

    if (unlikely(in_atomic_preempt_off())) {
        __schedule_bug(prev);
        preempt_count_set(PREEMPT_DISABLED);
    }
    rcu_sleep_check();
}

/*
 * Pick up the highest-prio task:
 */
static inline struct task_struct *
pick_next_task(struct rq *rq, struct task_struct *prev, struct rq_flags *rf)
{
    const struct sched_class *class;
    struct task_struct *p;

    /*
     * Optimization: we know that if all tasks are in the fair class we can
     * call that function directly, but only if the @prev task wasn't of a
     * higher scheduling class, because otherwise those loose the
     * opportunity to pull in more work from other CPUs.
     */
    if (likely((prev->sched_class == &idle_sched_class ||
            prev->sched_class == &fair_sched_class) &&
           rq->nr_running == rq->cfs.h_nr_running)) {

        p = fair_sched_class.pick_next_task(rq, prev, rf);
        if (unlikely(p == RETRY_TASK))
            goto again;

        /* Assumes fair_sched_class->next == idle_sched_class */
        if (unlikely(!p))
            p = idle_sched_class.pick_next_task(rq, prev, rf);

        return p;
    }

again:
    for_each_class(class) {
        p = class->pick_next_task(rq, prev, rf);
        if (p) {
            if (unlikely(p == RETRY_TASK))
                goto again;
            return p;
        }
    }

    /* The idle class should always have a runnable task: */
    BUG();
}

/*
 * __schedule() is the main scheduler function.
 *
 * The main means of driving the scheduler and thus entering this function are:
 *
 *   1. Explicit blocking: mutex, semaphore, waitqueue, etc.
 *
 *   2. TIF_NEED_RESCHED flag is checked on interrupt and userspace return
 *      paths. For example, see arch/x86/entry_64.S.
 *
 *      To drive preemption between tasks, the scheduler sets the flag in timer
 *      interrupt handler scheduler_tick().
 *
 *   3. Wakeups don't really cause entry into schedule(). They add a
 *      task to the run-queue and that's it.
 *
 *      Now, if the new task added to the run-queue preempts the current
 *      task, then the wakeup sets TIF_NEED_RESCHED and schedule() gets
 *      called on the nearest possible occasion:
 *
 *       - If the kernel is preemptible (CONFIG_PREEMPT=y):
 *
 *         - in syscall or exception context, at the next outmost
 *           preempt_enable(). (this might be as soon as the wake_up()'s
 *           spin_unlock()!)
 *
 *         - in IRQ context, return from interrupt-handler to
 *           preemptible context
 *
 *       - If the kernel is not preemptible (CONFIG_PREEMPT is not set)
 *         then at the next:
 *
 *          - cond_resched() call
 *          - explicit schedule() call
 *          - return from syscall or exception to user-space
 *          - return from interrupt-handler to user-space
 *
 * WARNING: must be called with preemption disabled!
 */
static void __sched __schedule(bool preempt)
{
    struct task_struct *prev, *next;
    unsigned long *switch_count;
    struct rq_flags rf;
    struct rq *rq;
    int cpu;

    cpu = smp_processor_id();
    rq = cpu_rq(cpu);
    prev = rq->curr;

    schedule_debug(prev);

    if (sched_feat(HRTICK))
        hrtick_clear(rq);

    local_irq_disable();
    rcu_note_context_switch(preempt);

    /*
     * Make sure that signal_pending_state()->signal_pending() below
     * can't be reordered with __set_current_state(TASK_INTERRUPTIBLE)
     * done by the caller to avoid the race with signal_wake_up().
     *
     * The membarrier system call requires a full memory barrier
     * after coming from user-space, before storing to rq->curr.
     */
    rq_lock(rq, &rf);
    smp_mb__after_spinlock();

    /* Promote REQ to ACT */
    rq->clock_update_flags <<= 1;
    update_rq_clock(rq);

    switch_count = &prev->nivcsw;
    if (!preempt && prev->state) {
        if (signal_pending_state(prev->state, prev)) {
            prev->state = TASK_RUNNING;
        } else {
            deactivate_task(rq, prev, DEQUEUE_SLEEP | DEQUEUE_NOCLOCK);
            prev->on_rq = 0;

            if (prev->in_iowait)
                atomic_inc(&rq->nr_iowait);
        }
        switch_count = &prev->nvcsw;
    }

    next = pick_next_task(rq, prev, &rf);
    clear_tsk_need_resched(prev);
    clear_preempt_need_resched();

    if (likely(prev != next)) {
        rq->nr_switches++;
        rq->curr = next;
        /*
         * The membarrier system call requires each architecture
         * to have a full memory barrier after updating
         * rq->curr, before returning to user-space.
         *
         * Here are the schemes providing that barrier on the
         * various architectures:
         * - mm ? switch_mm() : mmdrop() for x86, s390, sparc, PowerPC.
         *   switch_mm() rely on membarrier_arch_switch_mm() on PowerPC.
         * - finish_lock_switch() for weakly-ordered
         *   architectures where spin_unlock is a full barrier,
         * - switch_to() for arm64 (weakly-ordered, spin_unlock
         *   is a RELEASE barrier),
         */
        ++*switch_count;

        /* Also unlocks the rq: */
        rq = context_switch(rq, prev, next, &rf);
    } else {
        rq->clock_update_flags &= ~(RQCF_ACT_SKIP|RQCF_REQ_SKIP);
        rq_unlock_irq(rq, &rf);
    }

    balance_callback(rq);
}

void __noreturn do_task_dead(void)
{
    /* Causes final put_task_struct in finish_task_switch(): */
    set_special_state(TASK_DEAD);

    /* Tell freezer to ignore us: */
    current->flags |= PF_NOFREEZE;

    __schedule(false);
    BUG();

    /* Avoid "noreturn function does return" - but don't continue if BUG() is a NOP: */
    for (;;)
        cpu_relax();
}

asmlinkage __visible void __sched schedule(void)
{
    do {
        preempt_disable();
        __schedule(false);
        sched_preempt_enable_no_resched();
    } while (need_resched());
}

/*
 * synchronize_rcu_tasks() makes sure that no task is stuck in preempted
 * state (have scheduled out non-voluntarily) by making sure that all
 * tasks have either left the run queue or have gone into user space.
 * As idle tasks do not do either, they must not ever be preempted
 * (schedule out non-voluntarily).
 *
 * schedule_idle() is similar to schedule_preempt_disable() except that it
 * never enables preemption because it does not call sched_submit_work().
 */
void __sched schedule_idle(void)
{
    /*
     * As this skips calling sched_submit_work(), which the idle task does
     * regardless because that function is a nop when the task is in a
     * TASK_RUNNING state, make sure this isn't used someplace that the
     * current task can be in any other state. Note, idle is always in the
     * TASK_RUNNING state.
     */
    WARN_ON_ONCE(current->state);
    do {
        __schedule(false);
    } while (need_resched());
}

/**
 * schedule_preempt_disabled - called with preemption disabled
 *
 * Returns with preemption disabled. Note: preempt_count must be 1
 */
void __sched schedule_preempt_disabled(void)
{
    sched_preempt_enable_no_resched();
    schedule();
    preempt_disable();
}

static void __sched preempt_schedule_common(void)
{
    do {
        /*
         * Because the function tracer can trace preempt_count_sub()
         * and it also uses preempt_enable/disable_notrace(), if
         * NEED_RESCHED is set, the preempt_enable_notrace() called
         * by the function tracer will call this function again and
         * cause infinite recursion.
         *
         * Preemption must be disabled here before the function
         * tracer can trace. Break up preempt_disable() into two
         * calls. One to disable preemption without fear of being
         * traced. The other to still record the preemption latency,
         * which can also be traced by the function tracer.
         */
        preempt_disable();
        __schedule(true);
        preempt_enable_no_resched();

        /*
         * Check again in case we missed a preemption opportunity
         * between schedule and now.
         */
    } while (need_resched());
}

/*
 * this is the entry point to schedule() from in-kernel preemption
 * off of preempt_enable. Kernel preemptions off return from interrupt
 * occur there and call schedule directly.
 */
asmlinkage __visible void __sched preempt_schedule(void)
{
    /*
     * If there is a non-zero preempt_count or interrupts are disabled,
     * we do not want to preempt the current task. Just return..
     */
    if (likely(!preemptible()))
        return;

    preempt_schedule_common();
}

/*
 * this is the entry point to schedule() from kernel preemption
 * off of irq context.
 * Note, that this is called and return with irqs disabled. This will
 * protect us against recursive calling from irq.
 */
asmlinkage __visible void __sched preempt_schedule_irq(void)
{
    /* Catch callers which need to be fixed */
    BUG_ON(preempt_count() || !irqs_disabled());

    do {
        preempt_disable();
        local_irq_enable();
        __schedule(true);
        local_irq_disable();
        sched_preempt_enable_no_resched();
    } while (need_resched());
}

int default_wake_function(wait_queue_entry_t *curr, unsigned mode, int wake_flags,
              void *key)
{
    return try_to_wake_up(curr->private, mode, wake_flags);
}

void set_user_nice(struct task_struct *p, long nice)
{
    bool queued, running;
    int old_prio, delta;
    struct rq_flags rf;
    struct rq *rq;

    if (task_nice(p) == nice || nice < MIN_NICE || nice > MAX_NICE)
        return;
    /*
     * We have to be careful, if called from sys_setpriority(),
     * the task might be in the middle of scheduling on another CPU.
     */
    rq = task_rq_lock(p, &rf);
    update_rq_clock(rq);

    /*
     * The RT priorities are set via sched_setscheduler(), but we still
     * allow the 'normal' nice value to be set - but as expected
     * it wont have any effect on scheduling until the task is
     * SCHED_DEADLINE, SCHED_FIFO or SCHED_RR:
     */
    if (task_has_dl_policy(p) || task_has_rt_policy(p)) {
        p->static_prio = NICE_TO_PRIO(nice);
        goto out_unlock;
    }
    queued = task_on_rq_queued(p);
    running = task_current(rq, p);
    if (queued)
        dequeue_task(rq, p, DEQUEUE_SAVE | DEQUEUE_NOCLOCK);
    if (running)
        put_prev_task(rq, p);

    p->static_prio = NICE_TO_PRIO(nice);
    set_load_weight(p, true);
    old_prio = p->prio;
    p->prio = effective_prio(p);
    delta = p->prio - old_prio;

    if (queued) {
        enqueue_task(rq, p, ENQUEUE_RESTORE | ENQUEUE_NOCLOCK);
        /*
         * If the task increased its priority or is running and
         * lowered its priority, then reschedule its CPU:
         */
        if (delta < 0 || (delta > 0 && task_running(rq, p)))
            resched_curr(rq);
    }
    if (running)
        set_curr_task(rq, p);
out_unlock:
    task_rq_unlock(rq, p, &rf);
}

/*
 * can_nice - check if a task can reduce its nice value
 * @p: task
 * @nice: nice value
 */
int can_nice(const struct task_struct *p, const int nice)
{
    /* Convert nice value [19,-20] to rlimit style value [1,40]: */
    int nice_rlim = nice_to_rlimit(nice);

    return nice_rlim <= task_rlimit(p, RLIMIT_NICE);
}

/*
 * sys_nice - change the priority of the current process.
 * @increment: priority increment
 *
 * sys_setpriority is a more generic, but much slower function that
 * does similar things.
 */
SYSCALL_DEFINE1(nice, int, increment)
{
    long nice;

    /*
     * Setpriority might change our priority at the same moment.
     * We don't have to worry. Conceptually one call occurs first
     * and we have a single winner.
     */
    increment = clamp(increment, -NICE_WIDTH, NICE_WIDTH);
    nice = task_nice(current) + increment;

    nice = clamp_val(nice, MIN_NICE, MAX_NICE);
    if (increment < 0 && !can_nice(current, nice))
        return -EPERM;

    set_user_nice(current, nice);
    return 0;
}

/**
 * task_prio - return the priority value of a given task.
 * @p: the task in question.
 *
 * Return: The priority value as seen by users in /proc.
 * RT tasks are offset by -200. Normal tasks are centered
 * around 0, value goes from -16 to +15.
 */
int task_prio(const struct task_struct *p)
{
    return p->prio - MAX_RT_PRIO;
}

/**
 * idle_cpu - is a given CPU idle currently?
 * @cpu: the processor in question.
 *
 * Return: 1 if the CPU is currently idle. 0 otherwise.
 */
int idle_cpu(int cpu)
{
    struct rq *rq = cpu_rq(cpu);

    if (rq->curr != rq->idle)
        return 0;

    if (rq->nr_running)
        return 0;

    if (!llist_empty(&rq->wake_list))
        return 0;

    return 1;
}

/**
 * available_idle_cpu - is a given CPU idle for enqueuing work.
 * @cpu: the CPU in question.
 *
 * Return: 1 if the CPU is currently idle. 0 otherwise.
 */
int available_idle_cpu(int cpu)
{
    if (!idle_cpu(cpu))
        return 0;

    return 1;
}

/**
 * idle_task - return the idle task for a given CPU.
 * @cpu: the processor in question.
 *
 * Return: The idle task for the CPU @cpu.
 */
struct task_struct *idle_task(int cpu)
{
    return cpu_rq(cpu)->idle;
}

/**
 * find_process_by_pid - find a process with a matching PID value.
 * @pid: the pid in question.
 *
 * The task of @pid, if found. %NULL otherwise.
 */
static struct task_struct *find_process_by_pid(pid_t pid)
{
    return pid ? find_task_by_pid(pid) : current;
}

/*
 * sched_setparam() passes in -1 for its policy, to let the functions
 * it calls know not to change it.
 */
#define SETPARAM_POLICY	-1

static void __setscheduler_params(struct task_struct *p,
        const struct sched_attr *attr)
{
    int policy = attr->sched_policy;

    if (policy == SETPARAM_POLICY)
        policy = p->policy;

    p->policy = policy;

    if (dl_policy(policy))
        __setparam_dl(p, attr);
    else if (fair_policy(policy))
        p->static_prio = NICE_TO_PRIO(attr->sched_nice);

    /*
     * __sched_setscheduler() ensures attr->sched_priority == 0 when
     * !rt_policy. Always setting this ensures that things like
     * getparam()/getattr() don't report silly values for !rt tasks.
     */
    p->rt_priority = attr->sched_priority;
    p->normal_prio = normal_prio(p);
    set_load_weight(p, true);
}

/* Actually do priority change: must hold pi & rq lock. */
static void __setscheduler(struct rq *rq, struct task_struct *p,
               const struct sched_attr *attr, bool keep_boost)
{
    __setscheduler_params(p, attr);

    /*
     * Keep a potential priority boosting if called from
     * sched_setscheduler().
     */
    p->prio = normal_prio(p);

    if (dl_prio(p->prio))
        p->sched_class = &dl_sched_class;
    else if (rt_prio(p->prio))
        p->sched_class = &rt_sched_class;
    else
        p->sched_class = &fair_sched_class;
}

static int __sched_setscheduler(struct task_struct *p,
                const struct sched_attr *attr,
                bool user, bool pi)
{
    int newprio = dl_policy(attr->sched_policy) ? MAX_DL_PRIO - 1 :
              MAX_RT_PRIO - 1 - attr->sched_priority;
    int oldprio, oldpolicy = -1, queued, running;
    int new_effective_prio, policy = attr->sched_policy;
    const struct sched_class *prev_class;
    struct rq_flags rf;
    int reset_on_fork;
    int queue_flags = DEQUEUE_SAVE | DEQUEUE_MOVE | DEQUEUE_NOCLOCK;
    struct rq *rq;

    /* The pi code expects interrupts enabled */
    BUG_ON(pi && in_interrupt());
recheck:
    /* Double check policy once rq lock held: */
    if (policy < 0) {
        reset_on_fork = p->sched_reset_on_fork;
        policy = oldpolicy = p->policy;
    } else {
        reset_on_fork = !!(attr->sched_flags & SCHED_FLAG_RESET_ON_FORK);

        if (!valid_policy(policy))
            return -EINVAL;
    }

    if (attr->sched_flags & ~(SCHED_FLAG_ALL | SCHED_FLAG_SUGOV))
        return -EINVAL;

    /*
     * Valid priorities for SCHED_FIFO and SCHED_RR are
     * 1..MAX_USER_RT_PRIO-1, valid priority for SCHED_NORMAL,
     * SCHED_BATCH and SCHED_IDLE is 0.
     */
    if ((p->mm && attr->sched_priority > MAX_USER_RT_PRIO-1) ||
        (!p->mm && attr->sched_priority > MAX_RT_PRIO-1))
        return -EINVAL;
    if ((dl_policy(policy) && !__checkparam_dl(attr)) ||
        (rt_policy(policy) != (attr->sched_priority != 0)))
        return -EINVAL;

    /*
     * Allow unprivileged RT tasks to decrease priority:
     */
    if (user) {
        if (fair_policy(policy)) {
            if (attr->sched_nice < task_nice(p) &&
                !can_nice(p, attr->sched_nice))
                return -EPERM;
        }

        if (rt_policy(policy)) {
            unsigned long rlim_rtprio =
                    task_rlimit(p, RLIMIT_RTPRIO);

            /* Can't set/change the rt policy: */
            if (policy != p->policy && !rlim_rtprio)
                return -EPERM;

            /* Can't increase priority: */
            if (attr->sched_priority > p->rt_priority &&
                attr->sched_priority > rlim_rtprio)
                return -EPERM;
        }

         /*
          * Can't set/change SCHED_DEADLINE policy at all for now
          * (safest behavior); in the future we would like to allow
          * unprivileged DL tasks to increase their relative deadline
          * or reduce their runtime (both ways reducing utilization)
          */
        if (dl_policy(policy))
            return -EPERM;

        /*
         * Treat SCHED_IDLE as nice 20. Only allow a switch to
         * SCHED_NORMAL if the RLIMIT_NICE would normally permit it.
         */
        if (task_has_idle_policy(p) && !idle_policy(policy)) {
            if (!can_nice(p, task_nice(p)))
                return -EPERM;
        }

        /* Normal users shall not reset the sched_reset_on_fork flag: */
        if (p->sched_reset_on_fork && !reset_on_fork)
            return -EPERM;
    }

    if (user) {
        if (attr->sched_flags & SCHED_FLAG_SUGOV)
            return -EINVAL;
    }

    /*
     * Make sure no PI-waiters arrive (or leave) while we are
     * changing the priority of the task:
     *
     * To be able to change p->policy safely, the appropriate
     * runqueue lock must be held.
     */
    rq = task_rq_lock(p, &rf);
    update_rq_clock(rq);

    /*
     * Changing the policy of the stop threads its a very bad idea:
     */
    if (p == rq->stop) {
        task_rq_unlock(rq, p, &rf);
        return -EINVAL;
    }

    /*
     * If not changing anything there's no need to proceed further,
     * but store a possible modification of reset_on_fork.
     */
    if (unlikely(policy == p->policy)) {
        if (fair_policy(policy) && attr->sched_nice != task_nice(p))
            goto change;
        if (rt_policy(policy) && attr->sched_priority != p->rt_priority)
            goto change;
        if (dl_policy(policy) && dl_param_changed(p, attr))
            goto change;

        p->sched_reset_on_fork = reset_on_fork;
        task_rq_unlock(rq, p, &rf);
        return 0;
    }
change:

    if (user) {
        if (dl_bandwidth_enabled() && dl_policy(policy) &&
                !(attr->sched_flags & SCHED_FLAG_SUGOV)) {
            cpumask_t *span = &rq->rd->span;

            /*
             * Don't allow tasks with an affinity mask smaller than
             * the entire root_domain to become SCHED_DEADLINE. We
             * will also fail if there's no bandwidth available.
             */
            if (!cpumask_subset(span, &p->cpus_allowed) ||
                rq->rd->dl_bw.bw == 0) {
                task_rq_unlock(rq, p, &rf);
                return -EPERM;
            }
        }
    }

    /* Re-check policy now with rq lock held: */
    if (unlikely(oldpolicy != -1 && oldpolicy != p->policy)) {
        policy = oldpolicy = -1;
        task_rq_unlock(rq, p, &rf);
        goto recheck;
    }

    /*
     * If setscheduling to SCHED_DEADLINE (or changing the parameters
     * of a SCHED_DEADLINE task) we need to check if enough bandwidth
     * is available.
     */
    if ((dl_policy(policy) || dl_task(p)) && sched_dl_overflow(p, policy, attr)) {
        task_rq_unlock(rq, p, &rf);
        return -EBUSY;
    }

    p->sched_reset_on_fork = reset_on_fork;
    oldprio = p->prio;

    if (pi) {
        /*
         * Take priority boosted tasks into account. If the new
         * effective priority is unchanged, we just store the new
         * normal parameters and do not touch the scheduler class and
         * the runqueue. This will be done when the task deboost
         * itself.
         */
        new_effective_prio = newprio;
        if (new_effective_prio == oldprio)
            queue_flags &= ~DEQUEUE_MOVE;
    }

    queued = task_on_rq_queued(p);
    running = task_current(rq, p);
    if (queued)
        dequeue_task(rq, p, queue_flags);
    if (running)
        put_prev_task(rq, p);

    prev_class = p->sched_class;
    __setscheduler(rq, p, attr, pi);

    if (queued) {
        /*
         * We enqueue to tail when the priority of a task is
         * increased (user space view).
         */
        if (oldprio < p->prio)
            queue_flags |= ENQUEUE_HEAD;

        enqueue_task(rq, p, queue_flags);
    }
    if (running)
        set_curr_task(rq, p);

    check_class_changed(rq, p, prev_class, oldprio);

    /* Avoid rq from going away on us: */
    preempt_disable();
    task_rq_unlock(rq, p, &rf);

    if (pi)
        rt_mutex_adjust_pi(p);

    /* Run balance callbacks after we've adjusted the PI chain: */
    balance_callback(rq);
    preempt_enable();

    return 0;
}

static int _sched_setscheduler(struct task_struct *p, int policy,
                   const struct sched_param *param, bool check)
{
    struct sched_attr attr = {
        .sched_policy   = policy,
        .sched_priority = param->sched_priority,
        .sched_nice	= PRIO_TO_NICE(p->static_prio),
    };

    /* Fixup the legacy SCHED_RESET_ON_FORK hack. */
    if ((policy != SETPARAM_POLICY) && (policy & SCHED_RESET_ON_FORK)) {
        attr.sched_flags |= SCHED_FLAG_RESET_ON_FORK;
        policy &= ~SCHED_RESET_ON_FORK;
        attr.sched_policy = policy;
    }

    return __sched_setscheduler(p, &attr, check, true);
}
/**
 * sched_setscheduler - change the scheduling policy and/or RT priority of a thread.
 * @p: the task in question.
 * @policy: new policy.
 * @param: structure containing the new RT priority.
 *
 * Return: 0 on success. An error code otherwise.
 *
 * NOTE that the task may be already dead.
 */
int sched_setscheduler(struct task_struct *p, int policy,
               const struct sched_param *param)
{
    return _sched_setscheduler(p, policy, param, true);
}

int sched_setattr(struct task_struct *p, const struct sched_attr *attr)
{
    return __sched_setscheduler(p, attr, true, true);
}

int sched_setattr_nocheck(struct task_struct *p, const struct sched_attr *attr)
{
    return __sched_setscheduler(p, attr, false, true);
}

/**
 * sched_setscheduler_nocheck - change the scheduling policy and/or RT priority of a thread from kernelspace.
 * @p: the task in question.
 * @policy: new policy.
 * @param: structure containing the new RT priority.
 *
 * Just like sched_setscheduler, only don't bother checking if the
 * current context has permission.  For example, this is needed in
 * stop_machine(): we create temporary high priority worker threads,
 * but our caller might not have that capability.
 *
 * Return: 0 on success. An error code otherwise.
 */
int sched_setscheduler_nocheck(struct task_struct *p, int policy,
                   const struct sched_param *param)
{
    return _sched_setscheduler(p, policy, param, false);
}

static int
do_sched_setscheduler(pid_t pid, int policy, struct sched_param __user *param)
{
    struct sched_param lparam;
    struct task_struct *p;
    int retval;

    if (!param || pid < 0)
        return -EINVAL;
    if (copy_from_user(&lparam, param, sizeof(struct sched_param)))
        return -EFAULT;

    rcu_read_lock();
    retval = -ESRCH;
    p = find_process_by_pid(pid);
    if (p != NULL)
        retval = sched_setscheduler(p, policy, &lparam);
    rcu_read_unlock();

    return retval;
}

/*
 * Mimics kernel/events/core.c perf_copy_attr().
 */
static int sched_copy_attr(struct sched_attr __user *uattr, struct sched_attr *attr)
{
    u32 size;
    int ret;

    if (!access_ok(uattr, SCHED_ATTR_SIZE_VER0))
        return -EFAULT;

    /* Zero the full structure, so that a short copy will be nice: */
    memset(attr, 0, sizeof(*attr));

    ret = get_user(size, &uattr->size);
    if (ret)
        return ret;

    /* Bail out on silly large: */
    if (size > PAGE_SIZE)
        goto err_size;

    /* ABI compatibility quirk: */
    if (!size)
        size = SCHED_ATTR_SIZE_VER0;

    if (size < SCHED_ATTR_SIZE_VER0)
        goto err_size;

    /*
     * If we're handed a bigger struct than we know of,
     * ensure all the unknown bits are 0 - i.e. new
     * user-space does not rely on any kernel feature
     * extensions we dont know about yet.
     */
    if (size > sizeof(*attr)) {
        unsigned char __user *addr;
        unsigned char __user *end;
        unsigned char val;

        addr = (void __user *)uattr + sizeof(*attr);
        end  = (void __user *)uattr + size;

        for (; addr < end; addr++) {
            ret = get_user(val, addr);
            if (ret)
                return ret;
            if (val)
                goto err_size;
        }
        size = sizeof(*attr);
    }

    ret = copy_from_user(attr, uattr, size);
    if (ret)
        return -EFAULT;

    /*
     * XXX: Do we want to be lenient like existing syscalls; or do we want
     * to be strict and return an error on out-of-bounds values?
     */
    attr->sched_nice = clamp(attr->sched_nice, MIN_NICE, MAX_NICE);

    return 0;

err_size:
    put_user(sizeof(*attr), &uattr->size);
    return -E2BIG;
}

/**
 * sys_sched_setscheduler - set/change the scheduler policy and RT priority
 * @pid: the pid in question.
 * @policy: new policy.
 * @param: structure containing the new RT priority.
 *
 * Return: 0 on success. An error code otherwise.
 */
SYSCALL_DEFINE3(sched_setscheduler, pid_t, pid, int, policy, struct sched_param __user *, param)
{
    if (policy < 0)
        return -EINVAL;

    return do_sched_setscheduler(pid, policy, param);
}

/**
 * sys_sched_setparam - set/change the RT priority of a thread
 * @pid: the pid in question.
 * @param: structure containing the new RT priority.
 *
 * Return: 0 on success. An error code otherwise.
 */
SYSCALL_DEFINE2(sched_setparam, pid_t, pid, struct sched_param __user *, param)
{
    return do_sched_setscheduler(pid, SETPARAM_POLICY, param);
}

/**
 * sys_sched_setattr - same as above, but with extended sched_attr
 * @pid: the pid in question.
 * @uattr: structure containing the extended parameters.
 * @flags: for future extension.
 */
SYSCALL_DEFINE3(sched_setattr, pid_t, pid, struct sched_attr __user *, uattr,
                   unsigned int, flags)
{
    struct sched_attr attr;
    struct task_struct *p;
    int retval;

    if (!uattr || pid < 0 || flags)
        return -EINVAL;

    retval = sched_copy_attr(uattr, &attr);
    if (retval)
        return retval;

    if ((int)attr.sched_policy < 0)
        return -EINVAL;

    rcu_read_lock();
    retval = -ESRCH;
    p = find_process_by_pid(pid);
    if (p != NULL)
        retval = sched_setattr(p, &attr);
    rcu_read_unlock();

    return retval;
}

/**
 * sys_sched_getscheduler - get the policy (scheduling class) of a thread
 * @pid: the pid in question.
 *
 * Return: On success, the policy of the thread. Otherwise, a negative error
 * code.
 */
SYSCALL_DEFINE1(sched_getscheduler, pid_t, pid)
{
    struct task_struct *p;
    int retval;

    if (pid < 0)
        return -EINVAL;

    retval = -ESRCH;
    rcu_read_lock();
    p = find_process_by_pid(pid);
    if (p)
        retval = p->policy | (p->sched_reset_on_fork ? SCHED_RESET_ON_FORK : 0);
    rcu_read_unlock();
    return retval;
}

/**
 * sys_sched_getparam - get the RT priority of a thread
 * @pid: the pid in question.
 * @param: structure containing the RT priority.
 *
 * Return: On success, 0 and the RT priority is in @param. Otherwise, an error
 * code.
 */
SYSCALL_DEFINE2(sched_getparam, pid_t, pid, struct sched_param __user *, param)
{
    struct sched_param lp = { .sched_priority = 0 };
    struct task_struct *p;
    int retval;

    if (!param || pid < 0)
        return -EINVAL;

    rcu_read_lock();
    p = find_process_by_pid(pid);
    retval = -ESRCH;
    if (!p)
        goto out_unlock;

    if (task_has_rt_policy(p))
        lp.sched_priority = p->rt_priority;
    rcu_read_unlock();

    /*
     * This one might sleep, we cannot do it with a spinlock held ...
     */
    retval = copy_to_user(param, &lp, sizeof(*param)) ? -EFAULT : 0;

    return retval;

out_unlock:
    rcu_read_unlock();
    return retval;
}

static int sched_read_attr(struct sched_attr __user *uattr,
               struct sched_attr *attr,
               unsigned int usize)
{
    int ret;

    if (!access_ok(uattr, usize))
        return -EFAULT;

    /*
     * If we're handed a smaller struct than we know of,
     * ensure all the unknown bits are 0 - i.e. old
     * user-space does not get uncomplete information.
     */
    if (usize < sizeof(*attr)) {
        unsigned char *addr;
        unsigned char *end;

        addr = (void *)attr + usize;
        end  = (void *)attr + sizeof(*attr);

        for (; addr < end; addr++) {
            if (*addr)
                return -EFBIG;
        }

        attr->size = usize;
    }

    ret = copy_to_user(uattr, attr, attr->size);
    if (ret)
        return -EFAULT;

    return 0;
}

/**
 * sys_sched_getattr - similar to sched_getparam, but with sched_attr
 * @pid: the pid in question.
 * @uattr: structure containing the extended parameters.
 * @size: sizeof(attr) for fwd/bwd comp.
 * @flags: for future extension.
 */
SYSCALL_DEFINE4(sched_getattr, pid_t, pid, struct sched_attr __user *, uattr,
        unsigned int, size, unsigned int, flags)
{
    struct sched_attr attr = {
        .size = sizeof(struct sched_attr),
    };
    struct task_struct *p;
    int retval;

    if (!uattr || pid < 0 || size > PAGE_SIZE ||
        size < SCHED_ATTR_SIZE_VER0 || flags)
        return -EINVAL;

    rcu_read_lock();
    p = find_process_by_pid(pid);
    retval = -ESRCH;
    if (!p)
        goto out_unlock;

    attr.sched_policy = p->policy;
    if (p->sched_reset_on_fork)
        attr.sched_flags |= SCHED_FLAG_RESET_ON_FORK;
    if (task_has_dl_policy(p))
        __getparam_dl(p, &attr);
    else if (task_has_rt_policy(p))
        attr.sched_priority = p->rt_priority;
    else
        attr.sched_nice = task_nice(p);

    rcu_read_unlock();

    retval = sched_read_attr(uattr, &attr, size);
    return retval;

out_unlock:
    rcu_read_unlock();
    return retval;
}

long sched_setaffinity(pid_t pid, const struct cpumask *in_mask)
{
    struct cpumask cpus_allowed, new_mask;
    struct task_struct *p;
    int retval;

    rcu_read_lock();

    p = find_process_by_pid(pid);
    if (!p) {
        rcu_read_unlock();
        return -ESRCH;
    }

    /* Prevent p going away */
    get_task_struct(p);
    rcu_read_unlock();

    if (p->flags & PF_NO_SETAFFINITY) {
        retval = -EINVAL;
        goto out_put_task;
    }

    cpumask_clear(&cpus_allowed);
    cpumask_clear(&new_mask);

    cpumask_copy(&cpus_allowed, cpu_possible_mask);

    cpumask_and(&new_mask, in_mask, &cpus_allowed);

    /*
     * Since bandwidth control happens on root_domain basis,
     * if admission test is enabled, we only admit -deadline
     * tasks allowed to run on all the CPUs in the task's
     * root_domain.
     */
    if (task_has_dl_policy(p) && dl_bandwidth_enabled()) {
        rcu_read_lock();
        if (!cpumask_subset(&task_rq(p)->rd->span, &new_mask)) {
            retval = -EBUSY;
            rcu_read_unlock();
            goto out_put_task;
        }
        rcu_read_unlock();
    }
again:
    retval = __set_cpus_allowed_ptr(p, &new_mask, true);

    if (!retval) {
        cpumask_copy(&cpus_allowed, cpu_possible_mask);
        if (!cpumask_subset(&new_mask, &cpus_allowed)) {
            /*
             * We must have raced with a concurrent cpuset
             * update. Just reset the cpus_allowed to the
             * cpuset's cpus_allowed
             */
            cpumask_copy(&new_mask, &cpus_allowed);
            goto again;
        }
    }
out_put_task:
    put_task_struct(p);
    return retval;
}

static int get_user_cpu_mask(unsigned long __user *user_mask_ptr, unsigned len,
                 struct cpumask *new_mask)
{
    if (len < cpumask_size())
        cpumask_clear(new_mask);
    else if (len > cpumask_size())
        len = cpumask_size();

    return copy_from_user(new_mask, user_mask_ptr, len) ? -EFAULT : 0;
}

/**
 * sys_sched_setaffinity - set the CPU affinity of a process
 * @pid: pid of the process
 * @len: length in bytes of the bitmask pointed to by user_mask_ptr
 * @user_mask_ptr: user-space pointer to the new CPU mask
 *
 * Return: 0 on success. An error code otherwise.
 */
SYSCALL_DEFINE3(sched_setaffinity, pid_t, pid, unsigned int, len,
        unsigned long __user *, user_mask_ptr)
{
    struct cpumask new_mask;
    int retval;

    cpumask_clear(&new_mask);

    retval = get_user_cpu_mask(user_mask_ptr, len, &new_mask);
    if (retval == 0)
        retval = sched_setaffinity(pid, &new_mask);
    return retval;
}

long sched_getaffinity(pid_t pid, struct cpumask *mask)
{
    struct task_struct *p;
    unsigned long flags;
    int retval;

    rcu_read_lock();

    retval = -ESRCH;
    p = find_process_by_pid(pid);
    if (!p)
        goto out_unlock;

    raw_spin_lock_irqsave(&p->pi_lock, flags);
    cpumask_and(mask, &p->cpus_allowed, cpu_online_mask);
    raw_spin_unlock_irqrestore(&p->pi_lock, flags);

out_unlock:
    rcu_read_unlock();

    return retval;
}

/**
 * sys_sched_getaffinity - get the CPU affinity of a process
 * @pid: pid of the process
 * @len: length in bytes of the bitmask pointed to by user_mask_ptr
 * @user_mask_ptr: user-space pointer to hold the current CPU mask
 *
 * Return: size of CPU mask copied to user_mask_ptr on success. An
 * error code otherwise.
 */
SYSCALL_DEFINE3(sched_getaffinity, pid_t, pid, unsigned int, len,
        unsigned long __user *, user_mask_ptr)
{
    int ret;
    struct cpumask mask;

    if ((len * BITS_PER_BYTE) < nr_cpu_ids)
        return -EINVAL;
    if (len & (sizeof(unsigned long)-1))
        return -EINVAL;

    cpumask_clear(&mask);

    ret = sched_getaffinity(pid, &mask);
    if (ret == 0) {
        unsigned int retlen = min(len, cpumask_size());

        if (copy_to_user(user_mask_ptr, &mask, retlen))
            ret = -EFAULT;
        else
            ret = retlen;
    }

    return ret;
}

/**
 * sys_sched_yield - yield the current processor to other threads.
 *
 * This function yields the current CPU to other tasks. If there are no
 * other threads running on this CPU then this function will return.
 *
 * Return: 0.
 */
static void do_sched_yield(void)
{
    struct rq_flags rf;
    struct rq *rq;

    rq = this_rq_lock_irq(&rf);

    current->sched_class->yield_task(rq);

    /*
     * Since we are going to call schedule() anyway, there's
     * no need to preempt or enable interrupts:
     */
    preempt_disable();
    rq_unlock(rq, &rf);
    sched_preempt_enable_no_resched();

    schedule();
}

SYSCALL_DEFINE0(sched_yield)
{
    do_sched_yield();
    return 0;
}

/*
 * __cond_resched_lock() - if a reschedule is pending, drop the given lock,
 * call schedule, and on return reacquire the lock.
 *
 * This works OK both with and without CONFIG_PREEMPT. We do strange low-level
 * operations here to prevent schedule() from being called twice (once via
 * spin_unlock(), once by hand).
 */
int __cond_resched_lock(spinlock_t *lock)
{
    int resched = should_resched(PREEMPT_LOCK_OFFSET);
    int ret = 0;

    if (spin_needbreak(lock) || resched) {
        spin_unlock(lock);
        if (resched)
            preempt_schedule_common();
        else
            cpu_relax();
        ret = 1;
        spin_lock(lock);
    }
    return ret;
}

/**
 * yield - yield the current processor to other threads.
 *
 * Do not ever use this function, there's a 99% chance you're doing it wrong.
 *
 * The scheduler is at all times free to pick the calling task as the most
 * eligible task to run, if removing the yield() call from your code breaks
 * it, its already broken.
 *
 * Typical broken usage is:
 *
 * while (!event)
 *	yield();
 *
 * where one assumes that yield() will let 'the other' process run that will
 * make event true. If the current task is a SCHED_FIFO task that will never
 * happen. Never use yield() as a progress guarantee!!
 *
 * If you want to use yield() to wait for something, use wait_event().
 * If you want to use yield() to be 'nice' for others, use cond_resched().
 * If you still want to use yield(), do not!
 */
void __sched yield(void)
{
    set_current_state(TASK_RUNNING);
    do_sched_yield();
}

/**
 * yield_to - yield the current processor to another thread in
 * your thread group, or accelerate that thread toward the
 * processor it's on.
 * @p: target task
 * @preempt: whether task preemption is allowed or not
 *
 * It's the caller's job to ensure that the target task struct
 * can't go away on us before we can do any checks.
 *
 * Return:
 *	true (>0) if we indeed boosted the target task.
 *	false (0) if we failed to boost the target.
 *	-ESRCH if there's no task to yield to.
 */
int __sched yield_to(struct task_struct *p, bool preempt)
{
    struct task_struct *curr = current;
    struct rq *rq, *p_rq;
    unsigned long flags;
    int yielded = 0;

    local_irq_save(flags);
    rq = this_rq();

again:
    p_rq = task_rq(p);
    /*
     * If we're the only runnable task on the rq and target rq also
     * has only one task, there's absolutely no point in yielding.
     */
    if (rq->nr_running == 1 && p_rq->nr_running == 1) {
        yielded = -ESRCH;
        goto out_irq;
    }

    double_rq_lock(rq, p_rq);
    if (task_rq(p) != p_rq) {
        double_rq_unlock(rq, p_rq);
        goto again;
    }

    if (!curr->sched_class->yield_to_task)
        goto out_unlock;

    if (curr->sched_class != p->sched_class)
        goto out_unlock;

    if (task_running(p_rq, p) || p->state)
        goto out_unlock;

    yielded = curr->sched_class->yield_to_task(rq, p, preempt);
    if (yielded) {
        /*
         * Make p's CPU reschedule; pick_next_entity takes care of
         * fairness.
         */
        if (preempt && rq != p_rq)
            resched_curr(p_rq);
    }

out_unlock:
    double_rq_unlock(rq, p_rq);
out_irq:
    local_irq_restore(flags);

    if (yielded > 0)
        schedule();

    return yielded;
}

int io_schedule_prepare(void)
{
    int old_iowait = current->in_iowait;
    current->in_iowait = 1;
    return old_iowait;
}

void io_schedule_finish(int token)
{
    current->in_iowait = token;
}

/*
 * This task is about to go to sleep on IO. Increment rq->nr_iowait so
 * that process accounting knows that this is a task in IO wait state.
 */
long __sched io_schedule_timeout(long timeout)
{
    int token;
    long ret;

    token = io_schedule_prepare();
    ret = schedule_timeout(timeout);
    io_schedule_finish(token);

    return ret;
}

void io_schedule(void)
{
    int token;

    token = io_schedule_prepare();
    schedule();
    io_schedule_finish(token);
}

/**
 * sys_sched_get_priority_max - return maximum RT priority.
 * @policy: scheduling class.
 *
 * Return: On success, this syscall returns the maximum
 * rt_priority that can be used by a given scheduling class.
 * On failure, a negative error code is returned.
 */
SYSCALL_DEFINE1(sched_get_priority_max, int, policy)
{
    int ret = -EINVAL;

    switch (policy) {
    case SCHED_FIFO:
    case SCHED_RR:
        ret = MAX_USER_RT_PRIO-1;
        break;
    case SCHED_DEADLINE:
    case SCHED_NORMAL:
    case SCHED_BATCH:
    case SCHED_IDLE:
        ret = 0;
        break;
    }
    return ret;
}

/**
 * sys_sched_get_priority_min - return minimum RT priority.
 * @policy: scheduling class.
 *
 * Return: On success, this syscall returns the minimum
 * rt_priority that can be used by a given scheduling class.
 * On failure, a negative error code is returned.
 */
SYSCALL_DEFINE1(sched_get_priority_min, int, policy)
{
    int ret = -EINVAL;

    switch (policy) {
    case SCHED_FIFO:
    case SCHED_RR:
        ret = 1;
        break;
    case SCHED_DEADLINE:
    case SCHED_NORMAL:
    case SCHED_BATCH:
    case SCHED_IDLE:
        ret = 0;
    }
    return ret;
}

static int sched_rr_get_interval(pid_t pid, struct timespec64 *t)
{
    struct task_struct *p;
    unsigned int time_slice;
    struct rq_flags rf;
    struct rq *rq;
    int retval;

    if (pid < 0)
        return -EINVAL;

    retval = -ESRCH;
    rcu_read_lock();
    p = find_process_by_pid(pid);
    if (!p)
        goto out_unlock;

    rq = task_rq_lock(p, &rf);
    time_slice = 0;
    if (p->sched_class->get_rr_interval)
        time_slice = p->sched_class->get_rr_interval(rq, p);
    task_rq_unlock(rq, p, &rf);

    rcu_read_unlock();
    jiffies_to_timespec64(time_slice, t);
    return 0;

out_unlock:
    rcu_read_unlock();
    return retval;
}

/**
 * sys_sched_rr_get_interval - return the default timeslice of a process.
 * @pid: pid of the process.
 * @interval: userspace pointer to the timeslice value.
 *
 * this syscall writes the default timeslice value of a given process
 * into the user-space timespec buffer. A value of '0' means infinity.
 *
 * Return: On success, 0 and the timeslice is in @interval. Otherwise,
 * an error code.
 */
SYSCALL_DEFINE2(sched_rr_get_interval, pid_t, pid,
        struct __kernel_timespec __user *, interval)
{
    struct timespec64 t;
    int retval = sched_rr_get_interval(pid, &t);

    if (retval == 0)
        retval = put_timespec64(&t, interval);

    return retval;
}

void sched_show_task(struct task_struct *p)
{
    unsigned long free = 0;
    int ppid;

    if (!try_get_task_stack(p))
        return;

    printk(KERN_INFO "%-15.15s %c", p->comm, task_state_to_char(p));

    if (p->state == TASK_RUNNING)
        printk(KERN_CONT "  running task    ");
    ppid = 0;
    rcu_read_lock();
    if (pid_alive(p))
        ppid = task_pid_nr(rcu_dereference(p->real_parent));
    rcu_read_unlock();
    printk(KERN_CONT "%5lu %5d %6d 0x%08lx\n", free,
        task_pid_nr(p), ppid,
        (unsigned long)task_thread_info(p)->flags);

    show_stack(p, NULL);
    put_task_stack(p);
}

static inline bool
state_filter_match(unsigned long state_filter, struct task_struct *p)
{
    /* no filter, everything matches */
    if (!state_filter)
        return true;

    /* filter, but doesn't match */
    if (!(p->state & state_filter))
        return false;

    /*
     * When looking for TASK_UNINTERRUPTIBLE skip TASK_IDLE (allows
     * TASK_KILLABLE).
     */
    if (state_filter == TASK_UNINTERRUPTIBLE && p->state == TASK_IDLE)
        return false;

    return true;
}


void show_state_filter(unsigned long state_filter)
{
    struct task_struct *g, *p;

#if BITS_PER_LONG == 32
    printk(KERN_INFO
        "  task                PC stack   pid father\n");
#else
    printk(KERN_INFO
        "  task                        PC stack   pid father\n");
#endif
    rcu_read_lock();
    for_each_process_thread(g, p) {
        /*
         * reset the NMI-timeout, listing all files on a slow
         * console might take a lot of time:
         * Also, reset softlockup watchdogs on all CPUs, because
         * another CPU might be blocked waiting for us to process
         * an IPI.
         */
        touch_nmi_watchdog();
        if (state_filter_match(state_filter, p))
            sched_show_task(p);
    }

#ifdef SCHED_DEBUG
    if (!state_filter)
        sysrq_sched_debug_show();
#endif
    rcu_read_unlock();
}

/**
 * init_idle - set up an idle thread for a given CPU
 * @idle: task in question
 * @cpu: CPU the idle task belongs to
 *
 * NOTE: this function does not set the idle thread's NEED_RESCHED
 * flag, to make booting more robust.
 */
void init_idle(struct task_struct *idle, int cpu)
{
    struct rq *rq = cpu_rq(cpu);
    unsigned long flags;

    raw_spin_lock_irqsave(&idle->pi_lock, flags);
    raw_spin_lock(&rq->lock);

    __sched_fork(0, idle);
    idle->state = TASK_RUNNING;
    idle->se.exec_start = sched_clock();
    idle->flags |= PF_IDLE;

    /*
     * Its possible that init_idle() gets called multiple times on a task,
     * in that case do_set_cpus_allowed() will not do the right thing.
     *
     * And since this is boot we can forgo the serialization.
     */
    set_cpus_allowed_common(idle, cpumask_of(cpu));

    /*
     * We're having a chicken and egg problem, even though we are
     * holding rq->lock, the CPU isn't yet set to this CPU so the
     * lockdep check in task_group() will fail.
     *
     * Similar case to sched_fork(). / Alternatively we could
     * use task_rq_lock() here and obtain the other rq->lock.
     *
     * Silence PROVE_RCU
     */
    rcu_read_lock();
    __set_task_cpu(idle, cpu);
    rcu_read_unlock();

    rq->curr = rq->idle = idle;
    idle->on_rq = TASK_ON_RQ_QUEUED;
    idle->on_cpu = 1;

    raw_spin_unlock(&rq->lock);
    raw_spin_unlock_irqrestore(&idle->pi_lock, flags);

    /* Set the preempt count _outside_ the spinlocks! */
    init_idle_preempt_count(idle, cpu);

    /*
     * The idle tasks have their own, simple scheduling class:
     */
    idle->sched_class = &idle_sched_class;
    sprintf(idle->comm, "%s/%d", INIT_TASK_COMM, cpu);
}

bool sched_smp_initialized __read_mostly;

void set_rq_online(struct rq *rq)
{
    if (!rq->online) {
        const struct sched_class *class;

        rq->online = 1;
        for_each_class(class) {
            if (class->rq_online)
                class->rq_online(rq);
        }
    }
}

void set_rq_offline(struct rq *rq)
{
    if (rq->online) {
        const struct sched_class *class;

        for_each_class(class) {
            if (class->rq_offline)
                class->rq_offline(rq);
        }
        rq->online = 0;
    }
}

static void sched_rq_cpu_starting(unsigned int cpu)
{
    struct rq *rq = cpu_rq(cpu);

    rq->calc_load_update = calc_load_update;

}

int sched_cpu_starting(unsigned int cpu)
{
    sched_rq_cpu_starting(cpu);
    return 0;
}

void __init sched_init_smp(void)
{
    /* Move init over to a non-isolated CPU */
    if (set_cpus_allowed_ptr(current, cpu_possible_mask) < 0)
        BUG();
    sched_init_granularity();

    init_sched_rt_class();
    init_sched_dl_class();

    sched_smp_initialized = true;
}

static int __init migration_init(void)
{
    sched_rq_cpu_starting(smp_processor_id());
    return 0;
}
early_initcall(migration_init);

int in_sched_functions(unsigned long addr)
{
    return in_lock_functions(addr) ||
        (addr >= (unsigned long)__sched_text_start
        && addr < (unsigned long)__sched_text_end);
}

/*
 * By default the system creates a single root-domain with all CPUs as
 * members (mimicking the global state we have today).
 */
struct root_domain def_root_domain;

static int init_defrootdomain(void)
{
    int ret = -ENOMEM;

    cpumask_clear(&def_root_domain.span);
    cpumask_clear(&def_root_domain.rto_mask);
    cpumask_clear(&def_root_domain.dlo_mask);

    init_dl_bw(&def_root_domain.dl_bw);

    if (cpudl_init(&def_root_domain.cpudl) != 0)
        return ret;

    if (cpupri_init(&def_root_domain.cpupri) != 0)
        return ret;

    spin_lock_init(&def_root_domain.loadavg_lock);
    def_root_domain.max_loadavg = 0;
    def_root_domain.min_loadavg = 0;
    def_root_domain.max_loadavg_cpu = 0;
    def_root_domain.min_loadavg_cpu = 0;

    return 0;
}

void __init sched_init(void)
{
    int i, j;

    wait_bit_init();

    init_rt_bandwidth(&def_rt_bandwidth, global_rt_period(), global_rt_runtime());
    init_dl_bandwidth(&def_dl_bandwidth, global_rt_period(), global_rt_runtime());

    init_defrootdomain();

    for_each_possible_cpu(i) {
        struct rq *rq;

        rq = cpu_rq(i);
        raw_spin_lock_init(&rq->lock);
        rq->nr_running = 0;
        rq->calc_load_active = 0;
        rq->calc_load_update = jiffies_64 + LOAD_FREQ;
        init_cfs_rq(&rq->cfs);
        init_rt_rq(&rq->rt);
        init_dl_rq(&rq->dl);

        rq->rt.rt_runtime = def_rt_bandwidth.rt_runtime;

        for (j = 0; j < CPU_LOAD_IDX_MAX; j++)
            rq->cpu_load[j] = 0;

        rq->rd = &def_root_domain;
        rq->cpu_capacity = rq->cpu_capacity_orig = SCHED_CAPACITY_SCALE;
        rq->balance_callback = NULL;
        rq->cpu = i;
        rq->online = 0;
        rq->idle_stamp = 0;
        rq->avg_idle = 2*sysctl_sched_migration_cost;
        rq->max_idle_balance_cost = sysctl_sched_migration_cost;

        INIT_LIST_HEAD(&rq->cfs_tasks);

        rq->last_blocked_load_update_tick = jiffies_64;

        hrtick_rq_init(rq);
        atomic_set(&rq->nr_iowait, 0);
    }

    set_load_weight(&init_task, false);

    /*
     * The boot idle thread does lazy MMU switching as well:
     */
    mmgrab(&init_mm);
    enter_lazy_tlb(&init_mm, current);

    /*
     * Make us the idle thread. Technically, schedule() should not be
     * called from this thread, however somewhere below it might be,
     * but because we are the idle thread, we just pick up running again
     * when this runqueue becomes "idle".
     */
    init_idle(current, smp_processor_id());

    calc_load_update = jiffies_64 + LOAD_FREQ;

    idle_thread_set_boot_cpu();
    init_sched_fair_class();

    scheduler_running = 1;
}

void dump_cpu_task(int cpu)
{
    pr_info("Task dump for CPU %d:\n", cpu);
    sched_show_task(cpu_curr(cpu));
}

/*
 * Nice levels are multiplicative, with a gentle 10% change for every
 * nice level changed. I.e. when a CPU-bound task goes from nice 0 to
 * nice 1, it will get ~10% less CPU time than another CPU-bound task
 * that remained on nice 0.
 *
 * The "10% effect" is relative and cumulative: from _any_ nice level,
 * if you go up 1 level, it's -10% CPU usage, if you go down 1 level
 * it's +10% CPU usage. (to achieve that we use a multiplier of 1.25.
 * If a task goes up by ~10% and another task goes down by ~10% then
 * the relative distance between them is ~25%.)
 */
const int sched_prio_to_weight[40] = {
 /* -20 */     88761,     71755,     56483,     46273,     36291,
 /* -15 */     29154,     23254,     18705,     14949,     11916,
 /* -10 */      9548,      7620,      6100,      4904,      3906,
 /*  -5 */      3121,      2501,      1991,      1586,      1277,
 /*   0 */      1024,       820,       655,       526,       423,
 /*   5 */       335,       272,       215,       172,       137,
 /*  10 */       110,        87,        70,        56,        45,
 /*  15 */        36,        29,        23,        18,        15,
};

/*
 * Inverse (2^32/x) values of the sched_prio_to_weight[] array, precalculated.
 *
 * In cases where the weight does not change often, we can use the
 * precalculated inverse to speed up arithmetics by turning divisions
 * into multiplications:
 */
const u32 sched_prio_to_wmult[40] = {
 /* -20 */     48388,     59856,     76040,     92818,    118348,
 /* -15 */    147320,    184698,    229616,    287308,    360437,
 /* -10 */    449829,    563644,    704093,    875809,   1099582,
 /*  -5 */   1376151,   1717300,   2157191,   2708050,   3363326,
 /*   0 */   4194304,   5237765,   6557202,   8165337,  10153587,
 /*   5 */  12820798,  15790321,  19976592,  24970740,  31350126,
 /*  10 */  39045157,  49367440,  61356676,  76695844,  95443717,
 /*  15 */ 119304647, 148102320, 186737708, 238609294, 286331153,
};

#undef CREATE_TRACE_POINTS
