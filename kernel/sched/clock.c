/*
 * sched_clock() for unstable CPU clocks
 */
#include "sched.h"

extern void generic_sched_clock_init(void);

void __init sched_clock_init(void)
{
    local_irq_disable();
    generic_sched_clock_init();
    local_irq_enable();
}

u64 sched_clock_cpu(int cpu)
{
    return sched_clock();
}
