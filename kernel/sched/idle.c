/*
 * Generic entry points for the idle threads and
 * implementation of the idle task scheduling class.
 *
 * (NOTE: these are not related to SCHED_IDLE batch scheduled
 *        tasks which are handled in sched/fair.c )
 */
#include "sched.h"

/* Linker adds these: start and end of __cpuidle functions */
extern char __cpuidle_text_start[], __cpuidle_text_end[];

static int __read_mostly cpu_idle_force_poll;

static int __init cpu_idle_poll_setup(char *__unused)
{
    cpu_idle_force_poll = 1;
    return 0;
}
early_param("nohlt", cpu_idle_poll_setup);

static int __init cpu_idle_nopoll_setup(char *__unused)
{
    cpu_idle_force_poll = 0;
    return 0;
}
early_param("hlt", cpu_idle_nopoll_setup);

void cpu_idle_poll_ctrl(bool enable)
{
    if (enable) {
        cpu_idle_force_poll++;
    } else {
        cpu_idle_force_poll--;
        WARN_ON_ONCE(cpu_idle_force_poll < 0);
    }
}

static noinline int __cpuidle cpu_idle_poll(void)
{
    rcu_idle_enter();
    local_irq_enable();

    while (!tif_need_resched() && cpu_idle_force_poll)
        cpu_relax();

    rcu_idle_exit();

    return 1;
}

/* Weak implementations for optional arch specific functions */
void __weak arch_cpu_idle_prepare(void) { }
void __weak arch_cpu_idle_enter(void) { }
void __weak arch_cpu_idle_exit(void) { }
void __weak arch_cpu_idle_dead(void) { }
void __weak arch_cpu_idle(void)
{
    cpu_idle_force_poll = 1;
    local_irq_enable();
}

/**
 * default_idle_call - Default CPU idle routine.
 *
 * To use when the cpuidle framework cannot be used.
 */
void __cpuidle default_idle_call(void)
{
    if (current_clr_polling_and_test())
        local_irq_enable();
    else
        arch_cpu_idle();
}

static void cpuidle_idle_call(void)
{
    if (need_resched()) {
        local_irq_enable();
        return;
    }

    rcu_idle_enter();
    default_idle_call();
    __current_set_polling();

    /*
     * It is up to the idle functions to reenable local interrupts
     */
    if (WARN_ON_ONCE(irqs_disabled()))
        local_irq_enable();

    rcu_idle_exit();
}

/*
 * Generic idle loop implementation
 *
 * Called with polling cleared.
 */
static void do_idle(void)
{
    __current_set_polling();

    while (!need_resched()) {
        rmb();

        if (cpu_is_offline(smp_processor_id()))
            arch_cpu_idle_dead();

        local_irq_disable();
        arch_cpu_idle_enter();

        if (cpu_idle_force_poll)
            cpu_idle_poll();
        else
            cpuidle_idle_call();
        arch_cpu_idle_exit();
    }
    preempt_set_need_resched();
    __current_clr_polling();

    /*
     * We promise to call sched_ttwu_pending() and reschedule if
     * need_resched() is set while polling is set. That means that clearing
     * polling needs to be visible before doing these things.
     */
    smp_mb__after_atomic();

    sched_ttwu_pending();
    schedule_idle();
}

void cpu_startup_entry(void)
{
    arch_cpu_idle_prepare();
    while (1)
        do_idle();
}

/*
 * idle-task scheduling class.
 */
static int
select_task_rq_idle(struct task_struct *p, int cpu, int sd_flag, int flags)
{
    return task_cpu(p); /* IDLE tasks as never migrated */
}

/*
 * Idle tasks are unconditionally rescheduled:
 */
static void check_preempt_curr_idle(struct rq *rq, struct task_struct *p, int flags)
{
    resched_curr(rq);
}

static struct task_struct *
pick_next_task_idle(struct rq *rq, struct task_struct *prev, struct rq_flags *rf)
{
    put_prev_task(rq, prev);

    return rq->idle;
}

/*
 * It is not legal to sleep in the idle task - print a warning
 * message if some code attempts to do it:
 */
static void
dequeue_task_idle(struct rq *rq, struct task_struct *p, int flags)
{
    raw_spin_unlock_irq(&rq->lock);
    printk(KERN_ERR "bad: scheduling from the idle thread!\n");
    dump_stack();
    raw_spin_lock_irq(&rq->lock);
}

static void put_prev_task_idle(struct rq *rq, struct task_struct *prev)
{
}

/*
 * scheduler tick hitting a task of our scheduling class.
 *
 * NOTE: This function can be called remotely by the tick offload that
 * goes along full dynticks. Therefore no local assumption can be made
 * and everything must be accessed through the @rq and @curr passed in
 * parameters.
 */
static void task_tick_idle(struct rq *rq, struct task_struct *curr, int queued)
{
}

static void set_curr_task_idle(struct rq *rq)
{
}

static void switched_to_idle(struct rq *rq, struct task_struct *p)
{
    BUG();
}

static void
prio_changed_idle(struct rq *rq, struct task_struct *p, int oldprio)
{
    BUG();
}

static unsigned int get_rr_interval_idle(struct rq *rq, struct task_struct *task)
{
    return 0;
}

static void update_curr_idle(struct rq *rq)
{
}

/*
 * Simple, special scheduling class for the per-CPU idle tasks:
 */
const struct sched_class idle_sched_class = {
    /* .next is NULL */
    /* no enqueue/yield_task for idle tasks */

    /* dequeue is not valid, we print a debug message there: */
    .dequeue_task		= dequeue_task_idle,

    .check_preempt_curr	= check_preempt_curr_idle,

    .pick_next_task		= pick_next_task_idle,
    .put_prev_task		= put_prev_task_idle,

    .select_task_rq		= select_task_rq_idle,
    .set_cpus_allowed	= set_cpus_allowed_common,

    .set_curr_task          = set_curr_task_idle,
    .task_tick		= task_tick_idle,

    .get_rr_interval	= get_rr_interval_idle,

    .prio_changed		= prio_changed_idle,
    .switched_to		= switched_to_idle,
    .update_curr		= update_curr_idle,
};
