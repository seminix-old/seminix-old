#include <seminix/sched.h>
#include <seminix/signal.h>
#include <seminix/sched/signal.h>
#include <seminix/syscall.h>
#include <seminix/ratelimit.h>
#include <seminix/wait_bit.h>



void force_sig(int sig, struct task_struct *p)
{

}

void ignore_signals(struct task_struct *t)
{
}


void signal_setup_done(int failed, struct ksignal *ksig, int stepping)
{
}

bool get_signal(struct ksignal *ksig)
{
    if (ksig)
        return false;
    return true;
}

int copy_siginfo_to_user(siginfo_t __user *to, const kernel_siginfo_t *from)
{
    return 0;
}

int restore_altstack(const stack_t __user *uss)
{
    return 0;
}

int __save_altstack(stack_t __user *uss, unsigned long sp)
{
    return 0;
}

/**
 * set_current_blocked - change current->blocked mask
 * @newset: new mask
 *
 * It is wrong to change ->blocked directly, this helper should be used
 * to ensure the process can't miss a shared signal we are going to block.
 */
void set_current_blocked(sigset_t *newset)
{

}

long do_no_restart_syscall(struct restart_block *param)
{
    return -EINTR;
}

void __set_current_blocked(const sigset_t *newset)
{
}

int force_sig_fault(int sig, int code, void __user *addr
	___ARCH_SI_TRAPNO(int trapno)
	___ARCH_SI_IA64(int imm, unsigned int flags, unsigned long isr)
	, struct task_struct *t)
{
    panic("%s %p\n", __func__, addr);
    return 0;
}

int force_sig_mceerr(int code, void __user *addr, short lsb, struct task_struct *t)
{
    panic("%s %p\n", __func__, addr);
    return 0;
}

int send_sig_fault(int sig, int code, void __user *addr, struct task_struct *t)
{
    panic("%s %p\n", __func__, addr);
    return 0;
}

void calculate_sigpending(void)
{
}
