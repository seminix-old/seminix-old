#include <seminix/kernel.h>

void __noreturn do_exit(long code)
{
    for (;;);
}
