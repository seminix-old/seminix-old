// SPDX-License-Identifier: GPL-2.0
/* kernel/rwsem.c: R/W semaphores, public implementation
 *
 * Written by David Howells (dhowells@redhat.com).
 * Derived from asm-i386/semaphore.h
 */

#include <seminix/atomic.h>
#include <seminix/sched.h>
#include <seminix/rwsem.h>

/*
 * lock for reading
 */
void __sched down_read(struct rw_semaphore *sem)
{
    __down_read(sem);
}

int __sched down_read_killable(struct rw_semaphore *sem)
{
    if (__down_read_killable(sem))
        return -EINTR;
    return 0;
}

/*
 * trylock for reading -- returns 1 if successful, 0 if contention
 */
int down_read_trylock(struct rw_semaphore *sem)
{
    return __down_read_trylock(sem);
}

/*
 * lock for writing
 */
void __sched down_write(struct rw_semaphore *sem)
{
    __down_write(sem);
}

/*
 * lock for writing
 */
int __sched down_write_killable(struct rw_semaphore *sem)
{
    if (__down_write_killable(sem))
        return -EINTR;
    return 0;
}

/*
 * trylock for writing -- returns 1 if successful, 0 if contention
 */
int down_write_trylock(struct rw_semaphore *sem)
{
    return __down_write_trylock(sem);
}

/*
 * release a read lock
 */
void up_read(struct rw_semaphore *sem)
{
    __up_read(sem);
}

/*
 * release a write lock
 */
void up_write(struct rw_semaphore *sem)
{
    __up_write(sem);
}

/*
 * downgrade write lock to read lock
 */
void downgrade_write(struct rw_semaphore *sem)
{
    __downgrade_write(sem);
}
