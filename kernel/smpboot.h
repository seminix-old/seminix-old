/* SPDX-License-Identifier: GPL-2.0 */
#ifndef KERNEL_SMPBOOT_H
#define KERNEL_SMPBOOT_H

struct task_struct;

struct task_struct *idle_thread_get(unsigned int cpu);
void idle_thread_set_boot_cpu(void);
void idle_threads_init(void);

#endif /* KERNEL_SMPBOOT_H */
