#! /usr/bin/env perl
# SPDX-License-Identifier: GPL-2.0
#
# checkversion finds uses of all macros in <seminix/version.h>
# where the source files do not #include <seminix/version.h>; or cases
# of including <seminix/version.h> where it is not needed.
# Copyright (C) 2003, Randy Dunlap <rdunlap@infradead.org>

use strict;

$| = 1;

my $debugging;

foreach my $file (@ARGV) {
    next if $file =~ "include/generated/uapi/seminix/version\.h";
    next if $file =~ "usr/include/seminix/version\.h";
    # Open this file.
    open( my $f, '<', $file )
      or die "Can't open $file: $!\n";

    # Initialize variables.
    my ($fInComment, $fInString, $fUseVersion);
    my $iSeminixVersion = 0;

    while (<$f>) {
	# Strip comments.
	$fInComment && (s+^.*?\*/+ +o ? ($fInComment = 0) : next);
	m+/\*+o && (s+/\*.*?\*/+ +go, (s+/\*.*$+ +o && ($fInComment = 1)));

	# Pick up definitions.
	if ( m/^\s*#/o ) {
	    $iSeminixVersion      = $. if m/^\s*#\s*include\s*"seminix\/version\.h"/o;
	}

	# Strip strings.
	$fInString && (s+^.*?"+ +o ? ($fInString = 0) : next);
	m+"+o && (s+".*?"+ +go, (s+".*$+ +o && ($fInString = 1)));

	# Pick up definitions.
	if ( m/^\s*#/o ) {
	    $iSeminixVersion      = $. if m/^\s*#\s*include\s*<seminix\/version\.h>/o;
	}

	# Look for uses: SEMINIX_VERSION_CODE, KERNEL_VERSION,
	# SEMINIX_VERSION_MAJOR, SEMINIX_VERSION_PATCHLEVEL, SEMINIX_VERSION_SUBLEVEL
	if (($_ =~ /SEMINIX_VERSION_CODE/) || ($_ =~ /\WKERNEL_VERSION/) ||
	    ($_ =~ /SEMINIX_VERSION_MAJOR/) || ($_ =~ /SEMINIX_VERSION_PATCHLEVEL/) ||
	    ($_ =~ /SEMINIX_VERSION_SUBLEVEL/)) {
	    $fUseVersion = 1;
            last if $iSeminixVersion;
        }
    }

    # Report used version IDs without include?
    if ($fUseVersion && ! $iSeminixVersion) {
	print "$file: $.: need seminix/version.h\n";
    }

    # Report superfluous includes.
    if ($iSeminixVersion && ! $fUseVersion) {
	print "$file: $iSeminixVersion seminix/version.h not needed.\n";
    }

    # debug: report OK results:
    if ($debugging) {
        if ($iSeminixVersion && $fUseVersion) {
	    print "$file: version use is OK ($iSeminixVersion)\n";
        }
        if (! $iSeminixVersion && ! $fUseVersion) {
	    print "$file: version use is OK (none)\n";
        }
    }

    close($f);
}
