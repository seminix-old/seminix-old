#include <seminix/vmacache.h>
#include <seminix/pgtable.h>

#define VMACACHE_SHIFT	PMD_SHIFT
#define VMACACHE_HASH(addr) ((addr >> VMACACHE_SHIFT) & VMACACHE_MASK)

/*
 * This task may be accessing a foreign mm via (for example)
 * get_user_pages()->find_vma().  The vmacache is task-local and this
 * task's vmacache pertains to a different mm (ie, its own).  There is
 * nothing we can do here.
 *
 * Also handle the case where a kernel thread has adopted this mm via use_mm().
 * That kernel thread's vmacache is not applicable to this mm.
 */
static inline bool vmacache_valid_mm(struct mm_struct *mm)
{
    return current->mm == mm && !(current->flags & PF_KTHREAD);
}

void vmacache_update(unsigned long addr, struct vm_area_struct *newvma)
{
    if (vmacache_valid_mm(newvma->vm_mm))
        current->vmacache.vmas[VMACACHE_HASH(addr)] = newvma;
}

static bool vmacache_valid(struct mm_struct *mm)
{
    struct task_struct *curr;

    if (!vmacache_valid_mm(mm))
        return false;

    curr = current;
    if (mm->vmacache_seqnum != curr->vmacache.seqnum) {
        /*
         * First attempt will always be invalid, initialize
         * the new cache for this task here.
         */
        curr->vmacache.seqnum = mm->vmacache_seqnum;
        vmacache_flush(curr);
        return false;
    }
    return true;
}

struct vm_area_struct *vmacache_find(struct mm_struct *mm, unsigned long addr)
{
    int idx = VMACACHE_HASH(addr);
    int i;

    if (!vmacache_valid(mm))
        return NULL;

    for (i = 0; i < VMACACHE_SIZE; i++) {
        struct vm_area_struct *vma = current->vmacache.vmas[idx];

        if (vma) {
            if (vma->vm_start <= addr && vma->vm_end > addr) {
                return vma;
            }
        }
        if (++idx == VMACACHE_SIZE)
            idx = 0;
    }

    return NULL;
}
