// SPDX-License-Identifier: GPL-2.0
/*
 * mm/debug.c
 *
 * mm/ specific debug routines.
 *
 */
#include <seminix/mm.h>
#include <seminix/mmap.h>

void dump_page(struct page *page, const char *reason)
{
    bool page_poisoned = !page_is_initialized(page);
    int mapcount;

    /*
     * If struct page is poisoned don't access Page*() functions as that
     * leads to recursive loop. Page*() check for poisoned pages, and calls
     * dump_page() when detected.
     */
    if (page_poisoned) {
        pr_warn("page:%px is uninitialized and poisoned", page);
        goto hex_only;
    }

    /*
     * Avoid VM_BUG_ON() in page_mapcount().
     * page->mapcount space in struct page is used by sl[aou]b pages to
     * encode own info.
     */
    mapcount = page_is_slab(page) ? 0 : page_mapcount(page);

    pr_warn("page:%px refcount:%d mapcount:%d",
          page, page_ref_count(page), mapcount);
    pr_cont("\n");
    pr_warn("flags: %#lx(%pGp)\n", page->flags, &page->flags);
hex_only:
    print_hex_dump(KERN_WARNING, "raw: ", DUMP_PREFIX_NONE, 32,
            sizeof(unsigned long), page, sizeof(struct page),
            false, printk);

    if (reason)
        pr_warn("page dumped because: %s\n", reason);
}

#define __def_pageflag_names				    \
    {1UL << PG_locked,		"locked"	},		\
    {1UL << PG_active,		"active"	},		\
    {1UL << PG_table,		"table"  	},      \
    {1UL << PG_arch_1,		"arch_1"	},		\
    {1UL << PG_slab,		"slab"		},		\
    {1UL << PG_head,		"head"		},		\
    {1UL << PG_buddy,		"buddy"		},		\
    {1UL << PG_reserved,	"reserved"	},      \
    {1UL << PG_initialized, "initialized"}

const struct page_print_flags pageflag_names[] = {
    __def_pageflag_names,
    {0, NULL}
};

#define __def_gfpflag_names						\
    {(unsigned long)GFP_DMA,		"GFP_DMA"},	\
    {(unsigned long)GFP_NORMAL,	    "GFP_NORMAL"},  \
    {(unsigned long)GFP_ZERO,	    "GFP_ZERO"},    \
    {(unsigned long)GFP_NOWARN,		"GFP_NOWARN"},	\
    {(unsigned long)GFP_KERNEL,		"GFP_KERNEL"}

const struct page_print_flags gfpflag_names[] = {
    __def_gfpflag_names,
    {0, NULL}
};

#define __def_vmaflag_names						\
    {VM_READ,			"read"		},		\
    {VM_WRITE,			"write"		},		\
    {VM_EXEC,			"exec"		},		\
    {VM_SHARED,			"shared"	}

const struct page_print_flags vmaflag_names[] = {
    __def_vmaflag_names,
    {0, NULL}
};
