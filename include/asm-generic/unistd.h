/* SPDX-License-Identifier: GPL-2.0 WITH Linux-syscall-note */

#ifndef __SYSCALL
#define __SYSCALL(x, y)
#endif

#include <ulinux/asm/syscall.h>
