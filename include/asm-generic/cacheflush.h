/* SPDX-License-Identifier: GPL-2.0 */
#ifndef ASM_GENERIC_CACHEFLUSH_H
#define ASM_GENERIC_CACHEFLUSH_H

#ifndef flush_cache_vunmap
static inline void flush_cache_vunmap(unsigned long start, unsigned long end)
{
}
#endif

#endif /* !ASM_GENERIC_CACHEFLUSH_H */
