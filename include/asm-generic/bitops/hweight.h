/* SPDX-License-Identifier: GPL-2.0 */
#ifndef ASM_GENERIC_BITOPS_HWEIGHT_H
#define ASM_GENERIC_BITOPS_HWEIGHT_H

#include <asm-generic/bitops/arch_hweight.h>
#include <asm-generic/bitops/const_hweight.h>

#endif /* !ASM_GENERIC_BITOPS_HWEIGHT_H */
