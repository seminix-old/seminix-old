/* SPDX-License-Identifier: GPL-2.0 */
#ifndef ASM_GENERIC_MEMORY_MODEL_H
#define ASM_GENERIC_MEMORY_MODEL_H

#include <seminix/pfn.h>

#ifndef __ASSEMBLY__

/* memmap is virtually contiguous.  */
#define __pfn_to_page(pfn)	(vmemmap + (pfn))
#define __page_to_pfn(page)	(unsigned long)((page) - vmemmap)

#define page_to_pfn __page_to_pfn
#define pfn_to_page __pfn_to_page

#endif /* !__ASSEMBLY__ */
#endif /* !ASM_GENERIC_MEMORY_MODEL_H */
