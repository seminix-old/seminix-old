/* SPDX-License-Identifier: GPL-2.0 */
/* rwsem.h: R/W semaphores, public interface
 *
 * Written by David Howells (dhowells@redhat.com).
 * Derived from asm-i386/semaphore.h
 */

#ifndef SEMINIX_RWSEM_H
#define SEMINIX_RWSEM_H

#include <seminix/types.h>
#include <seminix/err.h>
#include <seminix/list.h>
#include <seminix/atomic.h>
#include <seminix/linkage.h>
#include <seminix/spinlock.h>

struct rw_semaphore;

/*
 * the rw-semaphore definition
 * - if count is 0 then there are no active readers or writers
 * - if count is +ve then that is the number of active readers
 * - if count is -1 then there is one active writer
 * - if wait_list is not empty, then there are processes waiting for the semaphore
 */
struct rw_semaphore {
    __i32			count;
    raw_spinlock_t		wait_lock;
    struct list_head	wait_list;
};

#define RWSEM_UNLOCKED_VALUE		0x00000000
#define __RWSEM_INIT_COUNT(name)	.count = RWSEM_UNLOCKED_VALUE

extern void __down_read(struct rw_semaphore *sem);
extern int __down_read_killable(struct rw_semaphore *sem);
extern int __down_read_trylock(struct rw_semaphore *sem);
extern void __down_write(struct rw_semaphore *sem);
extern int __down_write_killable(struct rw_semaphore *sem);
extern int __down_write_trylock(struct rw_semaphore *sem);
extern void __up_read(struct rw_semaphore *sem);
extern void __up_write(struct rw_semaphore *sem);
extern void __downgrade_write(struct rw_semaphore *sem);
extern int rwsem_is_locked(struct rw_semaphore *sem);

#define __RWSEM_INITIALIZER(name)				\
    { __RWSEM_INIT_COUNT(name),				\
      .wait_list = LIST_HEAD_INIT((name).wait_list),	\
      .wait_lock = __RAW_SPIN_LOCK_UNLOCKED(name.wait_lock)}

#define DECLARE_RWSEM(name) \
    struct rw_semaphore name = __RWSEM_INITIALIZER(name)

extern void init_rwsem(struct rw_semaphore *sem);

/*
 * This is the same regardless of which rwsem implementation that is being used.
 * It is just a heuristic meant to be called by somebody alreadying holding the
 * rwsem to see if somebody from an incompatible type is wanting access to the
 * lock.
 */
static inline int rwsem_is_contended(struct rw_semaphore *sem)
{
    return !list_empty(&sem->wait_list);
}

/*
 * lock for reading
 */
extern void down_read(struct rw_semaphore *sem);
extern int down_read_killable(struct rw_semaphore *sem);

/*
 * trylock for reading -- returns 1 if successful, 0 if contention
 */
extern int down_read_trylock(struct rw_semaphore *sem);

/*
 * lock for writing
 */
extern void down_write(struct rw_semaphore *sem);
extern int down_write_killable(struct rw_semaphore *sem);

/*
 * trylock for writing -- returns 1 if successful, 0 if contention
 */
extern int down_write_trylock(struct rw_semaphore *sem);

/*
 * release a read lock
 */
extern void up_read(struct rw_semaphore *sem);

/*
 * release a write lock
 */
extern void up_write(struct rw_semaphore *sem);

/*
 * downgrade write lock to read lock
 */
extern void downgrade_write(struct rw_semaphore *sem);

#define down_read_nested(sem, subclass)		down_read(sem)
#define down_write_nest_lock(sem, nest_lock)	down_write(sem)
#define down_write_nested(sem, subclass)	down_write(sem)
#define down_write_killable_nested(sem, subclass)	down_write_killable(sem)
#define down_read_non_owner(sem)		down_read(sem)
#define up_read_non_owner(sem)			up_read(sem)

#endif /* !SEMINIX_RWSEM_H */
