/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _SEMINIX_BSEARCH_H
#define _SEMINIX_BSEARCH_H

#include <seminix/types.h>

void *bsearch(const void *key, const void *base, usize num, usize size,
          int (*cmp)(const void *key, const void *elt));

#endif /* _SEMINIX_BSEARCH_H */
