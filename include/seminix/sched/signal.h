/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _SEMINIX_SCHED_SIGNAL_H
#define _SEMINIX_SCHED_SIGNAL_H

#include <seminix/rculist.h>
#include <seminix/signal.h>
#include <seminix/sched.h>
#include <seminix/wait.h>
#include <seminix/resource.h>
#include <seminix/sched/jobctl.h>
#include <seminix/spinlock.h>

struct sighand_struct {
    atomic_t		count;
    struct k_sigaction	action[_NSIG];
    spinlock_t		siglock;
    wait_queue_head_t	signalfd_wqh;
};

/*
 * NOTE! "signal_struct" does not have its own
 * locking, because a shared signal_struct always
 * implies a shared sighand_struct, so locking
 * sighand_struct is always a proper superset of
 * the locking of signal_struct.
 */
struct signal_struct {
    atomic_t		sigcnt;
    atomic_t		live;
    int			nr_threads;
    struct list_head	thread_head;

    /* shared signal handling: */
    struct sigpending	shared_pending;

    /* thread group exit support */
    int			group_exit_code;
    /* overloaded:
     * - notify group_exit_task when ->count is equal to notify_count
     * - everyone except group_exit_task is stopped during signal delivery
     *   of fatal signals, group_exit_task processes the signal.
     */
    int			notify_count;
    struct task_struct	*group_exit_task;

	/* thread group stop support, overloads group_exit_code too */
	int			group_stop_count;
	unsigned int		flags; /* see SIGNAL_* flags below */

    /* PID/PID hash table linkage. */
    struct pid *pids[PIDTYPE_MAX];

    /*
     * We don't bother to synchronize most readers of this at all,
     * because there is no reader checking a limit that actually needs
     * to get both rlim_cur and rlim_max atomically, and either one
     * alone is a single word that can safely be read normally.
     * getrlimit/setrlimit use task_lock(current->group_leader) to
     * protect this instead of the siglock, because they really
     * have no need to disable irqs.
     */
    struct rlimit rlim[RLIM_NLIMITS];
} __randomize_layout;

/*
 * Bits in flags field of signal_struct.
 */
#define SIGNAL_STOP_STOPPED	0x00000001 /* job control stop in effect */
#define SIGNAL_STOP_CONTINUED	0x00000002 /* SIGCONT since WCONTINUED reap */
#define SIGNAL_GROUP_EXIT	0x00000004 /* group exit in progress */
#define SIGNAL_GROUP_COREDUMP	0x00000008 /* coredump in progress */
/*
 * Pending notifications to parent.
 */
#define SIGNAL_CLD_STOPPED	0x00000010
#define SIGNAL_CLD_CONTINUED	0x00000020
#define SIGNAL_CLD_MASK		(SIGNAL_CLD_STOPPED|SIGNAL_CLD_CONTINUED)

#define SIGNAL_UNKILLABLE	0x00000040 /* for init: ignore fatal signals */

#define SIGNAL_STOP_MASK (SIGNAL_CLD_MASK | SIGNAL_STOP_STOPPED | \
              SIGNAL_STOP_CONTINUED)

static inline void signal_set_stop_flags(struct signal_struct *sig,
                     unsigned int flags)
{
    WARN_ON(sig->flags & (SIGNAL_GROUP_EXIT|SIGNAL_GROUP_COREDUMP));
    sig->flags = (sig->flags & ~SIGNAL_STOP_MASK) | flags;
}

/* If true, all threads except ->group_exit_task have pending SIGKILL */
static inline int signal_group_exit(const struct signal_struct *sig)
{
    return	(sig->flags & SIGNAL_GROUP_EXIT) ||
        (sig->group_exit_task != NULL);
}

extern void flush_signals(struct task_struct *);
extern void ignore_signals(struct task_struct *);
extern void flush_signal_handlers(struct task_struct *, int force_default);
extern int dequeue_signal(struct task_struct *tsk, sigset_t *mask, kernel_siginfo_t *info);

static inline int kernel_dequeue_signal(void)
{
    struct task_struct *tsk = current;
    kernel_siginfo_t __info;
    int ret;

    spin_lock_irq(&tsk->sighand->siglock);
    ret = dequeue_signal(tsk, &tsk->blocked, &__info);
    spin_unlock_irq(&tsk->sighand->siglock);

    return ret;
}

static inline void kernel_signal_stop(void)
{
    spin_lock_irq(&current->sighand->siglock);
    if (current->jobctl & JOBCTL_STOP_DEQUEUED)
        set_special_state(TASK_STOPPED);
    spin_unlock_irq(&current->sighand->siglock);

    schedule();
}

#ifdef __ARCH_SI_TRAPNO
# define ___ARCH_SI_TRAPNO(_a1) , _a1
#else
# define ___ARCH_SI_TRAPNO(_a1)
#endif
#ifdef __ia64__
# define ___ARCH_SI_IA64(_a1, _a2, _a3) , _a1, _a2, _a3
#else
# define ___ARCH_SI_IA64(_a1, _a2, _a3)
#endif

int force_sig_fault(int sig, int code, void __user *addr
    ___ARCH_SI_TRAPNO(int trapno)
    ___ARCH_SI_IA64(int imm, unsigned int flags, unsigned long isr)
    , struct task_struct *t);
int send_sig_fault(int sig, int code, void __user *addr
    ___ARCH_SI_TRAPNO(int trapno)
    ___ARCH_SI_IA64(int imm, unsigned int flags, unsigned long isr)
    , struct task_struct *t);

int force_sig_mceerr(int code, void __user *, short, struct task_struct *);
int send_sig_mceerr(int code, void __user *, short, struct task_struct *);

int force_sig_bnderr(void __user *addr, void __user *lower, void __user *upper);
int force_sig_pkuerr(void __user *addr, u32 pkey);

int force_sig_ptrace_errno_trap(int errno, void __user *addr);

extern int send_sig_info(int, struct kernel_siginfo *, struct task_struct *);
extern void force_sigsegv(int sig, struct task_struct *p);
extern int force_sig_info(int, struct kernel_siginfo *, struct task_struct *);
extern int __kill_pgrp_info(int sig, struct kernel_siginfo *info, struct pid *pgrp);
extern int kill_pid_info(int sig, struct kernel_siginfo *info, struct pid *pid);
extern int kill_pgrp(struct pid *pid, int sig, int priv);
extern int kill_pid(struct pid *pid, int sig, int priv);
extern __must_check bool do_notify_parent(struct task_struct *, int);
extern void __wake_up_parent(struct task_struct *p, struct task_struct *parent);
extern void force_sig(int, struct task_struct *);
extern int send_sig(int, struct task_struct *, int);
extern int zap_other_threads(struct task_struct *p);
extern struct sigqueue *sigqueue_alloc(void);
extern void sigqueue_free(struct sigqueue *);
extern int send_sigqueue(struct sigqueue *, struct pid *, enum pid_type);
extern int do_sigaction(int, struct k_sigaction *, struct k_sigaction *);

static inline int restart_syscall(void)
{
    set_tsk_thread_flag(current, TIF_SIGPENDING);
    return -ERESTARTNOINTR;
}

static inline int signal_pending(struct task_struct *p)
{
    return unlikely(test_tsk_thread_flag(p,TIF_SIGPENDING));
}

static inline int __fatal_signal_pending(struct task_struct *p)
{
    return unlikely(sigismember(&p->pending.signal, SIGKILL));
}

static inline int fatal_signal_pending(struct task_struct *p)
{
    return signal_pending(p) && __fatal_signal_pending(p);
}

static inline int signal_pending_state(long state, struct task_struct *p)
{
    if (!(state & (TASK_INTERRUPTIBLE | TASK_WAKEKILL)))
        return 0;
    if (!signal_pending(p))
        return 0;

    return (state & TASK_INTERRUPTIBLE) || __fatal_signal_pending(p);
}

/*
 * Reevaluate whether the task has signals pending delivery.
 * Wake the task if so.
 * This is required every time the blocked sigset_t changes.
 * callers must hold sighand->siglock.
 */
extern void recalc_sigpending_and_wake(struct task_struct *t);
extern void recalc_sigpending(void);
extern void calculate_sigpending(void);

extern void signal_wake_up_state(struct task_struct *t, unsigned int state);

static inline void signal_wake_up(struct task_struct *t, bool resume)
{
    signal_wake_up_state(t, resume ? TASK_WAKEKILL : 0);
}

void task_join_group_stop(struct task_struct *task);

#ifdef TIF_RESTORE_SIGMASK
/*
 * Legacy restore_sigmask accessors.  These are inefficient on
 * SMP architectures because they require atomic operations.
 */

/**
 * set_restore_sigmask() - make sure saved_sigmask processing gets done
 *
 * This sets TIF_RESTORE_SIGMASK and ensures that the arch signal code
 * will run before returning to user mode, to process the flag.  For
 * all callers, TIF_SIGPENDING is already set or it's no harm to set
 * it.  TIF_RESTORE_SIGMASK need not be in the set of bits that the
 * arch code will notice on return to user mode, in case those bits
 * are scarce.  We set TIF_SIGPENDING here to ensure that the arch
 * signal code always gets run when TIF_RESTORE_SIGMASK is set.
 */
static inline void set_restore_sigmask(void)
{
    set_thread_flag(TIF_RESTORE_SIGMASK);
    WARN_ON(!test_thread_flag(TIF_SIGPENDING));
}
static inline void clear_restore_sigmask(void)
{
    clear_thread_flag(TIF_RESTORE_SIGMASK);
}
static inline bool test_restore_sigmask(void)
{
    return test_thread_flag(TIF_RESTORE_SIGMASK);
}
static inline bool test_and_clear_restore_sigmask(void)
{
    return test_and_clear_thread_flag(TIF_RESTORE_SIGMASK);
}

#else	/* TIF_RESTORE_SIGMASK */

/* Higher-quality implementation, used if TIF_RESTORE_SIGMASK doesn't exist. */
static inline void set_restore_sigmask(void)
{
    current->restore_sigmask = true;
    WARN_ON(!test_thread_flag(TIF_SIGPENDING));
}
static inline void clear_restore_sigmask(void)
{
    current->restore_sigmask = false;
}
static inline bool test_restore_sigmask(void)
{
    return current->restore_sigmask;
}
static inline bool test_and_clear_restore_sigmask(void)
{
    if (!current->restore_sigmask)
        return false;
    current->restore_sigmask = false;
    return true;
}
#endif

static inline void restore_saved_sigmask(void)
{
    if (test_and_clear_restore_sigmask())
        __set_current_blocked(&current->saved_sigmask);
}

static inline sigset_t *sigmask_to_save(void)
{
    sigset_t *res = &current->blocked;
    if (unlikely(test_restore_sigmask()))
        res = &current->saved_sigmask;
    return res;
}

/* These can be the second arg to send_sig_info/send_group_sig_info.  */
#define SEND_SIG_NOINFO ((struct kernel_siginfo *) 0)
#define SEND_SIG_PRIV	((struct kernel_siginfo *) 1)

/*
 * True if we are on the alternate signal stack.
 */
static inline int on_sig_stack(unsigned long sp)
{
    /*
     * If the signal stack is SS_AUTODISARM then, by construction, we
     * can't be on the signal stack unless user code deliberately set
     * SS_AUTODISARM when we were already on it.
     *
     * This improves reliability: if user state gets corrupted such that
     * the stack pointer points very close to the end of the signal stack,
     * then this check will enable the signal to be handled anyway.
     */
    if (current->sas_ss_flags & SS_AUTODISARM)
        return 0;

    return sp > current->sas_ss_sp &&
        sp - current->sas_ss_sp <= current->sas_ss_size;
}

static inline int sas_ss_flags(unsigned long sp)
{
    if (!current->sas_ss_size)
        return SS_DISABLE;

    return on_sig_stack(sp) ? SS_ONSTACK : 0;
}

static inline void sas_ss_reset(struct task_struct *p)
{
    p->sas_ss_sp = 0;
    p->sas_ss_size = 0;
    p->sas_ss_flags = SS_DISABLE;
}

static inline unsigned long sigsp(unsigned long sp, struct ksignal *ksig)
{
    if (unlikely((ksig->ka.sa.sa_flags & SA_ONSTACK)) && ! sas_ss_flags(sp))
        return current->sas_ss_sp + current->sas_ss_size;
    return sp;
}

extern void __cleanup_sighand(struct sighand_struct *);
extern void flush_itimer_signals(void);

#define tasklist_empty() \
    list_empty(&init_task.tasks)

#define next_task(p) \
    list_entry_rcu((p)->tasks.next, struct task_struct, tasks)

#define for_each_process(p) \
    for (p = &init_task ; (p = next_task(p)) != &init_task ; )

extern bool current_is_single_threaded(void);

/*
 * Careful: do_each_thread/while_each_thread is a double loop so
 *          'break' will not work as expected - use goto instead.
 */
#define do_each_thread(g, t) \
    for (g = t = &init_task ; (g = t = next_task(g)) != &init_task ; ) do

#define while_each_thread(g, t) \
    while ((t = next_thread(t)) != g)

#define __for_each_thread(signal, t)	\
    list_for_each_entry_rcu(t, &(signal)->thread_head, thread_node)

#define for_each_thread(p, t)		\
    __for_each_thread((p)->signal, t)

/* Careful: this is a double loop, 'break' won't work as expected. */
#define for_each_process_thread(p, t)	\
    for_each_process(p) for_each_thread(p, t)

typedef int (*proc_visitor)(struct task_struct *p, void *data);
void walk_process_tree(struct task_struct *top, proc_visitor, void *);

static inline
struct pid *task_pid_type(struct task_struct *task, enum pid_type type)
{
    struct pid *pid;
    if (type == PIDTYPE_PID)
        pid = task_pid(task);
    else
        pid = task->signal->pids[type];
    return pid;
}

static inline struct pid *task_tgid(struct task_struct *task)
{
    return task->signal->pids[PIDTYPE_TGID];
}

/*
 * Without tasklist or RCU lock it is not safe to dereference
 * the result of task_pgrp/task_session even if task == current,
 * we can race with another thread doing sys_setsid/sys_setpgid.
 */
static inline struct pid *task_pgrp(struct task_struct *task)
{
    return task->signal->pids[PIDTYPE_PGID];
}

static inline struct pid *task_session(struct task_struct *task)
{
    return task->signal->pids[PIDTYPE_SID];
}

static inline int get_nr_threads(struct task_struct *tsk)
{
    return tsk->signal->nr_threads;
}

static inline bool thread_group_leader(struct task_struct *p)
{
    return p->exit_signal >= 0;
}

/* Do to the insanities of de_thread it is possible for a process
 * to have the pid of the thread group leader without actually being
 * the thread group leader.  For iteration through the pids in proc
 * all we care about is that we have a task with the appropriate
 * pid, we don't actually care if we have the right task.
 */
static inline bool has_group_leader_pid(struct task_struct *p)
{
    return task_pid(p) == task_tgid(p);
}

static inline
bool same_thread_group(struct task_struct *p1, struct task_struct *p2)
{
    return p1->signal == p2->signal;
}

static inline struct task_struct *next_thread(const struct task_struct *p)
{
    return list_entry_rcu(p->thread_group.next,
                  struct task_struct, thread_group);
}

static inline int thread_group_empty(struct task_struct *p)
{
    return list_empty(&p->thread_group);
}

#define delay_group_leader(p) \
        (thread_group_leader(p) && !thread_group_empty(p))

extern struct sighand_struct *__lock_task_sighand(struct task_struct *tsk,
                            unsigned long *flags);

static inline struct sighand_struct *lock_task_sighand(struct task_struct *tsk,
                               unsigned long *flags)
{
    struct sighand_struct *ret;

    ret = __lock_task_sighand(tsk, flags);
    (void)__cond_lock(&tsk->sighand->siglock, ret);
    return ret;
}

static inline void unlock_task_sighand(struct task_struct *tsk,
                        unsigned long *flags)
{
    spin_unlock_irqrestore(&tsk->sighand->siglock, *flags);
}

static inline unsigned long task_rlimit(const struct task_struct *tsk,
        unsigned int limit)
{
    return READ_ONCE(tsk->signal->rlim[limit].rlim_cur);
}

static inline unsigned long task_rlimit_max(const struct task_struct *tsk,
        unsigned int limit)
{
    return READ_ONCE(tsk->signal->rlim[limit].rlim_max);
}

static inline unsigned long rlimit(unsigned int limit)
{
    return task_rlimit(current, limit);
}

static inline unsigned long rlimit_max(unsigned int limit)
{
    return task_rlimit_max(current, limit);
}

#endif /* _SEMINIX_SCHED_SIGNAL_H */
