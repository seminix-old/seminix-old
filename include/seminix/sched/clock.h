/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_SCHED_CLOCK_H
#define SEMINIX_SCHED_CLOCK_H

#include <seminix/types.h>

extern u64 sched_clock(void);
extern u64 sched_clock_cpu(int cpu);

static inline u64 local_clock(void)
{
    return sched_clock();
}

void sched_clock_init(void);

void sched_clock_register(u64 (*read)(void), int bits, unsigned long rate);

int sched_clock_suspend(void);
void sched_clock_resume(void);

#endif /* !SEMINIX_SCHED_CLOCK_H */
