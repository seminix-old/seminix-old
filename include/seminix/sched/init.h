#ifndef SEMINIX_SCHED_INIT_H
#define SEMINIX_SCHED_INIT_H

extern void sched_init(void);
extern void sched_init_smp(void);

#endif
