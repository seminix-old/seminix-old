#ifndef SEMINIX_SCHED_RT_H
#define SEMINIX_SCHED_RT_H

#include <seminix/sched.h>

struct task_struct;

static inline int rt_prio(int prio)
{
    if (unlikely(prio < MAX_RT_PRIO))
        return 1;
    return 0;
}

static inline int rt_task(struct task_struct *p)
{
    return rt_prio(p->prio);
}

static inline bool task_is_realtime(struct task_struct *tsk)
{
    int policy = tsk->policy;

    if (policy == SCHED_FIFO || policy == SCHED_RR)
        return true;
    if (policy == SCHED_DEADLINE)
        return true;
    return false;
}

#ifndef rt_mutex_adjust_pi
#define rt_mutex_adjust_pi(p) do {} while (0)
#endif

/*
 * default timeslice is 100 msecs (used only for SCHED_RR tasks).
 * Timeslices get refilled after they expire.
 */
#define RR_TIMESLICE		(100 * HZ / 1000)

#endif /* !SEMINIX_SCHED_RT_H */
