/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _SEMINIX_SCHED_TOPOLOGY_H
#define _SEMINIX_SCHED_TOPOLOGY_H

/*
 * Increase resolution of cpu_capacity calculations
 */
#define SCHED_CAPACITY_SHIFT	SCHED_FIXEDPOINT_SHIFT
#define SCHED_CAPACITY_SCALE	(1L << SCHED_CAPACITY_SHIFT)

#ifndef arch_scale_freq_capacity
static __always_inline
unsigned long arch_scale_freq_capacity(int cpu)
{
    return SCHED_CAPACITY_SCALE;
}
#endif

#ifndef arch_scale_cpu_capacity
static __always_inline
unsigned long arch_scale_cpu_capacity(void __always_unused *sd, int cpu)
{
    return SCHED_CAPACITY_SCALE;
}
#endif

#define SD_BALANCE_EXEC		0x0001	/* Balance on exec */
#define SD_BALANCE_FORK		0x0002	/* Balance on fork, clone */
#define SD_BALANCE_WAKE		0x0004  /* Balance on wakeup */
#define SD_WAKE_AFFINE		0x0008	/* Wake task to waking CPU */

#endif
