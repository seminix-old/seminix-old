#ifndef SEMINIX_SCHED_CPUFREQ_H
#define SEMINIX_SCHED_CPUFREQ_H

struct rq;

/*
 * Interface between cpufreq drivers and the scheduler:
 */

#define SCHED_CPUFREQ_IOWAIT	(1U << 0)
#define SCHED_CPUFREQ_MIGRATION	(1U << 1)

#ifndef cpufreq_update_util
static inline void cpufreq_update_util(struct rq *rq, unsigned int flags) {}
#endif

#endif /* SEMINIX_SCHED_CPUFREQ_H */
