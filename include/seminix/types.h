/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_TYPES_H
#define SEMINIX_TYPES_H

#ifndef __ASSEMBLY__

#include <asm/bitsperlong.h>

#if __BITS_PER_LONG != 64
typedef unsigned int usize;
typedef __signed__ int isize;
typedef int        ptrdiff_t;
#else
typedef unsigned long usize;
typedef __signed__ long isize;
typedef long       ptrdiff_t;
#endif

typedef unsigned char __u8;
typedef __signed__ char __i8;

typedef unsigned short __u16;
typedef __signed__ short __i16;

typedef unsigned int __u32;
typedef __signed__ int __i32;

#ifdef __GNUC__
__extension__ typedef unsigned long long __u64;
__extension__ typedef __signed__ long long __i64;
#else
typedef unsigned long long __u64;
typedef __signed__ long long __i64;
#endif

typedef __u8 u8;
typedef __i8 i8;

typedef __u16 u16;
typedef __i16 i16;

typedef __u32 u32;
typedef __i32 i32;

typedef __u64 u64;
typedef __i64 i64;

#define __bitwise

typedef __u16 __bitwise __le16;
typedef __u16 __bitwise __be16;
typedef __u32 __bitwise __le32;
typedef __u32 __bitwise __be32;
typedef __u64 __bitwise __le64;
typedef __u64 __bitwise __be64;

typedef usize phys_addr_t;
typedef usize dma_addr_t;

typedef phys_addr_t resource_size_t;

typedef _Bool			bool;

typedef u32		uid_t;
typedef u32		gid_t;

typedef unsigned long		uintptr_t;

typedef unsigned int __bitwise gfp_t;

typedef long suseconds_t;
typedef int clockid_t;

typedef long clock_t;

typedef int pid_t;
typedef unsigned int uid32_t;

typedef int timer_t;

typedef long off_t;
typedef long long loff_t;

typedef unsigned __bitwise __poll_t;

#define S8_C(x)  x
#define U8_C(x)  x ## U
#define S16_C(x) x
#define U16_C(x) x ## U
#define S32_C(x) x
#define U32_C(x) x ## U
#define S64_C(x) x ## LL
#define U64_C(x) x ## ULL

typedef struct {
    i32 counter;
} atomic_t;

#define ATOMIC_INIT(i) { (i) }

#ifdef CONFIG_64BIT
typedef struct {
    long long counter;
} atomic64_t;
#endif

/**
 * struct callback_head - callback structure for use with RCU and task_work
 * @next: next update requests in a list
 * @func: actual update function to call after the grace period.
 *
 * The struct is aligned to size of pointer. On most architectures it happens
 * naturally due ABI requirements, but some architectures (like CRIS) have
 * weird ABI and we need to ask it explicitly.
 *
 * The alignment is required to guarantee that bit 0 of @next will be
 * clear under normal conditions -- as long as we use call_rcu() or
 * call_srcu() to queue the callback.
 *
 * This guarantee is important for few reasons:
 *  - future call_rcu_lazy() will make use of lower bits in the pointer;
 *  - the structure shares storage space in struct page with @compound_head,
 *    which encode PageTail() in bit 0. The guarantee is needed to avoid
 *    false-positive PageTail().
 */
struct callback_head {
    struct callback_head *next;
    void (*func)(struct callback_head *head);
} __attribute__((aligned(sizeof(void *))));
#define rcu_head callback_head

typedef void (*rcu_callback_t)(struct rcu_head *head);
typedef void (*call_rcu_func_t)(struct rcu_head *head, rcu_callback_t func);

struct sched_param {
	int sched_priority;
};

#define SCHED_ATTR_SIZE_VER0	48	/* sizeof first published struct */

struct sched_attr {
	__u32 size;

	__u32 sched_policy;
	__u64 sched_flags;

	/* SCHED_NORMAL, SCHED_BATCH */
	__i32 sched_nice;

	/* SCHED_FIFO, SCHED_RR */
	__u32 sched_priority;

	/* SCHED_DEADLINE */
	__u64 sched_runtime;
	__u64 sched_deadline;
	__u64 sched_period;
};

#else

#define S8_C(x)  x
#define U8_C(x)  x
#define S16_C(x) x
#define U16_C(x) x
#define S32_C(x) x
#define U32_C(x) x
#define S64_C(x) x
#define U64_C(x) x

#endif /* !__ASSEMBLY__ */

#include <seminix/compiler.h>
#include <seminix/stddef.h>

#endif /* !SEMINIX_TYPES_H */
