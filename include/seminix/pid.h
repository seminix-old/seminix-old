/* SPDX-License-Identifier: GPL-2.0 */
#ifndef _SEMINIX_PID_H
#define _SEMINIX_PID_H

#include <seminix/rculist.h>

enum pid_type {
    PIDTYPE_PID,
    PIDTYPE_TGID,
    PIDTYPE_PGID,
    PIDTYPE_SID,
    PIDTYPE_MAX,
};

struct pid {
    atomic_t count;
    /* lists of tasks that use this pid */
    struct hlist_head tasks[PIDTYPE_MAX];
    struct rcu_head rcu;
    int nr;
};

#endif /* _SEMINIX_PID_H */
