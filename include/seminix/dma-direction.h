/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_DMA_DIRECTION_H
#define SEMINIX_DMA_DIRECTION_H

#define	DMA_BIDIRECTIONAL	0
#define	DMA_TO_DEVICE		1
#define DMA_FROM_DEVICE		2
#define	DMA_NONE			3

#endif /* !SEMINIX_DMA_DIRECTION_H */
