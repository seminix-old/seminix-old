#ifndef SEMINIX_SYSCTL_H
#define SEMINIX_SYSCTL_H

// task 最大可以 mmap 的数量
extern int sysctl_max_map_count;

// rt 调度器 rr 执行时间片(ms)
extern int sysctl_sched_rr_timeslice;

extern unsigned int sysctl_sched_latency;
extern unsigned int sysctl_sched_min_granularity;
extern unsigned int sysctl_sched_wakeup_granularity;
extern unsigned int sysctl_sched_child_runs_first;

extern unsigned int sysctl_sched_rt_period;
extern int sysctl_sched_rt_runtime;

enum sched_tunable_scaling {
    SCHED_TUNABLESCALING_NONE,
    SCHED_TUNABLESCALING_LOG,
    SCHED_TUNABLESCALING_LINEAR,
    SCHED_TUNABLESCALING_END,
};
extern enum sched_tunable_scaling sysctl_sched_tunable_scaling;

extern int sysctl_sched_rt_handler(usize rt_period, usize rt_runtime);
extern int sysctl_sched_rr_handler(usize timeslice);

#endif /* !SEMINIX_SYSCTL_H */
