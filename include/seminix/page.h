#ifndef SEMINIX_PAGE_H
#define SEMINIX_PAGE_H

#include <seminix/string.h>
#include <seminix/page_types.h>
#include <seminix/pagesize.h>
#include <asm/page.h>

#ifndef __ARCH_HAVE_CLEAR_PAGE
#define clear_page(page)	memset((page), 0, PAGE_SIZE)
#endif

#ifndef __ARCH_HAVE_COPY_PAGE
#define copy_page(to,from)	memcpy((to), (from), PAGE_SIZE)
#endif

#endif /* !SEMINIX_PAGE_H */
