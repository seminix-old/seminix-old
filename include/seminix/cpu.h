#ifndef SEMINIX_CPU_H
#define SEMINIX_CPU_H

#include <seminix/types.h>
#include <asm/cpu.h>

struct device_node;

void boot_cpu_init(void);
int boot_cpu_id(void);

extern bool arch_match_cpu_phys_id(int cpu, u64 phys_id);
extern bool arch_find_n_match_cpu_physical_id(struct device_node *cpun,
                int cpu, unsigned int *thread);

enum cpuhp_state {
    CPUHP_INVALID = -1,
    CPUHP_OFFLINE = 0,
    CPUHP_RADIX_DEAD,
    CPUHP_IRQ_GIC_STARTING,
    CPUHP_ARM_ARCH_TIMER_STARTING,
    CPUHP_HRTIMERS_PREPARE,
    CPUHP_SYSTEM_TICK_STARTING,
    CPUHP_ONLINE,
};

extern int cpuhp_register_callback(int (*startup)(int cpu), const char *name, enum cpuhp_state state);
extern void notify_cpu_starting(int cpu);

extern void cpu_startup_entry(void);

extern void cpu_idle_poll_ctrl(bool enable);

extern void arch_cpu_idle(void);
extern void arch_cpu_idle_prepare(void);
extern void arch_cpu_idle_enter(void);
extern void arch_cpu_idle_exit(void);
extern void arch_cpu_idle_dead(void);

#endif /* !SEMINIX_CPU_H */
