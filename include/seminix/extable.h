/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_EXTABLE_H
#define SEMINIX_EXTABLE_H

#include <seminix/types.h>
#include <asm/extable.h>

const struct exception_table_entry *
search_extable(const struct exception_table_entry *base,
           const usize num,
           unsigned long value);
extern void sort_extable(struct exception_table_entry *start,
          struct exception_table_entry *finish);
extern void sort_main_extable(void);

/* Given an address, look for it in the exception tables */
const struct exception_table_entry *search_exception_tables(unsigned long add);

#endif /* !SEMINIX_EXTABLE_H */
