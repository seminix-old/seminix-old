#ifndef SEMINIX_MMDEBUG_H
#define SEMINIX_MMDEBUG_H

struct page;

extern void dump_page(struct page *page, const char *reason);

struct page_print_flags {
    unsigned long mask;
    const char  *name;
};
extern const struct page_print_flags pageflag_names[];
extern const struct page_print_flags vmaflag_names[];
extern const struct page_print_flags gfpflag_names[];

#endif /* !SEMINIX_MMDEBUG_H */
