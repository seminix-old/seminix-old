/*
 * Copyright (c) 2008 Intel Corporation
 * Author: Matthew Wilcox <willy@linux.intel.com>
 *
 * Distributed under the terms of the GNU GPL, version 2
 *
 * Please see kernel/locking/semaphore.c for documentation of these functions
 */
#ifndef SEMINIX_SEMAPHORE_H
#define SEMINIX_SEMAPHORE_H

#include <seminix/list.h>
#include <seminix/spinlock.h>

/* Please don't access any members of this structure directly */
struct semaphore {
    raw_spinlock_t		lock;
    unsigned int		count;
    struct list_head	wait_list;
};

#define __SEMAPHORE_INITIALIZER(name, n)				\
{									\
    .lock		= __RAW_SPIN_LOCK_UNLOCKED((name).lock),	\
    .count		= n,						\
    .wait_list	= LIST_HEAD_INIT((name).wait_list),		\
}

#define DEFINE_SEMAPHORE(name)	\
    struct semaphore name = __SEMAPHORE_INITIALIZER(name, 1)

static inline void sema_init(struct semaphore *sem, int val)
{
    *sem = (struct semaphore) __SEMAPHORE_INITIALIZER(*sem, val);
}

extern void down(struct semaphore *sem);
extern int down_interruptible(struct semaphore *sem);
extern int down_killable(struct semaphore *sem);
extern int down_trylock(struct semaphore *sem);
extern int down_timeout(struct semaphore *sem, long jiffies);
extern void up(struct semaphore *sem);

#endif /* SEMINIX_SEMAPHORE_H */
