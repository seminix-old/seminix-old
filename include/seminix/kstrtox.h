/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_KSTRTOX_H
#define SEMINIX_KSTRTOX_H

#define KSTRTOX_OVERFLOW	(1U << 31)

const char *_parse_integer_fixup_radix(const char *s, unsigned int *base);
unsigned int _parse_integer(const char *s, unsigned int base, unsigned long long *res);

#endif /* !SEMINIX_KSTRTOX_H */
