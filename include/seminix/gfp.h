#ifndef SEMINIX_GFP_H
#define SEMINIX_GFP_H

#include <seminix/mmdebug.h>
#include <seminix/mmzone.h>
#include <seminix/page_types.h>

typedef unsigned int __bitwise gfp_t;

/* zone index */
#define __GFP_DMA			BIT(0)
#define __GFP_NORMAL		BIT(1)

/* page alloc feature */
#define __GFP_ZERO			BIT(2)
#define __GFP_NOWARN		BIT(3)

#define __GFP_KERNEL		BIT(4)

#define __GFP_BITS_SHIFT	5
#define __GFP_BITS_MASK 	((gfp_t)((1 << __GFP_BITS_SHIFT) - 1))

#define GFP_DMA			    ((gfp_t)__GFP_DMA)
#define GFP_NORMAL		    ((gfp_t)__GFP_NORMAL)

#define GFP_ZERO			((gfp_t)__GFP_ZERO)
#define GFP_NOWARN		    ((gfp_t)__GFP_NOWARN)

#define GFP_KERNEL		    ((gfp_t)__GFP_KERNEL)

static inline enum zone_type gfp_zone(gfp_t flags)
{
    if (unlikely(GFP_DMA & flags))
        return ZONE_DMA;

    return ZONE_NORMAL;
}

extern void __free_pages(struct page *page, unsigned int order);
extern void free_pages(unsigned long addr, unsigned int order);

#define __free_page(page) __free_pages((page), 0)
#define free_page(addr) free_pages((addr), 0)


extern struct page *__alloc_pages(gfp_t gfp_mask, unsigned int order);

#define alloc_pages(gfp_mask, order)	__alloc_pages(gfp_mask, order)
#define alloc_page(gfp_mask) alloc_pages(gfp_mask, 0)

extern unsigned long __get_free_pages(gfp_t gfp_mask, unsigned int order);

#define __get_free_page(gfp_mask) \
        __get_free_pages((gfp_mask), 0)

extern void free_compound_page(struct page *page);
extern void free_unref_page(struct page *page);

static inline void __put_page(struct page *page)
{
    if (unlikely(page_is_compound(page)))
        free_compound_page(page);
    else
        free_unref_page(page);
}

static inline void put_page(struct page *page)
{
    page = compound_head(page);

    if (put_page_testzero(page))
        __put_page(page);
}

static inline void get_page(struct page *page)
{
    page = compound_head(page);
    /*
     * Getting a normal page or the head of a compound page
     * requires to already have an elevated page->refcount.
     */
    BUG_ON(page_ref_count(page) <= 0);
    page_ref_inc(page);
}

#endif /* !SEMINIX_GFP_H */
