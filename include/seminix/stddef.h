/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_STDDEF_H
#define SEMINIX_STDDEF_H

#ifndef __ASSEMBLY__

#ifndef SEMINIX_TYPES_H
#error "Please don't include <seminix/stddef.h> directly, use <seminix/types.h> instead."
#endif

#ifndef __always_inline
#define __always_inline inline
#endif

#undef NULL
#define NULL ((void *)0)

enum {
    false	= 0,
    true	= 1
};

#undef offsetof
#define offsetof(TYPE, MEMBER)	__builtin_offsetof(TYPE, MEMBER)

/**
 * sizeof_field(TYPE, MEMBER)
 *
 * @TYPE: The structure containing the field of interest
 * @MEMBER: The field to return the size of
 */
#define sizeof_field(TYPE, MEMBER) sizeof((((TYPE *)0)->MEMBER))

/**
 * offsetofend(TYPE, MEMBER)
 *
 * @TYPE: The type of the structure
 * @MEMBER: The member within the structure to get the end offset of
 */
#define offsetofend(TYPE, MEMBER) \
    (offsetof(TYPE, MEMBER)	+ sizeof_field(TYPE, MEMBER))

#endif /* !__ASSEMBLY__ */
#endif /* !SEMINIX_STDDEF_H */
