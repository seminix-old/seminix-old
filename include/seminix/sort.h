/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_SORT_H
#define SEMINIX_SORT_H

#include <seminix/types.h>

void sort(void *base, int num, int size,
      int (*cmp)(const void *, const void *),
      void (*swap)(void *, void *, int));

#endif /* !SEMINIX_SORT_H */
