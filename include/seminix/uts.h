/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_UTS_H
#define SEMINIX_UTS_H

/*
 * Defines for what uname() should return
 */
#ifndef UTS_SYSNAME
#define UTS_SYSNAME "Seminix"
#endif

#ifndef UTS_NODENAME
#define UTS_NODENAME CONFIG_DEFAULT_HOSTNAME /* set by sethostname() */
#endif

#ifndef UTS_DOMAINNAME
#define UTS_DOMAINNAME "(none)"	/* set by setdomainname() */
#endif

#define __NEW_UTS_LEN   64

struct new_utsname {
    char sysname[__NEW_UTS_LEN + 1];
    char nodename[__NEW_UTS_LEN + 1];
    char release[__NEW_UTS_LEN + 1];
    char version[__NEW_UTS_LEN + 1];
    char machine[__NEW_UTS_LEN + 1];
    char domainname[__NEW_UTS_LEN + 1];
};

extern const struct new_utsname utsname;
extern const char seminix_banner[];

#endif /* !SEMINIX_UTS_H */
