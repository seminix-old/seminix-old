/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_PFN_H
#define SEMINIX_PFN_H

#ifndef __ASSEMBLY__

#include <seminix/types.h>
#include <seminix/pagesize.h>

#define PFN_ALIGN(x)	(((unsigned long)(x) + (PAGE_SIZE - 1)) & PAGE_MASK)
#define PFN_UP(x)	(((x) + PAGE_SIZE-1) >> PAGE_SHIFT)
#define PFN_DOWN(x)	((x) >> PAGE_SHIFT)
#define PFN_PHYS(x)	((phys_addr_t)(x) << PAGE_SHIFT)
#define PHYS_PFN(x)	((unsigned long)((x) >> PAGE_SHIFT))

/*
 * Convert a physical address to a Page Frame Number and back
 */
#define	__phys_to_pfn(paddr)	PHYS_PFN(paddr)
#define	__pfn_to_phys(pfn)	PFN_PHYS(pfn)

#endif /* !__ASSEMBLY__ */
#endif /* !SEMINIX_PFN_H */
