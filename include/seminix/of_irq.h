/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_OF_IRQ_H
#define SEMINIX_OF_IRQ_H

#include <seminix/errno.h>
#include <seminix/types.h>
#include <seminix/ioport.h>
#include <seminix/of.h>
#include <seminix/irq.h>
#include <seminix/irqdomain.h>

typedef int (*of_irq_init_cb_t)(struct device_node *, struct device_node *);

/*
 * Workarounds only applied to 32bit powermac machines
 */
#define OF_IMAP_OLDWORLD_MAC	0x00000001
#define OF_IMAP_NO_PHANDLE	0x00000002

#define of_irq_workarounds (0)
#define of_irq_dflt_pic (NULL)

extern int of_irq_parse_raw(const __be32 *addr, struct of_phandle_args *out_irq);
extern int of_irq_parse_one(struct device_node *device, int index,
              struct of_phandle_args *out_irq);
extern int irq_create_of_mapping(struct of_phandle_args *irq_data);
extern int of_irq_to_resource(struct device_node *dev, int index,
                  struct resource *r);

extern void of_irq_init(const struct of_device_id *matches);

#ifdef CONFIG_OF_IRQ
extern int of_irq_count(struct device_node *dev);
extern int of_irq_get(struct device_node *dev, int index);
extern int of_irq_get_byname(struct device_node *dev, const char *name);
extern int of_irq_to_resource_table(struct device_node *dev,
        struct resource *res, int nr_irqs);
extern struct device_node *of_irq_find_parent(struct device_node *child);
extern struct irq_domain *of_msi_get_domain(struct device_node *np,
                        enum irq_domain_bus_token token);
#else
static inline int of_irq_count(struct device_node *dev)
{
    return 0;
}
static inline int of_irq_get(struct device_node *dev, int index)
{
    return 0;
}
static inline int of_irq_get_byname(struct device_node *dev, const char *name)
{
    return 0;
}
static inline int of_irq_to_resource_table(struct device_node *dev,
                       struct resource *res, int nr_irqs)
{
    return 0;
}
static inline void *of_irq_find_parent(struct device_node *child)
{
    return NULL;
}

static inline struct irq_domain *of_msi_get_domain(struct device *dev,
                           struct device_node *np,
                           enum irq_domain_bus_token token)
{
    return NULL;
}
#endif

#if defined(CONFIG_OF_IRQ)
/*
 * irq_of_parse_and_map() is used by all OF enabled platforms; but SPARC
 * implements it differently.  However, the prototype is the same for all,
 * so declare it here regardless of the CONFIG_OF_IRQ setting.
 */
extern unsigned int irq_of_parse_and_map(struct device_node *node, int index);

#else /* !CONFIG_OF */
static inline unsigned int irq_of_parse_and_map(struct device_node *dev,
                        int index)
{
    return 0;
}
#endif /* !CONFIG_OF */
#endif /* !SEMINIX_OF_IRQ_H */
