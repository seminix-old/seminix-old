/* SPDX-License-Identifier: GPL-2.0 */
/*
 * irq_domain - IRQ translation domains
 *
 * Translation infrastructure between hw and linux irq numbers.  This is
 * helpful for interrupt controllers to implement mapping between hardware
 * irq numbers and the Linux irq number space.
 *
 * irq_domains also have hooks for translating device tree or other
 * firmware interrupt representations into a hardware irq number that
 * can be mapped back to a Linux irq number without any extra platform
 * support code.
 *
 * Interrupt controller "domain" data structure. This could be defined as a
 * irq domain controller. That is, it handles the mapping between hardware
 * and virtual interrupt numbers for a given interrupt domain. The domain
 * structure is generally created by the PIC code for a given PIC instance
 * (though a domain can cover more than one PIC if they have a flat number
 * model). It's the domain callbacks that are responsible for setting the
 * irq_chip on a given irq_desc after it's been mapped.
 *
 * The host code and data structures use a fwnode_handle pointer to
 * identify the domain. In some cases, and in order to preserve source
 * code compatibility, this fwnode pointer is "upgraded" to a DT
 * device_node. For those firmware infrastructures that do not provide
 * a unique identifier for an interrupt controller, the irq_domain
 * code offers a fwnode allocator.
 */

#ifndef SEMINIX_IRQDOMAIN_H
#define SEMINIX_IRQDOMAIN_H

#include <seminix/types.h>
#include <seminix/list.h>
#include <seminix/of.h>
#include <seminix/irq.h>
#include <seminix/radix-tree.h>
#include <seminix/mutex.h>

struct irq_domain;

/*
 * This type is the placeholder for a hardware interrupt number. It has to be
 * big enough to enclose whatever representation is used by a given platform.
 */
typedef unsigned long irq_hw_number_t;

/* Number of irqs reserved for a legacy isa controller */
#define NUM_ISA_INTERRUPTS	16

#define IRQ_DOMAIN_IRQ_SPEC_PARAMS 16

/**
 * struct irq_fwspec - generic IRQ specifier structure
 *
 * @fwnode:		Pointer to a firmware-specific descriptor
 * @param_count:	Number of device-specific parameters
 * @param:		Device-specific parameters
 *
 * This structure, directly modeled after of_phandle_args, is used to
 * pass a device-specific description of an interrupt.
 */
struct irq_fwspec {
    struct fwnode_handle *fwnode;
    int param_count;
    u32 param[IRQ_DOMAIN_IRQ_SPEC_PARAMS];
};

/*
 * Should several domains have the same device node, but serve
 * different purposes (for example one domain is for PCI/MSI, and the
 * other for wired IRQs), they can be distinguished using a
 * bus-specific token. Most domains are expected to only carry
 * DOMAIN_BUS_ANY.
 */
enum irq_domain_bus_token {
    DOMAIN_BUS_ANY      = 0,
    DOMAIN_BUS_WIRED,
    DOMAIN_BUS_GENERIC_MSI,
    DOMAIN_BUS_PCI_MSI,
    DOMAIN_BUS_PLATFORM_MSI,
    DOMAIN_BUS_NEXUS,
    DOMAIN_BUS_IPI,
    DOMAIN_BUS_FSL_MC_MSI,
};

/**
 * struct irq_domain_ops - Methods for irq_domain objects
 * @match: Match an interrupt controller device node to a host, returns
 *         1 on a match
 *
 * Functions below are provided by the driver and called whenever a new mapping
 * is created or an old mapping is disposed. The driver can then proceed to
 * whatever internal data structures management is required. It also needs
 * to setup the irq_desc when returning from map().
 */
struct irq_domain_ops {
    int (*select)(struct irq_domain *d, struct irq_fwspec *fwspec,
              enum irq_domain_bus_token bus_token);
    int (*match)(struct irq_domain *d, struct device_node *node,
             enum irq_domain_bus_token bus_token);
    int (*translate)(struct irq_domain *d, struct irq_fwspec *fwspec,
             irq_hw_number_t *out_hwirq, unsigned int *out_type);
    int (*alloc)(struct irq_domain *d, int virq, int nr_irqs, void *arg);
    void (*free)(struct irq_domain *d, int virq, int nr_irqs);
};

/**
 * struct irq_domain - Hardware interrupt number translation object
 */
struct irq_domain {
    struct list_head link;
    const char *name;
    const struct irq_domain_ops *ops;
    void *host_data;
    unsigned int flags;
    unsigned int mapcount;

    /* Optional data */
    struct fwnode_handle *fwnode;
    enum irq_domain_bus_token bus_token;
    struct irq_domain *parent;

    /* reverse map data. The linear map gets appended to the irq_domain */
    struct radix_tree_root revmap_tree;
    struct mutex revmap_tree_mutex;
};

/* Irq domain flags */
enum {
    /* Irq domain name was allocated in __irq_domain_add() */
    IRQ_DOMAIN_NAME_ALLOCATED	= (1 << 6),

    /* Irq domain is an IPI domain with virq per cpu */
    IRQ_DOMAIN_FLAG_IPI_PER_CPU	= (1 << 2),

    /* Irq domain is an IPI domain with single virq */
    IRQ_DOMAIN_FLAG_IPI_SINGLE	= (1 << 3),

    /* Irq domain implements MSIs */
    IRQ_DOMAIN_FLAG_MSI		= (1 << 4),

    /* Irq domain implements MSI remapping */
    IRQ_DOMAIN_FLAG_MSI_REMAP	= (1 << 5),

    /*
     * Flags starting from IRQ_DOMAIN_FLAG_NONCORE are reserved
     * for implementation specific purposes and ignored by the
     * core code.
     */
    IRQ_DOMAIN_FLAG_NONCORE		= (1 << 16),
};

extern const struct fwnode_operations irqchip_fwnode_ops;

static inline bool is_fwnode_irqchip(struct fwnode_handle *fwnode)
{
    return fwnode && fwnode->ops == &irqchip_fwnode_ops;
}

static inline struct device_node *irq_domain_get_of_node(struct irq_domain *d)
{
    return to_of_node(d->fwnode);
}

static inline struct fwnode_handle *of_node_to_fwnode(struct device_node *node)
{
    return node ? &node->fwnode : NULL;
}

void irq_set_default_host(struct irq_domain *host);

enum {
    IRQCHIP_FWNODE_REAL,
    IRQCHIP_FWNODE_NAMED,
    IRQCHIP_FWNODE_NAMED_ID,
};

struct fwnode_handle *__irq_domain_alloc_fwnode(unsigned int type, int id,
    const char *name, void *data);

static inline
struct fwnode_handle *irq_domain_alloc_named_fwnode(const char *name)
{
    return __irq_domain_alloc_fwnode(IRQCHIP_FWNODE_NAMED, 0, name, NULL);
}

static inline
struct fwnode_handle *irq_domain_alloc_named_id_fwnode(const char *name, int id)
{
    return __irq_domain_alloc_fwnode(IRQCHIP_FWNODE_NAMED_ID, id, name,
                     NULL);
}

static inline struct fwnode_handle *irq_domain_alloc_fwnode(void *data)
{
    return __irq_domain_alloc_fwnode(IRQCHIP_FWNODE_REAL, 0, NULL, data);
}

void irq_domain_free_fwnode(struct fwnode_handle *fwnode);

struct irq_domain *__irq_domain_add(struct fwnode_handle *fwnode,
    const struct irq_domain_ops *ops, void *host_data);

static inline struct irq_domain *irq_domain_create_tree(struct fwnode_handle *fwnode,
                     const struct irq_domain_ops *ops, void *host_data)
{
    return __irq_domain_add(fwnode, ops, host_data);
}

void irq_domain_remove(struct irq_domain *domain);

void irq_domain_update_bus_token(struct irq_domain *domain,
    enum irq_domain_bus_token bus_token);

struct irq_data *irq_domain_get_irq_data(struct irq_domain *domain,
    int virq);

struct irq_domain *irq_find_matching_fwspec(struct irq_fwspec *fwspec,
    enum irq_domain_bus_token bus_token);

static inline
struct irq_domain *irq_find_matching_fwnode(struct fwnode_handle *fwnode,
                        enum irq_domain_bus_token bus_token)
{
    struct irq_fwspec fwspec = {
        .fwnode = fwnode,
    };

    return irq_find_matching_fwspec(&fwspec, bus_token);
}

static inline struct irq_domain *irq_find_matching_host(struct device_node *node,
                            enum irq_domain_bus_token bus_token)
{
    return irq_find_matching_fwnode(of_node_to_fwnode(node), bus_token);
}

static inline struct irq_domain *irq_find_host(struct device_node *node)
{
    struct irq_domain *d;

    d = irq_find_matching_host(node, DOMAIN_BUS_WIRED);
    if (!d)
        d = irq_find_matching_host(node, DOMAIN_BUS_ANY);

    return d;
}

bool irq_domain_check_msi_remap(void);

bool irq_domain_hierarchical_is_msi_remap(struct irq_domain *domain);

int irq_find_mapping(struct irq_domain *host, irq_hw_number_t hwirq);

int irq_create_fwspec_mapping(struct irq_fwspec *fwspec);

struct irq_domain *irq_domain_create_hierarchy(struct irq_domain *parent,
                        u32 flags, struct fwnode_handle *fwnode,
                        const struct irq_domain_ops *ops,
                        void *host_data);

static inline struct irq_domain *irq_domain_add_hierarchy(struct irq_domain *parent,
                        unsigned int flags, struct device_node *node,
                        const struct irq_domain_ops *ops,
                        void *host_data)
{
    return irq_domain_create_hierarchy(parent, flags, of_node_to_fwnode(node),
                       ops, host_data);
}

int irq_domain_alloc_irqs_hierarchy(struct irq_domain *domain,
                    int irq_base, int nr_irqs, void *arg);
void irq_domain_free_irqs_hierarchy(struct irq_domain *domain,
                       int irq_base, int nr_irqs);

int __irq_domain_alloc_irqs(struct irq_domain *domain, int irq_base,
                   int nr_irqs, void *arg,
                   const struct cpumask *affinity);

static inline int irq_domain_alloc_irqs(struct irq_domain *domain,
            unsigned int nr_irqs, void *arg)
{
    return __irq_domain_alloc_irqs(domain, -1, nr_irqs, arg, NULL);
}

void irq_domain_free_irqs(int virq, int nr_irqs);

static inline bool irq_domain_is_msi(struct irq_domain *domain)
{
    return domain->flags & IRQ_DOMAIN_FLAG_MSI;
}

static inline bool irq_domain_is_msi_remap(struct irq_domain *domain)
{
    return domain->flags & IRQ_DOMAIN_FLAG_MSI_REMAP;
}

int irq_domain_set_hwirq_and_chip(struct irq_domain *domain, int virq,
                  irq_hw_number_t hwirq, struct irq_chip *chip,
                  void *chip_data);

void irq_domain_set_info(struct irq_domain *domain, int virq,
             irq_hw_number_t hwirq, struct irq_chip *chip,
             void *chip_data, irq_flow_handler_t handler,
             void *handler_data, const char *handler_name);

void irq_domain_reset_irq_data(struct irq_data *irq_data);

#endif /* !SEMINIX_IRQDOMAIN_H */
