#ifndef SEMINIX_IRQ_MANAGE_H
#define SEMINIX_IRQ_MANAGE_H

#include <seminix/irq.h>

bool synchronize_hardirq(int irq);
void synchronize_irq(int irq);

int irq_can_set_affinity(int irq);

int irq_do_set_affinity(struct irq_data *data, const struct cpumask *mask,
    bool force);
int __irq_set_affinity(int irq, const struct cpumask *mask, bool force);

static inline int
irq_set_affinity(int irq, const struct cpumask *cpumask)
{
    return __irq_set_affinity(irq, cpumask, false);
}

static inline int
irq_force_affinity(int irq, const struct cpumask *cpumask)
{
    return __irq_set_affinity(irq, cpumask, true);
}

int can_request_irq(int irq, unsigned long irqflags);

int __irq_set_trigger(struct irq_desc *desc, unsigned long flags);

int setup_irq(int irq, struct irqaction *act);
void remove_irq(int irq, struct irqaction *act);
const void *free_irq(int irq, void *dev_id);

int __request_irq(int irq, irq_handler_t handler,
    unsigned long irqflags, const char *devname, void *dev_id);

static inline int
request_irq(int irq, irq_handler_t handler, unsigned long flags,
        const char *name, void *dev)
{
    return __request_irq(irq, handler, flags, name, dev);
}

void enable_percpu_irq(int irq, unsigned int type);
bool irq_percpu_is_enabled(int irq);
void disable_percpu_irq(int irq);

void remove_percpu_irq(int irq, struct irqaction *act);

int setup_percpu_irq(int irq, struct irqaction *act);

int __request_percpu_irq(int irq, irq_handler_t handler,
             unsigned long flags, const char *devname,
             void __percpu *dev_id);

static inline int
request_percpu_irq(int irq, irq_handler_t handler,
           const char *devname, void __percpu *percpu_dev_id)
{
    return __request_percpu_irq(irq, handler, 0,
                    devname, percpu_dev_id);
}

#endif /* !SEMINIX_IRQ_MANAGE_H */
