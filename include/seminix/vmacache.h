#ifndef SEMINIX_VMACACHE_H
#define SEMINIX_VMACACHE_H

#include <seminix/mm.h>
#include <seminix/sched.h>

static inline void vmacache_flush(struct task_struct *tsk)
{
    memset(tsk->vmacache.vmas, 0, sizeof(tsk->vmacache.vmas));
}

extern void vmacache_update(unsigned long addr, struct vm_area_struct *newvma);
extern struct vm_area_struct *vmacache_find(struct mm_struct *mm,
                            unsigned long addr);

static inline void vmacache_invalidate(struct mm_struct *mm)
{
    mm->vmacache_seqnum++;
}

#endif /* !SEMINIX_VMACACHE_H */
