/* SPDX-License-Identifier: GPL-2.0 */
#ifndef LIBFDT_ENV_H
#define LIBFDT_ENV_H

#include <seminix/kernel.h>	/* For INT_MAX */
#include <seminix/string.h>

#include <asm/byteorder.h>

#define INT32_MAX	S32_MAX
#define UINT32_MAX	U32_MAX

typedef __be16 fdt16_t;
typedef __be32 fdt32_t;
typedef __be64 fdt64_t;

#define fdt32_to_cpu(x) be32_to_cpu(x)
#define cpu_to_fdt32(x) cpu_to_be32(x)
#define fdt64_to_cpu(x) be64_to_cpu(x)
#define cpu_to_fdt64(x) cpu_to_be64(x)

#ifdef HAVE_LIBFDT_ENV_TYPE

typedef u8			uint8_t;
typedef u16			uint16_t;
typedef u32			uint32_t;
typedef i32			int32_t;

#if defined(__GNUC__)
typedef u64			uint64_t;
typedef u64			u_int64_t;
typedef i64			int64_t;
#endif

typedef usize size_t;

#endif /* HAVE_LIBFDT_ENV_TYPE */
#endif /* !LIBFDT_ENV_H */
