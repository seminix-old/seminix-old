/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_PREEMPT_H
#define SEMINIX_PREEMPT_H

/*
 * include/linux/preempt.h - macros for accessing and manipulating
 * preempt_count (used for kernel preemption, interrupt count, etc.)
 */
#include <seminix/bits.h>
#include <seminix/linkage.h>
#include <seminix/irqflags.h>
#include <seminix/thread_info.h>

/*
 * - bits 0-7  抢占计数(最大深度 256)
 * - bits 8-11 irq 计数(理论最大为 1)
 * - bit  12   nmi 计数(最大为 1)
 *
 *         PREEMPT_MASK:	0x000000ff
 *         HARDIRQ_MASK:	0x00000f00
 *             NMI_MASK:	0x00001000
 * PREEMPT_NEED_RESCHED:	0x80000000
 */
#define PREEMPT_BITS	8
#define HARDIRQ_BITS	4
#define NMI_BITS	    1

#define PREEMPT_SHIFT	0
#define HARDIRQ_SHIFT	(PREEMPT_SHIFT + PREEMPT_BITS)
#define NMI_SHIFT	(HARDIRQ_SHIFT + HARDIRQ_BITS)

#define __PREEMPT_MASK(x)	((1UL << (x))-1)

#define PREEMPT_MASK	(__PREEMPT_MASK(PREEMPT_BITS) << PREEMPT_SHIFT)
#define HARDIRQ_MASK	(__PREEMPT_MASK(HARDIRQ_BITS) << HARDIRQ_SHIFT)
#define NMI_MASK	    (__PREEMPT_MASK(NMI_BITS)     << NMI_SHIFT)

#define PREEMPT_OFFSET	(1UL << PREEMPT_SHIFT)
#define HARDIRQ_OFFSET	(1UL << HARDIRQ_SHIFT)
#define NMI_OFFSET	    (1UL << NMI_SHIFT)

/*
 * The preempt_count offset after preempt_disable();
 */
#define PREEMPT_DISABLE_OFFSET	PREEMPT_OFFSET

/*
 * The preempt_count offset after spin_lock()
 */
#define PREEMPT_LOCK_OFFSET	PREEMPT_DISABLE_OFFSET

#define PREEMPT_ENABLED     (0)

#define PREEMPT_DISABLED	(PREEMPT_DISABLE_OFFSET + PREEMPT_ENABLED)

#define INIT_PREEMPT_COUNT	PREEMPT_OFFSET

#define FORK_PREEMPT_COUNT	(2 * PREEMPT_DISABLE_OFFSET + PREEMPT_ENABLED)

static __always_inline int preempt_count(void)
{
    return READ_ONCE(current_thread_info()->preempt_count);
}

static __always_inline volatile int *preempt_count_ptr(void)
{
    return &current_thread_info()->preempt_count;
}

static __always_inline void preempt_count_set(int pc)
{
    *preempt_count_ptr() = pc;
}

/*
 * must be macros to avoid header recursion hell
 */
#define init_task_preempt_count(p) do { \
	task_thread_info(p)->preempt_count = FORK_PREEMPT_COUNT; \
} while (0)

#define init_idle_preempt_count(p, cpu) do { \
	task_thread_info(p)->preempt_count = PREEMPT_ENABLED; \
} while (0)

static __always_inline void set_preempt_need_resched(void)
{
}

static __always_inline void clear_preempt_need_resched(void)
{
}

/*
 * The various preempt_count add/sub methods
 */

static __always_inline void preempt_count_add(int val)
{
    *preempt_count_ptr() += val;
}

static __always_inline void preempt_count_sub(int val)
{
    *preempt_count_ptr() -= val;
}

/* 减 1 抢占计数, 测试结果是否为 0, 如果为 0 并且需要抢占则返回 true */
static __always_inline bool preempt_count_dec_and_test(void)
{
    /*
     * Because of load-store architectures cannot do per-cpu atomic
     * operations; we cannot use PREEMPT_NEED_RESCHED because it might get
     * lost.
     */
    return !--*preempt_count_ptr() && tif_need_resched();
}

/*
 * Returns true when we need to resched and can (barring IRQ state).
 */
static __always_inline bool should_resched(int preempt_offset)
{
    return unlikely(preempt_count() == preempt_offset &&
            tif_need_resched());
}

extern asmlinkage void preempt_schedule(void);
#define __preempt_schedule() preempt_schedule()

#define hardirq_count()	(preempt_count() & HARDIRQ_MASK)
#define irq_count()	(preempt_count() & (HARDIRQ_MASK | NMI_MASK))

/*
 * Are we doing bottom half or hardware interrupt processing?
 *
 * in_irq()       - We're in (hard) IRQ context
 * in_interrupt() - We're in NMI,IRQ,SoftIRQ context or have BH disabled
 * in_nmi()       - We're in NMI context
 * in_task()	  - We're in task context
 *
 * Note: due to the BH disabled confusion: in_interrupt() really
 *       should not be used in new code.
 */
#define in_irq()		(hardirq_count())
#define in_interrupt()	(irq_count())
#define in_nmi()		(preempt_count() & NMI_MASK)
#define in_task()		(!(preempt_count() & (NMI_MASK | HARDIRQ_MASK)))

/*
 * Are we running in atomic context?  WARNING: this macro cannot
 * always detect atomic context; in particular, it cannot know about
 * held spinlocks in non-preemptible kernels.  Thus it should not be
 * used in the general case to determine whether sleeping is possible.
 * Do not use in_atomic() in driver code.
 */
#define in_atomic()	(preempt_count() != 0)

/*
 * Check whether we were atomic before we did preempt_disable():
 * (used by the scheduler)
 */
#define in_atomic_preempt_off() (preempt_count() != PREEMPT_DISABLE_OFFSET)

#define preempt_count_inc() preempt_count_add(1)
#define preempt_count_dec() preempt_count_sub(1)

#define preempt_disable() \
do { \
    preempt_count_inc(); \
    barrier(); \
} while (0)

#define sched_preempt_enable_no_resched() \
do { \
    barrier(); \
    preempt_count_dec(); \
} while (0)

#define preempt_enable_no_resched() sched_preempt_enable_no_resched()

#define preemptible()	(preempt_count() == 0 && !irqs_disabled())

#define preempt_enable() \
do { \
    barrier(); \
    if (unlikely(preempt_count_dec_and_test())) \
        __preempt_schedule(); \
} while (0)

#define preempt_check_resched() \
do { \
    if (should_resched(0)) \
        __preempt_schedule(); \
} while (0)

#define preempt_set_need_resched() \
do { \
    set_preempt_need_resched(); \
} while (0)
#define preempt_fold_need_resched() \
do { \
    if (tif_need_resched()) \
        set_preempt_need_resched(); \
} while (0)

#endif /* !SEMINIX_PREEMPT_H */
