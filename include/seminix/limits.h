/* SPDX-License-Identifier: GPL-2.0 */
#ifndef SEMINIX_LIMITS_H
#define SEMINIX_LIMITS_H

#include <seminix/types.h>

#define PHYS_ADDR_MAX	(~(phys_addr_t)0)

#define U8_MAX		((u8)~0U)
#define S8_MAX		((i8)(U8_MAX >> 1))
#define S8_MIN		((i8)(-S8_MAX - 1))
#define U16_MAX		((u16)~0U)
#define S16_MAX		((i16)(U16_MAX >> 1))
#define S16_MIN		((i16)(-S16_MAX - 1))
#define U32_MAX		((u32)~0U)
#define U32_MIN		((u32)0)
#define S32_MAX		((i32)(U32_MAX >> 1))
#define S32_MIN		((i32)(-S32_MAX - 1))
#define U64_MAX		((u64)~0ULL)
#define S64_MAX		((i64)(U64_MAX >> 1))
#define S64_MIN		((i64)(-S64_MAX - 1))

#endif /* !SEMINIX_LIMITS_H */
