#ifndef SEMINIX_IRQ_H
#define SEMINIX_IRQ_H

#include <seminix/types.h>
#include <seminix/init.h>
#include <seminix/cache.h>
#include <seminix/cpumask.h>
#include <seminix/interrupt.h>
#include <seminix/irq/msi.h>
#include <asm/ptrace.h>

struct irq_desc;
struct irq_data;

/*
 * irq_get_irqchip_state/irq_set_irqchip_state specific flags
 */
enum irqchip_irq_state {
    IRQCHIP_STATE_PENDING,		/* Is interrupt pending? */
    IRQCHIP_STATE_ACTIVE,		/* Is interrupt in progress? */
    IRQCHIP_STATE_MASKED,		/* Is interrupt masked? */
    IRQCHIP_STATE_LINE_LEVEL,	/* Is IRQ line high? */
};

extern int irq_get_irqchip_state(int irq, enum irqchip_irq_state which,
              bool *state);
extern int irq_set_irqchip_state(int irq, enum irqchip_irq_state which,
              bool val);

typedef	void (*irq_flow_handler_t)(struct irq_desc *desc);

/**
 * struct irq_chip - hardware interrupt chip descriptor
 *
 * @parent_chip:	pointer to parent device for irqchip
 * @name:		name for /proc/interrupts
 * @irq_startup:	start up the interrupt (defaults to ->enable if NULL)
 * @irq_shutdown:	shut down the interrupt (defaults to ->disable if NULL)
 * @irq_ack:		start of a new interrupt
 * @irq_mask:		mask an interrupt source
 * @irq_mask_ack:	ack and mask an interrupt source
 * @irq_unmask:		unmask an interrupt source
 * @irq_eoi:		end of interrupt
 * @irq_set_affinity:	Set the CPU affinity on SMP machines. If the force
 *			argument is true, it tells the driver to
 *			unconditionally apply the affinity setting. Sanity
 *			checks against the supplied affinity mask are not
 *			required. This is used for CPU hotplug where the
 *			target CPU is not yet set in the cpu_online_mask.
 * @irq_set_type:	set the flow type (IRQ_TYPE_LEVEL/etc.) of an IRQ
 * @irq_bus_lock:	function to lock access to slow bus (i2c) chips
 * @irq_bus_sync_unlock:function to sync and unlock slow bus (i2c) chips
 * @irq_compose_msi_msg:	optional to compose message content for MSI
 * @irq_write_msi_msg:	optional to write message content for MSI
 * @irq_get_irqchip_state:	return the internal state of an interrupt
 * @irq_set_irqchip_state:	set the internal state of a interrupt
 * @ipi_send_single:	send a single IPI to destination cpus
 * @ipi_send_mask:	send an IPI to destination cpus in cpumask
 * @flags:		chip specific flags
 */
struct irq_chip {
    struct irq_chip    *parent_chip;
    const char	*name;
    int	(*irq_startup)(struct irq_data *data);
    void		(*irq_shutdown)(struct irq_data *data);

    void		(*irq_ack)(struct irq_data *data);
    void		(*irq_mask)(struct irq_data *data);
    void		(*irq_mask_ack)(struct irq_data *data);
    void		(*irq_unmask)(struct irq_data *data);
    void		(*irq_eoi)(struct irq_data *data);

    int		(*irq_set_affinity)(struct irq_data *data, const struct cpumask *dest, bool force);

    int		(*irq_set_type)(struct irq_data *data, unsigned int flow_type);

    void		(*irq_bus_lock)(struct irq_data *data);
    void		(*irq_bus_sync_unlock)(struct irq_data *data);

    void		(*irq_compose_msi_msg)(struct irq_data *data, struct msi_msg *msg);
    void		(*irq_write_msi_msg)(struct irq_data *data, struct msi_msg *msg);

    int		(*irq_get_irqchip_state)(struct irq_data *data, enum irqchip_irq_state which, bool *state);
    int		(*irq_set_irqchip_state)(struct irq_data *data, enum irqchip_irq_state which, bool state);

    void		(*ipi_send_single)(struct irq_data *data, unsigned int cpu);
    void		(*ipi_send_mask)(struct irq_data *data, const struct cpumask *dest);

    unsigned long	flags;
};

/*
 * irq_chip specific flags
 *
 * IRQCHIP_SET_TYPE_MASKED:	Mask before calling chip.irq_set_type()
 * IRQCHIP_EOI_IF_HANDLED:	Only issue irq_eoi() when irq was handled
 * IRQCHIP_MASK_ON_SUSPEND:	Mask non wake irqs in the suspend path
 * IRQCHIP_ONOFFLINE_ENABLED:	Only call irq_on/off_line callbacks
 *				when irq enabled
 * IRQCHIP_SKIP_SET_WAKE:	Skip chip.irq_set_wake(), for this irq chip
 * IRQCHIP_ONESHOT_SAFE:	One shot does not require mask/unmask
 * IRQCHIP_EOI_THREADED:	Chip requires eoi() on unmask in threaded mode
 * IRQCHIP_SUPPORTS_LEVEL_MSI	Chip can provide two doorbells for Level MSIs
 */
enum {
    IRQCHIP_SET_TYPE_MASKED		= (1 <<  0),
    IRQCHIP_EOI_IF_HANDLED		= (1 <<  1),
    IRQCHIP_MASK_ON_SUSPEND		= (1 <<  2),
    IRQCHIP_ONOFFLINE_ENABLED	= (1 <<  3),
    IRQCHIP_SKIP_SET_WAKE		= (1 <<  4),
    IRQCHIP_ONESHOT_SAFE		= (1 <<  5),
    IRQCHIP_SUPPORTS_LEVEL_MSI	= (1 <<  6),
};

struct irq_data {
    int         hwirq;
    int         irq;

    struct msi_desc     *msi_desc;
    struct irq_desc     *irq_desc;

    cpumask_t            affinity;

    struct irq_chip     *chip;
    void                *chip_data;

    struct irq_domain   *domain;
    struct irq_data     *parent_data;

    void			*handler_data;
};

/*
 * Return value for chip->irq_set_affinity()
 *
 * IRQ_SET_MASK_OK	- OK, core updates irq_common_data.affinity
 * IRQ_SET_MASK_NOCPY	- OK, chip did update irq_common_data.affinity
 * IRQ_SET_MASK_OK_DONE	- Same as IRQ_SET_MASK_OK for core. Special code to
 *			  support stacked irqchips, which indicates skipping
 *			  all descendent irqchips.
 */
enum {
    IRQ_SET_MASK_OK = 0,
    IRQ_SET_MASK_OK_DONE,
};

extern struct irq_data *irq_get_irq_data(int irq);

static inline void irq_data_update_effective_affinity(struct irq_data *d,
                              const struct cpumask *m)
{
    cpumask_copy(&d->affinity, m);
}

extern void init_IRQ(void);

#endif /* !SEMINIX_IRQ_H */
