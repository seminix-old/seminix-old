#ifndef SEMINIX_PAGE_TYPES_H
#define SEMINIX_PAGE_TYPES_H

enum pageflags {
    PG_initialized = 0,
    PG_reserved,
    PG_buddy,
    PG_head,
    PG_slab,
    PG_arch_1,
    PG_table,
    PG_active,
    PG_locked,
    __NR_PAGEFLAGS,
};

#ifndef __GENERATING_BOUNDS_H
#include <generated/bounds.h>
#include <seminix/list.h>
#include <seminix/mmzone.h>

#ifdef CONFIG_HAVE_ALIGNED_STRUCT_PAGE
#define _struct_page_alignment	__aligned(2 * sizeof(unsigned long))
#else
#define _struct_page_alignment
#endif

struct kmem_cache;

struct page {
    unsigned long flags;

    union {
        /* buddy 系统复合页头部和单页描述 */
        struct {
            struct list_head lru;
            unsigned long order;
        };
        /* buddy 系统复合页尾部描述 */
        struct {
            unsigned long compound_head;
            unsigned char compound_order;
        };
        /* slab 系统使用 */
        struct {
            struct kmem_cache *slab;
            void *freelist;
            unsigned inuse;
        };
    };

    atomic_t mapcount;
    atomic_t refcount;
} _struct_page_alignment;

/*
 * Used for sizing the vmemmap region on some architectures
 */
#define STRUCT_PAGE_MAX_SHIFT	(order_base_2(sizeof(struct page)))

static __always_inline int page_is_initialized(struct page *page)
{
    return page->flags & BIT(PG_initialized);
}

static __always_inline void __set_page_flags_initialized(struct page *page)
{
    __set_bit(PG_initialized, &page->flags);
}

static __always_inline int page_is_head(struct page *page)
{
    return test_bit(PG_head, &page->flags);
}

static __always_inline int page_is_tail(struct page *page)
{
    return READ_ONCE(page->compound_head) & 1;
}

static __always_inline int page_is_compound(struct page *page)
{
    return page_is_head(page) || page_is_tail(page);
}

static __always_inline int page_is_single(struct page *page)
{
    return !page_is_compound(page);
}

/*
 * 返回该页的头部页
 *
 * 如果该页是复合页尾部, 则从 compound_head 获取复合页头部.
 * 否则直接返回 page, 因为它可能是复合页头部页也可能是单页.
 */
static inline struct page *compound_head(struct page *page)
{
    unsigned long head = READ_ONCE(page->compound_head);

    if (unlikely(head & 1))
        return (struct page *)(head - 1);

    return page;
}

/* 检查页是否初始化 */
#define PF_INITIALIZED_CHECK(page) ({   \
        BUG_ON(!page_is_initialized(page)); \
        page; })
/* 检查页是否是单页/复合页头部/复合页尾部 */
#define PF_ANY(page, enforce) PF_INITIALIZED_CHECK(page)
/* 任何复合页的检查都是针对头部页 */
#define PF_HEAD(page, enforce) PF_INITIALIZED_CHECK(compound_head(page))
/* 对于复合页, 该页只能是头部页 */
#define PF_ONLY_HEAD(page, enforce) ({  \
        BUG_ON(page_is_tail(page));     \
        PF_INITIALIZED_CHECK(page); })
/* 修改页 flags 必须在单页或者复合页头部上进行, 检查也可以在尾页进行 */
#define PF_NO_TAIL(page, enforce) ({    \
        BUG_ON(enforce && page_is_tail(page)); \
        PF_INITIALIZED_CHECK(compound_head(page)); })
/* 页不是一个复合页 */
#define PF_NO_COMPOUND(page, enforce) ({ \
        BUG_ON(enforce && page_is_compound(page)) ; \
        PF_INITIALIZED_CHECK(page); })

#define TESTPAGEFLAG(uname, lname, policy) \
static __always_inline int page_is_##uname(struct page *page) \
    { return test_bit(PG_##lname, &policy(page, 0)->flags); }

#define SETPAGEFLAG(uname, lname, policy) \
static __always_inline void set_page_flags_##uname(struct page *page) \
    { set_bit(PG_##lname, &policy(page, 1)->flags); }

#define CLEARPAGEFLAG(uname, lname, policy) \
static __always_inline void clear_page_flags_##uname(struct page *page) \
    { clear_bit(PG_##lname, &policy(page, 1)->flags); }

#define __SETPAGEFLAG(uname, lname, policy) \
static __always_inline void __set_page_flags_##uname(struct page *page) \
    { __set_bit(PG_##lname, &policy(page, 1)->flags); }

#define __CLEARPAGEFLAG(uname, lname, policy) \
static __always_inline void __clear_page_flags_##uname(struct page *page) \
    { __clear_bit(PG_##lname, &policy(page, 1)->flags); }

#define TESTSETFLAG(uname, lname, policy) \
static __always_inline int test_and_set_page_flags_##uname(struct page *page) \
    { return test_and_set_bit(PG_##lname, &policy(page, 1)->flags); }

#define TESTCLEARFLAG(uname, lname, policy) \
static __always_inline int test_and_clear_page_flags_##uname(struct page *page) \
    { return test_and_clear_bit(PG_##lname, &policy(page, 1)->flags); }

#define PAGEFLAG(uname, lname, policy)					\
    TESTPAGEFLAG(uname, lname, policy)				\
    SETPAGEFLAG(uname, lname, policy)				\
    CLEARPAGEFLAG(uname, lname, policy)

#define __PAGEFLAG(uname, lname, policy)				\
    TESTPAGEFLAG(uname, lname, policy)				\
    __SETPAGEFLAG(uname, lname, policy)				\
    __CLEARPAGEFLAG(uname, lname, policy)

#define TESTSCFLAG(uname, lname, policy)				\
    TESTSETFLAG(uname, lname, policy)				\
    TESTCLEARFLAG(uname, lname, policy)

PAGEFLAG(reserved, reserved, PF_NO_COMPOUND)
__CLEARPAGEFLAG(reserved, reserved, PF_NO_COMPOUND)
__SETPAGEFLAG(reserved, reserved, PF_NO_COMPOUND)

PAGEFLAG(buddy, buddy, PF_ANY)
TESTSCFLAG(buddy, buddy, PF_ANY)
__SETPAGEFLAG(buddy, buddy, PF_ANY)
__CLEARPAGEFLAG(buddy, buddy, PF_ANY)

__SETPAGEFLAG(head, head, PF_ANY)
__CLEARPAGEFLAG(head, head, PF_ANY)
CLEARPAGEFLAG(head, head, PF_ANY)

__PAGEFLAG(slab, slab, PF_NO_TAIL)

__PAGEFLAG(table, table, PF_NO_TAIL)

PAGEFLAG(active, active, PF_HEAD)
__CLEARPAGEFLAG(active, active, PF_HEAD)
TESTCLEARFLAG(active, active, PF_HEAD)

__PAGEFLAG(locked, locked, PF_NO_TAIL)

static __always_inline void set_compound_head(struct page *page, struct page *head)
{
    WRITE_ONCE(page->compound_head, (unsigned long)head + 1);
}

static __always_inline void clear_compound_head(struct page *page)
{
    WRITE_ONCE(page->compound_head, 0);
}

/* 当页被释放时 flags 被检查, 页不应该有这些 flags, 如果有, 则有问题 */
#define PAGE_FLAGS_CHECK_AT_FREE    \
    (BIT(PG_reserved) | BIT(PG_slab) | BIT(PG_active))

#define __PG_HWPOISON  BIT(PG_initialized)
/* 当页准备返回分配器时检查 flags, 正在准备释放的页面不应该包含这些 flags,
 * 如果有, 则 struct page 结构被破坏.
 */
#define PAGE_FLAGS_CHECK_AT_PREP    \
    (((1UL << NR_PAGEFLAGS) - 1) & ~__PG_HWPOISON)

#undef PF_ANY
#undef PF_HEAD
#undef PF_ONLY_HEAD
#undef PF_NO_TAIL
#undef PF_NO_COMPOUND

#define set_page_order(page, o) ((page)->order = (o))
#define page_order(page)            ((page)->order)

#ifndef mm_zero_struct_page
#define mm_zero_struct_page(pp) ((void)memset((pp), 0, sizeof(struct page)))
#endif

#define page_address(page) page_to_virt(page)

static inline void page_mapcount_reset(struct page *page)
{
    atomic_set(&page->mapcount, -1);
}

static inline int page_mapcount(struct page *page)
{
    return atomic_read(&page->mapcount) + 1;
}

/*
 * 页的引用计数, 确保页是复合页头部或者是单页
 */
static inline int page_ref_count(struct page *page)
{
    return atomic_read(&page->refcount);
}

/*
 * 复合页或者单页的引用计数
 */
static inline int page_count(struct page *page)
{
    return page_ref_count(compound_head(page));
}

static inline void set_page_count(struct page *page, int c)
{
    atomic_set(&page->refcount, c);
}

/*
 * 在首次释放进分配器之前, 设置页的引用计数(boot)
 */
static inline void init_page_count(struct page *page)
{
    set_page_count(page, 1);
}

static inline void page_ref_add(struct page *page, int nr)
{
    atomic_add(nr, &page->refcount);
}

static inline void page_ref_sub(struct page *page, int nr)
{
    atomic_sub(nr, &page->refcount);
}

static inline void page_ref_inc(struct page *page)
{
    atomic_inc(&page->refcount);
}

static inline void page_ref_dec(struct page *page)
{
    atomic_dec(&page->refcount);
}

static inline int page_ref_sub_and_test(struct page *page, int nr)
{
    return atomic_sub_and_test(nr, &page->refcount);
}

static inline int page_ref_inc_return(struct page *page)
{
    return atomic_inc_return(&page->refcount);
}

static inline int page_ref_dec_and_test(struct page *page)
{
    return atomic_dec_and_test(&page->refcount);
}

static inline int page_ref_dec_return(struct page *page)
{
    return atomic_dec_return(&page->refcount);
}

static inline int page_ref_add_unless(struct page *page, int nr, int u)
{
    return atomic_add_unless(&page->refcount, nr, u);
}

static inline int page_ref_freeze(struct page *page, int count)
{
    return likely(atomic_cmpxchg(&page->refcount, count, 0) == count);
}

static inline void page_ref_unfreeze(struct page *page, int count)
{
    BUG_ON(page_count(page) != 0);
    BUG_ON(count == 0);

    atomic_set_release(&page->refcount, count);
}

/*
 * 丢弃一个 ref, 如果 refcount 降为 0(页面没有用户), 则返回 true.
 */
static inline int put_page_testzero(struct page *page)
{
    BUG_ON(page_ref_count(page) == 0);
    return page_ref_dec_and_test(page);
}

/*
 * 尝试抓取一个 ref, 除非页面的 refcount 为 0, 如果是这种情况则返回 false.
 */
static inline int get_page_unless_zero(struct page *page)
{
    return page_ref_add_unless(page, 1, 0);
}

#if MAX_NR_ZONES < 2
#define ZONES_SHIFT 0
#elif MAX_NR_ZONES <= 2
#define ZONES_SHIFT 1
#elif MAX_NR_ZONES <= 4
#define ZONES_SHIFT 2
#elif MAX_NR_ZONES <= 8
#define ZONES_SHIFT 3
#else
#error ZONES_SHIFT -- too many zones configured adjust calculation
#endif

#define ZONES_WIDTH		ZONES_SHIFT

#if ZONES_WIDTH > BITS_PER_LONG - NR_PAGEFLAGS
#error "Page-types: No space for zones field in page flags"
#endif

#define NODES_PGOFF		((sizeof(unsigned long)*8) - ZONES_WIDTH)
#define ZONEID_MASK		((1UL << ZONES_SHIFT) - 1)

static inline void set_page_zone(struct page *page, enum zone_type zone)
{
    page->flags &= ~(ZONEID_MASK << NODES_PGOFF);
    page->flags |= (zone & ZONEID_MASK) << NODES_PGOFF;
}

static inline int page_zone_id(const struct page *page)
{
    return (page->flags >> NODES_PGOFF) & ZONEID_MASK;
}

static inline struct zone *page_zone(const struct page *page)
{
    return &PG_DATA()->zones[page_zone_id(page)];
}

static inline struct page *virt_to_head_page(const void *v)
{
    struct page *page = virt_to_page(v);
    return compound_head(page);
}

/*
 * 返回复合页或者单页的 order, 确保 page 不是复合页尾部.
 */
static inline unsigned int compound_order(struct page *page)
{
    if (!page_is_head(page))
        return 0;
    return page[1].compound_order;
}

static inline void set_compound_order(struct page *page, unsigned int order)
{
    page[1].compound_order = order;
}

#endif /* !__GENERATING_BOUNDS_H */
#endif /* !SEMINIX_PAGE_TYPES_H */
