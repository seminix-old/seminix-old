#ifndef SEMINIX_PERCPU_H
#define SEMINIX_PERCPU_H

#include <asm/percpu.h>

extern void setup_per_cpu_areas(void);

#endif /* !SEMINIX_PERCPU_H */
