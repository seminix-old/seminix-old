#ifndef SEMINIX_PAGESIZE_H
#define SEMINIX_PAGESIZE_H

#include <asm/page-def.h>

/* to align the pointer to the (next) page boundary */
#define PAGE_ALIGN(addr) ALIGN(addr, PAGE_SIZE)
#define PAGE_ALIGN_DOWN(addr) ALIGN_DOWN(addr, PAGE_SIZE)

/* test whether an address (unsigned long or pointer) is aligned to PAGE_SIZE */
#define PAGE_ALIGNED(addr)	IS_ALIGNED((unsigned long)(addr), PAGE_SIZE)

#define offset_in(p, a)     ((unsigned long)(p) & (a))
#define offset_in_page(p)   offset_in(p, ~PAGE_MASK)

#endif /* !SEMINIX_PAGESIZE_H */
