#ifndef SEMINIX_INIT_TASK_H
#define SEMINIX_INIT_TASK_H

#include <seminix/thread_info.h>

#define __init_task_data __attribute__((__section__(".data..init_task")))

#define INIT_TASK_COMM "swapper"

#endif /* SEMINIX_INIT_TASK_H */
