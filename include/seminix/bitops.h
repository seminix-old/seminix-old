/*
 * Bitops Module
 *
 * Copyright (C) 2010 Corentin Chary <corentin.chary@gmail.com>
 *
 * Mostly inspired by (stolen from) linux/bitmap.h and linux/bitops.h
 *
 * This work is licensed under the terms of the GNU LGPL, version 2.1 or later.
 * See the COPYING.LIB file in the top-level directory.
 */

#ifndef SEMINIX_BITOPS_H
#define SEMINIX_BITOPS_H

#include <seminix/string.h>
#include <seminix/types.h>
#include <seminix/bits.h>
#include <seminix/bug.h>

extern unsigned int __sw_hweight8(unsigned int w);
extern unsigned int __sw_hweight16(unsigned int w);
#ifndef __HAVE_ARCH_SW_HWEIGHT
extern unsigned int __sw_hweight32(unsigned int w);
extern unsigned long __sw_hweight64(__u64 w);
#endif

#include <asm/bitops.h>

#define for_each_set_bit(bit, addr, size) \
    for ((bit) = find_first_bit((addr), (size));		\
         (bit) < (size);					\
         (bit) = find_next_bit((addr), (size), (bit) + 1))

/* same as for_each_set_bit() but use bit as value to start with */
#define for_each_set_bit_from(bit, addr, size) \
    for ((bit) = find_next_bit((addr), (size), (bit));	\
         (bit) < (size);					\
         (bit) = find_next_bit((addr), (size), (bit) + 1))

#define for_each_clear_bit(bit, addr, size) \
    for ((bit) = find_first_zero_bit((addr), (size));	\
         (bit) < (size);					\
         (bit) = find_next_zero_bit((addr), (size), (bit) + 1))

/* same as for_each_clear_bit() but use bit as value to start with */
#define for_each_clear_bit_from(bit, addr, size) \
    for ((bit) = find_next_zero_bit((addr), (size), (bit));	\
         (bit) < (size);					\
         (bit) = find_next_zero_bit((addr), (size), (bit) + 1))

static inline int get_bitmask_order(unsigned int count)
{
    int order;

    order = fls(count);
    return order;	/* We could be slightly more clever with -1 here... */
}

static __always_inline unsigned long hweight_long(unsigned long w)
{
    return sizeof(w) == 4 ? hweight32(w) : hweight64(w);
}

/**
 * rol64 - rotate a 64-bit value left
 * @word: value to rotate
 * @shift: bits to roll
 */
static inline __u64 rol64(__u64 word, unsigned int shift)
{
    return (word << shift) | (word >> (64 - shift));
}

/**
 * ror64 - rotate a 64-bit value right
 * @word: value to rotate
 * @shift: bits to roll
 */
static inline __u64 ror64(__u64 word, unsigned int shift)
{
    return (word >> shift) | (word << (64 - shift));
}

/**
 * rol32 - rotate a 32-bit value left
 * @word: value to rotate
 * @shift: bits to roll
 */
static inline __u32 rol32(__u32 word, unsigned int shift)
{
    return (word << shift) | (word >> ((-shift) & 31));
}

/**
 * ror32 - rotate a 32-bit value right
 * @word: value to rotate
 * @shift: bits to roll
 */
static inline __u32 ror32(__u32 word, unsigned int shift)
{
    return (word >> shift) | (word << (32 - shift));
}

/**
 * rol16 - rotate a 16-bit value left
 * @word: value to rotate
 * @shift: bits to roll
 */
static inline __u16 rol16(__u16 word, unsigned int shift)
{
    return (word << shift) | (word >> (16 - shift));
}

/**
 * ror16 - rotate a 16-bit value right
 * @word: value to rotate
 * @shift: bits to roll
 */
static inline __u16 ror16(__u16 word, unsigned int shift)
{
    return (word >> shift) | (word << (16 - shift));
}

/**
 * rol8 - rotate an 8-bit value left
 * @word: value to rotate
 * @shift: bits to roll
 */
static inline __u8 rol8(__u8 word, unsigned int shift)
{
    return (word << shift) | (word >> (8 - shift));
}

/**
 * ror8 - rotate an 8-bit value right
 * @word: value to rotate
 * @shift: bits to roll
 */
static inline __u8 ror8(__u8 word, unsigned int shift)
{
    return (word >> shift) | (word << (8 - shift));
}

/**
 * sign_extend32 - sign extend a 32-bit value using specified bit as sign-bit
 * @value: value to sign extend
 * @index: 0 based bit index (0<=index<32) to sign bit
 *
 * This is safe to use for 16- and 8-bit types as well.
 */
static inline __i32 sign_extend32(__u32 value, int index)
{
    __u8 shift = 31 - index;
    return (__i32)(value << shift) >> shift;
}

/**
 * sign_extend64 - sign extend a 64-bit value using specified bit as sign-bit
 * @value: value to sign extend
 * @index: 0 based bit index (0<=index<64) to sign bit
 */
static inline __i64 sign_extend64(__u64 value, int index)
{
    __u8 shift = 63 - index;
    return (__i64)(value << shift) >> shift;
}

static inline unsigned fls_long(unsigned long l)
{
    if (sizeof(l) == 4)
        return fls(l);
    return fls64(l);
}

static inline int get_count_order(unsigned int count)
{
    int order;

    order = fls(count) - 1;
    if (count & (count - 1))
        order++;
    return order;
}

/**
 * get_count_order_long - get order after rounding @l up to power of 2
 * @l: parameter
 *
 * it is same as get_count_order() but with long type parameter
 */
static inline int get_count_order_long(unsigned long l)
{
    if (l == 0UL)
        return -1;
    else if (l & (l - 1UL))
        return (int)fls_long(l);
    else
        return (int)fls_long(l) - 1;
}

/**
 * __ffs64 - find first set bit in a 64 bit word
 * @word: The 64 bit word
 *
 * On 64 bit arches this is a synomyn for __ffs
 * The result is not defined if no bits are set, so check that @word
 * is non-zero before calling this.
 */
static inline unsigned long __ffs64(u64 word)
{
#if BITS_PER_LONG == 32
    if (((u32)word) == 0UL)
        return __ffs((u32)(word >> 32)) + 32;
#elif BITS_PER_LONG != 64
#error BITS_PER_LONG not 32 or 64
#endif
    return __ffs((unsigned long)word);
}

/**
 * assign_bit - Assign value to a bit in memory
 * @nr: the bit to set
 * @addr: the address to start counting from
 * @value: the value to assign
 */
static __always_inline void assign_bit(long nr, volatile unsigned long *addr,
                       bool value)
{
    if (value)
        set_bit(nr, addr);
    else
        clear_bit(nr, addr);
}

static __always_inline void __assign_bit(long nr, volatile unsigned long *addr,
                     bool value)
{
    if (value)
        __set_bit(nr, addr);
    else
        __clear_bit(nr, addr);
}

#ifndef set_mask_bits
#define set_mask_bits(ptr, mask, bits)	\
({								\
    const typeof(*(ptr)) mask__ = (mask), bits__ = (bits);	\
    typeof(*(ptr)) old__, new__;				\
                                \
    do {							\
        old__ = READ_ONCE(*(ptr));			\
        new__ = (old__ & ~mask__) | bits__;		\
    } while (cmpxchg(ptr, old__, new__) != old__);		\
                                \
    new__;							\
})
#endif

#ifndef bit_clear_unless
#define bit_clear_unless(ptr, clear, test)	\
({								\
    const typeof(*(ptr)) clear__ = (clear), test__ = (test);\
    typeof(*(ptr)) old__, new__;				\
                                \
    do {							\
        old__ = READ_ONCE(*(ptr));			\
        new__ = old__ & ~clear__;			\
    } while (!(old__ & test__) &&				\
         cmpxchg(ptr, old__, new__) != old__);		\
                                \
    !(old__ & test__);					\
})
#endif

#ifndef find_last_bit
/**
 * find_last_bit - find the last set bit in a memory region
 * @addr: The address to start the search at
 * @size: The number of bits to search
 *
 * Returns the bit number of the last set bit, or size.
 */
extern unsigned long find_last_bit(const unsigned long *addr,
                   unsigned long size);
#endif

/**
 * hswap32 - swap 16-bit halfwords within a 32-bit value
 * @h: value to swap
 */
static inline u32 hswap32(u32 h)
{
    return rol32(h, 16);
}

/**
 * hswap64 - swap 16-bit halfwords within a 64-bit value
 * @h: value to swap
 */
static inline u64 hswap64(u64 h)
{
    u64 m = 0x0000ffff0000ffffull;
    h = rol64(h, 32);
    return ((h & m) << 16) | ((h >> 16) & m);
}

/**
 * wswap64 - swap 32-bit words within a 64-bit value
 * @h: value to swap
 */
static inline u64 wswap64(u64 h)
{
    return rol64(h, 32);
}

/**
 * extract32:
 * @value: the value to extract the bit field from
 * @start: the lowest bit in the bit field (numbered from 0)
 * @length: the length of the bit field
 *
 * Extract from the 32 bit input @value the bit field specified by the
 * @start and @length parameters, and return it. The bit field must
 * lie entirely within the 32 bit word. It is valid to request that
 * all 32 bits are returned (ie @length 32 and @start 0).
 *
 * Returns: the value of the bit field extracted from the input value.
 */
static inline u32 extract32(u32 value, int start, int length)
{
    BUG_ON(start < 0 || length <= 0 || length > 32 - start);
    return (value >> start) & (~0U >> (32 - length));
}

/**
 * extract8:
 * @value: the value to extract the bit field from
 * @start: the lowest bit in the bit field (numbered from 0)
 * @length: the length of the bit field
 *
 * Extract from the 8 bit input @value the bit field specified by the
 * @start and @length parameters, and return it. The bit field must
 * lie entirely within the 8 bit word. It is valid to request that
 * all 8 bits are returned (ie @length 8 and @start 0).
 *
 * Returns: the value of the bit field extracted from the input value.
 */
static inline u8 extract8(u8 value, int start, int length)
{
    BUG_ON(start < 0 || length <= 0 || length > 8 - start);
    return extract32(value, start, length);
}

/**
 * extract16:
 * @value: the value to extract the bit field from
 * @start: the lowest bit in the bit field (numbered from 0)
 * @length: the length of the bit field
 *
 * Extract from the 16 bit input @value the bit field specified by the
 * @start and @length parameters, and return it. The bit field must
 * lie entirely within the 16 bit word. It is valid to request that
 * all 16 bits are returned (ie @length 16 and @start 0).
 *
 * Returns: the value of the bit field extracted from the input value.
 */
static inline u16 extract16(u16 value, int start, int length)
{
    BUG_ON(start < 0 || length <= 0 || length > 16 - start);
    return extract32(value, start, length);
}

/**
 * extract64:
 * @value: the value to extract the bit field from
 * @start: the lowest bit in the bit field (numbered from 0)
 * @length: the length of the bit field
 *
 * Extract from the 64 bit input @value the bit field specified by the
 * @start and @length parameters, and return it. The bit field must
 * lie entirely within the 64 bit word. It is valid to request that
 * all 64 bits are returned (ie @length 64 and @start 0).
 *
 * Returns: the value of the bit field extracted from the input value.
 */
static inline u64 extract64(u64 value, int start, int length)
{
    BUG_ON(start < 0 || length <= 0 || length > 64 - start);
    return (value >> start) & (~0ULL >> (64 - length));
}

/**
 * sextract32:
 * @value: the value to extract the bit field from
 * @start: the lowest bit in the bit field (numbered from 0)
 * @length: the length of the bit field
 *
 * Extract from the 32 bit input @value the bit field specified by the
 * @start and @length parameters, and return it, sign extended to
 * an i32 (ie with the most significant bit of the field propagated
 * to all the upper bits of the return value). The bit field must lie
 * entirely within the 32 bit word. It is valid to request that
 * all 32 bits are returned (ie @length 32 and @start 0).
 *
 * Returns: the sign extended value of the bit field extracted from the
 * input value.
 */
static inline i32 sextract32(u32 value, int start, int length)
{
    BUG_ON(start < 0 || length <= 0 || length > 32 - start);
    /* Note that this implementation relies on right shift of signed
     * integers being an arithmetic shift.
     */
    return ((i32)(value << (32 - length - start))) >> (32 - length);
}

/**
 * sextract64:
 * @value: the value to extract the bit field from
 * @start: the lowest bit in the bit field (numbered from 0)
 * @length: the length of the bit field
 *
 * Extract from the 64 bit input @value the bit field specified by the
 * @start and @length parameters, and return it, sign extended to
 * an i64 (ie with the most significant bit of the field propagated
 * to all the upper bits of the return value). The bit field must lie
 * entirely within the 64 bit word. It is valid to request that
 * all 64 bits are returned (ie @length 64 and @start 0).
 *
 * Returns: the sign extended value of the bit field extracted from the
 * input value.
 */
static inline i64 sextract64(u64 value, int start, int length)
{
    BUG_ON(start < 0 || length <= 0 || length > 64 - start);
    /* Note that this implementation relies on right shift of signed
     * integers being an arithmetic shift.
     */
    return ((i64)(value << (64 - length - start))) >> (64 - length);
}

/**
 * deposit32:
 * @value: initial value to insert bit field into
 * @start: the lowest bit in the bit field (numbered from 0)
 * @length: the length of the bit field
 * @fieldval: the value to insert into the bit field
 *
 * Deposit @fieldval into the 32 bit @value at the bit field specified
 * by the @start and @length parameters, and return the modified
 * @value. Bits of @value outside the bit field are not modified.
 * Bits of @fieldval above the least significant @length bits are
 * ignored. The bit field must lie entirely within the 32 bit word.
 * It is valid to request that all 32 bits are modified (ie @length
 * 32 and @start 0).
 *
 * Returns: the modified @value.
 */
static inline u32 deposit32(u32 value, int start, int length,
                                 u32 fieldval)
{
    u32 mask;
    BUG_ON(start < 0 || length <= 0 || length > 32 - start);
    mask = (~0U >> (32 - length)) << start;
    return (value & ~mask) | ((fieldval << start) & mask);
}

/**
 * deposit64:
 * @value: initial value to insert bit field into
 * @start: the lowest bit in the bit field (numbered from 0)
 * @length: the length of the bit field
 * @fieldval: the value to insert into the bit field
 *
 * Deposit @fieldval into the 64 bit @value at the bit field specified
 * by the @start and @length parameters, and return the modified
 * @value. Bits of @value outside the bit field are not modified.
 * Bits of @fieldval above the least significant @length bits are
 * ignored. The bit field must lie entirely within the 64 bit word.
 * It is valid to request that all 64 bits are modified (ie @length
 * 64 and @start 0).
 *
 * Returns: the modified @value.
 */
static inline u64 deposit64(u64 value, int start, int length,
                                 u64 fieldval)
{
    u64 mask;
    BUG_ON(start < 0 || length <= 0 || length > 64 - start);
    mask = (~0ULL >> (64 - length)) << start;
    return (value & ~mask) | ((fieldval << start) & mask);
}

/**
 * half_shuffle32:
 * @x: 32-bit value (of which only the bottom 16 bits are of interest)
 *
 * Given an input value::
 *
 *   xxxx xxxx xxxx xxxx ABCD EFGH IJKL MNOP
 *
 * return the value where the bottom 16 bits are spread out into
 * the odd bits in the word, and the even bits are zeroed::
 *
 *   0A0B 0C0D 0E0F 0G0H 0I0J 0K0L 0M0N 0O0P
 *
 * Any bits set in the top half of the input are ignored.
 *
 * Returns: the shuffled bits.
 */
static inline u32 half_shuffle32(u32 x)
{
    /* This algorithm is from _Hacker's Delight_ section 7-2 "Shuffling Bits".
     * It ignores any bits set in the top half of the input.
     */
    x = ((x & 0xFF00) << 8) | (x & 0x00FF);
    x = ((x << 4) | x) & 0x0F0F0F0F;
    x = ((x << 2) | x) & 0x33333333;
    x = ((x << 1) | x) & 0x55555555;
    return x;
}

/**
 * half_shuffle64:
 * @x: 64-bit value (of which only the bottom 32 bits are of interest)
 *
 * Given an input value::
 *
 *   xxxx xxxx xxxx .... xxxx xxxx ABCD EFGH IJKL MNOP QRST UVWX YZab cdef
 *
 * return the value where the bottom 32 bits are spread out into
 * the odd bits in the word, and the even bits are zeroed::
 *
 *   0A0B 0C0D 0E0F 0G0H 0I0J 0K0L 0M0N .... 0U0V 0W0X 0Y0Z 0a0b 0c0d 0e0f
 *
 * Any bits set in the top half of the input are ignored.
 *
 * Returns: the shuffled bits.
 */
static inline u64 half_shuffle64(u64 x)
{
    /* This algorithm is from _Hacker's Delight_ section 7-2 "Shuffling Bits".
     * It ignores any bits set in the top half of the input.
     */
    x = ((x & 0xFFFF0000ULL) << 16) | (x & 0xFFFF);
    x = ((x << 8) | x) & 0x00FF00FF00FF00FFULL;
    x = ((x << 4) | x) & 0x0F0F0F0F0F0F0F0FULL;
    x = ((x << 2) | x) & 0x3333333333333333ULL;
    x = ((x << 1) | x) & 0x5555555555555555ULL;
    return x;
}

/**
 * half_unshuffle32:
 * @x: 32-bit value (of which only the odd bits are of interest)
 *
 * Given an input value::
 *
 *   xAxB xCxD xExF xGxH xIxJ xKxL xMxN xOxP
 *
 * return the value where all the odd bits are compressed down
 * into the low half of the word, and the high half is zeroed::
 *
 *   0000 0000 0000 0000 ABCD EFGH IJKL MNOP
 *
 * Any even bits set in the input are ignored.
 *
 * Returns: the unshuffled bits.
 */
static inline u32 half_unshuffle32(u32 x)
{
    /* This algorithm is from _Hacker's Delight_ section 7-2 "Shuffling Bits".
     * where it is called an inverse half shuffle.
     */
    x &= 0x55555555;
    x = ((x >> 1) | x) & 0x33333333;
    x = ((x >> 2) | x) & 0x0F0F0F0F;
    x = ((x >> 4) | x) & 0x00FF00FF;
    x = ((x >> 8) | x) & 0x0000FFFF;
    return x;
}

/**
 * half_unshuffle64:
 * @x: 64-bit value (of which only the odd bits are of interest)
 *
 * Given an input value::
 *
 *   xAxB xCxD xExF xGxH xIxJ xKxL xMxN .... xUxV xWxX xYxZ xaxb xcxd xexf
 *
 * return the value where all the odd bits are compressed down
 * into the low half of the word, and the high half is zeroed::
 *
 *   0000 0000 0000 .... 0000 0000 ABCD EFGH IJKL MNOP QRST UVWX YZab cdef
 *
 * Any even bits set in the input are ignored.
 *
 * Returns: the unshuffled bits.
 */
static inline u64 half_unshuffle64(u64 x)
{
    /* This algorithm is from _Hacker's Delight_ section 7-2 "Shuffling Bits".
     * where it is called an inverse half shuffle.
     */
    x &= 0x5555555555555555ULL;
    x = ((x >> 1) | x) & 0x3333333333333333ULL;
    x = ((x >> 2) | x) & 0x0F0F0F0F0F0F0F0FULL;
    x = ((x >> 4) | x) & 0x00FF00FF00FF00FFULL;
    x = ((x >> 8) | x) & 0x0000FFFF0000FFFFULL;
    x = ((x >> 16) | x) & 0x00000000FFFFFFFFULL;
    return x;
}

#endif /* !SEMINIX_BITOPS_H */
