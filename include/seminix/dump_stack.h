#ifndef SEMINIX_DUMP_STACK_H
#define SEMINIX_DUMP_STACK_H

#include <seminix/types.h>
#include <seminix/linkage.h>

__printf(1, 2) void dump_stack_set_arch_desc(const char *fmt, ...);

extern void dump_stack_print_info(const char *log_lvl);
extern void show_regs_print_info(const char *log_lvl);

asmlinkage __visible void dump_stack(void);

#endif /* !SEMINIX_DUMP_STACK_H */
