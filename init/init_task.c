#include <seminix/sched.h>
#include <seminix/sched/rt.h>
#include <seminix/init_task.h>
#include <seminix/sched/prio.h>
#include <seminix/mm.h>

struct task_struct init_task = {
    .thread_info     = INIT_THREAD_INFO(init_task),
    .stack_refcount  = ATOMIC_INIT(1),
    .state           = TASK_RUNNING,
    .stack           = init_stack,
    .usage		     = ATOMIC_INIT(2),
    .flags           = PF_KTHREAD,
    .prio		     = MAX_PRIO - 20,
    .static_prio	 = MAX_PRIO - 20,
    .normal_prio	 = MAX_PRIO - 20,
    .policy		     = SCHED_NORMAL,
    .cpus_allowed	 = CPU_MASK_ALL,
    .nr_cpus_allowed = CONFIG_NR_CPUS,
    .mm		         = NULL,
    .active_mm       = &init_mm,
    .restart_block   = {
        .fn = do_no_restart_syscall,
    },
    .se		         = {
        .group_node 	= LIST_HEAD_INIT(init_task.se.group_node),
    },
    .rt		         = {
        .run_list	 = LIST_HEAD_INIT(init_task.rt.run_list),
        .time_slice	 = RR_TIMESLICE,
    },
    .tasks		     = LIST_HEAD_INIT(init_task.tasks),
    .pushable_tasks	 = PLIST_NODE_INIT(init_task.pushable_tasks, MAX_PRIO),
    .real_parent	 = &init_task,
    .parent		     = &init_task,
    .children	     = LIST_HEAD_INIT(init_task.children),
    .sibling	     = LIST_HEAD_INIT(init_task.sibling),
    .group_leader	 = &init_task,
    .comm            = INIT_TASK_COMM,
    .thread		     = INIT_THREAD,





    .tgid        = 0,


    .pi_lock    = __RAW_SPIN_LOCK_UNLOCKED(init_task.pi_lock),



    .pagefault_disabled = 0,
};
