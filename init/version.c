/*
 *  kernel/init/version.c
 *
 *  Copyright (C) 1992  Theodore Ts'o
 *
 *  May be freely distributed as part of Linux.
 */
#include <generated/compile.h>
#include <generated/utsrelease.h>
#include <generated/uapi/seminix/version.h>
#include <seminix/build-salt.h>
#include <seminix/uts.h>

#define version(a) Version_ ## a
#define version_string(a) version(a)

extern int version_string(SEMINIX_VERSION_CODE);
int version_string(SEMINIX_VERSION_CODE);

/* FIXED STRINGS! Don't touch! */
const char seminix_banner[] =
    "Seminix version " UTS_RELEASE " (" KERNEL_COMPILE_BY "@"
    KERNEL_COMPILE_HOST ") (" KERNEL_COMPILER ") " UTS_VERSION "\n";

BUILD_SALT;

const struct new_utsname utsname = {
    .sysname 	= UTS_SYSNAME,
    .nodename 	= UTS_NODENAME,
    .release	= UTS_RELEASE,
    .version	= UTS_VERSION,
    .machine	= UTS_MACHINE,
    .domainname	= UTS_DOMAINNAME,
};
