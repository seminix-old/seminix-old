# SPDX-License-Identifier: GPL-2.0
#
menu "Library routines"

#
# libfdt files, only selected if needed.
#
config LIBFDT
  bool

config ARCH_HAS_FAST_MULTIPLIER
  bool

config GENERIC_FIND_FIRST_BIT
  bool

config BITREVERSE
  bool

config HAVE_ARCH_BITREVERSE
  bool
  default n
  depends on BITREVERSE
  help
    This option enables the use of hardware bit-reversal instructions on
    architectures which support such operations.

config GENERIC_STRNCPY_FROM_USER
  bool

config GENERIC_STRNLEN_USER
  bool

config CRC32
bool "CRC32/CRC32c functions"
default y
select BITREVERSE
help
  This option is provided for the case where no in-kernel-tree
  modules require CRC32/CRC32c functions, but a module built outside
  the kernel tree does. Such modules that use library CRC32/CRC32c
  functions require M here.

choice
prompt "CRC32 implementation"
depends on CRC32
default CRC32_SLICEBY8
help
  This option allows a kernel builder to override the default choice
  of CRC32 algorithm.  Choose the default ("slice by 8") unless you
  know that you need one of the others.

config CRC32_SLICEBY8
  bool "Slice by 8 bytes"
  help
    Calculate checksum 8 bytes at a time with a clever slicing algorithm.
    This is the fastest algorithm, but comes with a 8KiB lookup table.
    Most modern processors have enough cache to hold this table without
    thrashing the cache.

    This is the default implementation choice.  Choose this one unless
    you have a good reason not to.

config CRC32_SLICEBY4
  bool "Slice by 4 bytes"
  help
    Calculate checksum 4 bytes at a time with a clever slicing algorithm.
    This is a bit slower than slice by 8, but has a smaller 4KiB lookup
    table.

    Only choose this option if you know what you are doing.

config CRC32_SARWATE
  bool "Sarwate's Algorithm (one byte at a time)"
  help
    Calculate checksum a byte at a time using Sarwate's algorithm.  This
    is not particularly fast, but has a small 256 byte lookup table.

    Only choose this option if you know what you are doing.

config CRC32_BIT
  bool "Classic Algorithm (one bit at a time)"
  help
    Calculate checksum one bit at a time.  This is VERY slow, but has
    no lookup table.  This is provided as a debugging option.

    Only choose this option if you are debugging crc32.

endchoice

config HAVE_EFFICIENT_UNALIGNED_ACCESS
  bool
  help
    Some architectures are unable to perform unaligned accesses
    without the use of get_unaligned/put_unaligned. Others are
    unable to perform such accesses efficiently (e.g. trap on
    unaligned access and require fixing it up in the exception
    handler.)

    This symbol should be selected by an architecture if it can
    perform unaligned accesses efficiently to allow different
    code paths to be selected for these cases. Some network
    drivers, for example, could opt to not fix up alignment
    problems with received packets if doing so would not help
    much.

    See Documentation/unaligned-memory-access.txt for more
    information on the topic of unaligned memory accesses.

config ARCH_HAS_UACCESS_FLUSHCACHE
  bool

endmenu
